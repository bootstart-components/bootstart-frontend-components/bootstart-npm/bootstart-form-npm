import { Component, Input, EventEmitter, Output, NgModule } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { __spread } from 'tslib';
import { SelectionModel } from '@angular/cdk/collections';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CommonModule } from '@angular/common';
import { MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatOptionModule, MatSelectModule, MatSlideToggleModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { NgxCurrencyModule } from 'ngx-currency';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartTextFormComponent = /** @class */ (function () {
    function BootstartTextFormComponent() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    BootstartTextFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('width', 100);
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartTextFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartTextFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-text-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <input matInput type=\"text\" [formControl]=\"control\" [required]=\"options.required\">\n</mat-form-field>\n",
                    styles: [".full-width{width:100%}"]
                },] },
    ];
    /** @nocollapse */
    BootstartTextFormComponent.ctorParameters = function () { return []; };
    BootstartTextFormComponent.propDecorators = {
        control: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartTextFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartParagraphFormComponent = /** @class */ (function () {
    function BootstartParagraphFormComponent() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    BootstartParagraphFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('width', 100);
        this._replaceUndefinedOption('rows', 6);
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartParagraphFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartParagraphFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-paragraph-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <textarea matInput type=\"paragraph\"\n            [formControl]=\"control\"\n            [required]=\"options.required\"\n            rows=\"{{options.rows}}\"></textarea>\n</mat-form-field>\n",
                    styles: [".full-width{width:100%}"]
                },] },
    ];
    /** @nocollapse */
    BootstartParagraphFormComponent.ctorParameters = function () { return []; };
    BootstartParagraphFormComponent.propDecorators = {
        control: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartParagraphFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartChipsFormComponent = /** @class */ (function () {
    function BootstartChipsFormComponent() {
        this.selection = new SelectionModel(true);
    }
    /**
     * @return {?}
     */
    BootstartChipsFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('choicesTranslatePrefix', '');
        this._replaceUndefinedOption('maximumSelectable', Infinity);
        (_a = this.selection).select.apply(_a, __spread(this.formArray.value));
        var _a;
    };
    /**
     * What happens when selection changes
     * @param {?} value Chip's value that has changed.
     * @return {?}
     */
    BootstartChipsFormComponent.prototype.selectionChanges = /**
     * What happens when selection changes
     * @param {?} value Chip's value that has changed.
     * @return {?}
     */
    function (value) {
        if (this.selection.selected.length >= this.options.maximumSelectable && !this.selection.isSelected(value)) {
            return;
        }
        this.selection.toggle(value);
        if (this.selection.isSelected(value)) {
            this.formArray.push(new FormControl(value));
        }
        else {
            this.formArray.removeAt(this.formArray.controls.findIndex(function (x) { return x.value === value; }));
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    BootstartChipsFormComponent.prototype.isClickable = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (this.selection.selected.length >= this.options.maximumSelectable) {
            return this.selection.isSelected(value);
        }
        return true;
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartChipsFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartChipsFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-chips-form',
                    template: "<mat-chip-list>\n  <mat-chip *ngFor=\"let choice of options.choices\"\n            (click)=\"selectionChanges(choice)\" selected\n            [disabled]=\"selection.selected.length >= options.maximumSelectable && !selection.isSelected(choice)\"\n            [ngClass]=\"isClickable(choice) ? 'bootstart-clickable-chip' : 'bootstart-non-clickable-chip'\"\n            [color]=\"selection.isSelected(choice) ? 'primary' : 'none'\">\n\n    <mat-chip-avatar *ngIf=\"selection.isSelected(choice)\">\n      <span class=\"fas fa-check\"></span>\n    </mat-chip-avatar>\n\n    {{options.choicesTranslatePrefix + choice|translate}}\n  </mat-chip>\n</mat-chip-list>\n",
                    styles: [".bootstart-clickable-chip{cursor:pointer}.bootstart-non-clickable-chip{cursor:not-allowed}"]
                },] },
    ];
    /** @nocollapse */
    BootstartChipsFormComponent.ctorParameters = function () { return []; };
    BootstartChipsFormComponent.propDecorators = {
        formArray: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartChipsFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartSelectFormComponent = /** @class */ (function () {
    function BootstartSelectFormComponent() {
        this.control = new FormControl('');
        this.selectionChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    BootstartSelectFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('choicesTranslatePrefix', '');
        this._replaceUndefinedOption('width', 100);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    BootstartSelectFormComponent.prototype.selectionChanges = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.selectionChange.emit(event);
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartSelectFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartSelectFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-select-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <mat-select [required]=\"options.required\" [formControl]=\"control\" (selectionChange)=\"selectionChanges($event)\">\n    <mat-option *ngFor=\"let choice of options.choices\" value=\"{{choice}}\">\n      {{options.choicesTranslatePrefix + choice|translate}}\n    </mat-option>\n  </mat-select>\n</mat-form-field>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartSelectFormComponent.ctorParameters = function () { return []; };
    BootstartSelectFormComponent.propDecorators = {
        control: [{ type: Input }],
        options: [{ type: Input }],
        selectionChange: [{ type: Output }]
    };
    return BootstartSelectFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartNumberFormComponent = /** @class */ (function () {
    function BootstartNumberFormComponent() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    BootstartNumberFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('width', '100');
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartNumberFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartNumberFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-number-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <input matInput type=\"number\" [formControl]=\"control\" [required]=\"options.required\">\n</mat-form-field>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartNumberFormComponent.ctorParameters = function () { return []; };
    BootstartNumberFormComponent.propDecorators = {
        control: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartNumberFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartBooleanFormComponent = /** @class */ (function () {
    function BootstartBooleanFormComponent() {
        this.control = new FormControl(false);
    }
    /**
     * @return {?}
     */
    BootstartBooleanFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('color', 'primary');
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartBooleanFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartBooleanFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-boolean-form',
                    template: "<mat-slide-toggle [formControl]=\"control\" [color]=\"options.color\">\n  <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n  <span *ngIf=\"options.icon\">&nbsp;</span>\n  <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n</mat-slide-toggle>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartBooleanFormComponent.ctorParameters = function () { return []; };
    BootstartBooleanFormComponent.propDecorators = {
        control: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartBooleanFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartChipsTextFormComponent = /** @class */ (function () {
    function BootstartChipsTextFormComponent() {
    }
    /**
     * @return {?}
     */
    BootstartChipsTextFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('maximum', Infinity);
        this._replaceUndefinedOption('separator', [ENTER, COMMA]);
        this._replaceUndefinedOption('chipColor', 'primary');
    };
    /**
     * @param {?} event
     * @return {?}
     */
    BootstartChipsTextFormComponent.prototype.addChip = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.formArray.controls.length < this.options.maximum) {
            /** @type {?} */
            var input = event.input;
            /** @type {?} */
            var value = event.value;
            if ((value || '').trim()) {
                this.formArray.push(new FormControl(value.trim()));
            }
            // Reset the input value
            if (input) {
                input.value = '';
            }
        }
    };
    /**
     * @param {?} chip
     * @return {?}
     */
    BootstartChipsTextFormComponent.prototype.removeChip = /**
     * @param {?} chip
     * @return {?}
     */
    function (chip) {
        /** @type {?} */
        var index = this.formArray.value.indexOf(chip);
        if (index >= 0) {
            this.formArray.removeAt(index);
        }
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartChipsTextFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartChipsTextFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-chips-text-form',
                    template: "<mat-label>\n  <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n  <span *ngIf=\"options.icon\">&nbsp;</span>\n  <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n</mat-label>\n\n<mat-chip-list #chipList>\n  <mat-chip *ngFor=\"let chip of formArray.value\"\n            selected\n            [color]=\"options.chipColor\"\n            [selectable]=\"false\"\n            [removable]=\"true\"\n            (removed)=\"removeChip(chip)\">\n    {{chip}}\n    <mat-icon matChipRemove>cancel</mat-icon>\n  </mat-chip>\n\n  <input matInput type=\"text\"\n         *ngIf=\"formArray.controls.length < options.maximum\"\n         placeholder=\"{{options.placeholder|translate}}\"\n         [matChipInputFor]=\"chipList\"\n         [matChipInputSeparatorKeyCodes]=\"options.separator\"\n         (matChipInputTokenEnd)=\"addChip($event)\">\n\n</mat-chip-list>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartChipsTextFormComponent.ctorParameters = function () { return []; };
    BootstartChipsTextFormComponent.propDecorators = {
        options: [{ type: Input }],
        formArray: [{ type: Input }]
    };
    return BootstartChipsTextFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartCurrencyFormComponent = /** @class */ (function () {
    function BootstartCurrencyFormComponent() {
    }
    /**
     * @return {?}
     */
    BootstartCurrencyFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('width', 100);
        this._replaceUndefinedOption('align', 'left');
        this._replaceUndefinedOption('allowNegative', true);
        this._replaceUndefinedOption('decimal', '.');
        this._replaceUndefinedOption('precision', 2);
        this._replaceUndefinedOption('prefix', '$ ');
        this._replaceUndefinedOption('suffix', '');
        this._replaceUndefinedOption('thousands', ',');
        this._replaceUndefinedOption('nullable', true);
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartCurrencyFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartCurrencyFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-currency-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <form [formGroup]=\"group\">\n    <input currencyMask matInput\n           formControlName=\"{{controlName}}\"\n           [required]=\"options.required\"\n           [options]=\"{\n           align: options.align,\n           allowNegative: options.allowNegative,\n           decimal: options.decimal,\n           precision: options.precision,\n           prefix: options.prefix,\n           suffix: options.suffix,\n           thousands: options.thousands,\n           nullable: options.nullable\n           }\">\n  </form>\n</mat-form-field>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartCurrencyFormComponent.ctorParameters = function () { return []; };
    BootstartCurrencyFormComponent.propDecorators = {
        group: [{ type: Input }],
        controlName: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartCurrencyFormComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
var BootstartFormModule = /** @class */ (function () {
    function BootstartFormModule() {
    }
    BootstartFormModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                        MatInputModule,
                        MatFormFieldModule,
                        MatIconModule,
                        MatChipsModule,
                        MatSelectModule,
                        MatOptionModule,
                        MatSlideToggleModule,
                        NgxCurrencyModule,
                        TranslateModule
                    ],
                    declarations: [
                        BootstartTextFormComponent,
                        BootstartParagraphFormComponent,
                        BootstartChipsFormComponent,
                        BootstartSelectFormComponent,
                        BootstartNumberFormComponent,
                        BootstartBooleanFormComponent,
                        BootstartChipsTextFormComponent,
                        BootstartCurrencyFormComponent
                    ],
                    exports: [
                        BootstartTextFormComponent,
                        BootstartParagraphFormComponent,
                        BootstartChipsFormComponent,
                        BootstartSelectFormComponent,
                        BootstartNumberFormComponent,
                        BootstartBooleanFormComponent,
                        BootstartChipsTextFormComponent,
                        BootstartCurrencyFormComponent
                    ]
                },] },
    ];
    return BootstartFormModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @param {?} value
 * @return {?}
 */
function SafeSetFormArrayValue(value) {
    /** @type {?} */
    var builder = new FormBuilder();
    if (typeof value === 'object') {
        return builder.array(value);
    }
    else {
        return builder.array([]);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { BootstartFormModule, SafeSetFormArrayValue, BootstartBooleanFormComponent as ɵf, BootstartChipsFormComponent as ɵc, BootstartChipsTextFormComponent as ɵg, BootstartCurrencyFormComponent as ɵh, BootstartNumberFormComponent as ɵe, BootstartParagraphFormComponent as ɵb, BootstartSelectFormComponent as ɵd, BootstartTextFormComponent as ɵa };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZvcm0uanMubWFwIiwic291cmNlcyI6WyJuZzovL2Jvb3RzdGFydC1mb3JtL2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC10ZXh0LWZvcm0vYm9vdHN0YXJ0LXRleHQtZm9ybS5jb21wb25lbnQudHMiLCJuZzovL2Jvb3RzdGFydC1mb3JtL2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS9ib290c3RhcnQtcGFyYWdyYXBoLWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvY29tcG9uZW50cy9ib290c3RhcnQtY2hpcHMtZm9ybS9ib290c3RhcnQtY2hpcHMtZm9ybS5jb21wb25lbnQudHMiLCJuZzovL2Jvb3RzdGFydC1mb3JtL2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS9ib290c3RhcnQtc2VsZWN0LWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvY29tcG9uZW50cy9ib290c3RhcnQtbnVtYmVyLWZvcm0vYm9vdHN0YXJ0LW51bWJlci1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS9ib290c3RhcnQtYm9vbGVhbi1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS9ib290c3RhcnQtY2hpcHMtdGV4dC1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0vYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvYm9vdHN0YXJ0LWZvcm0ubW9kdWxlLnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvZnVuY3Rpb25zL3NhZmUtc2V0LWZvcm0tYXJyYXktdmFsdWUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnRUZXh0Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC10ZXh0LWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LXRleHQtZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWZvcm0tZmllbGQgW25nU3R5bGVdPVwieyd3aWR0aCcgOiBvcHRpb25zLndpZHRoKyclJ31cIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxpbnB1dCBtYXRJbnB1dCB0eXBlPVwidGV4dFwiIFtmb3JtQ29udHJvbF09XCJjb250cm9sXCIgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIj5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2AuZnVsbC13aWR0aHt3aWR0aDoxMDAlfWBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0VGV4dEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRUZXh0Rm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Qm9vdHN0YXJ0UGFyYWdyYXBoRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS1vcHRpb25zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtcGFyYWdyYXBoLWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8dGV4dGFyZWEgbWF0SW5wdXQgdHlwZT1cInBhcmFncmFwaFwiXG4gICAgICAgICAgICBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiXG4gICAgICAgICAgICBbcmVxdWlyZWRdPVwib3B0aW9ucy5yZXF1aXJlZFwiXG4gICAgICAgICAgICByb3dzPVwie3tvcHRpb25zLnJvd3N9fVwiPjwvdGV4dGFyZWE+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgLmZ1bGwtd2lkdGh7d2lkdGg6MTAwJX1gXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydFBhcmFncmFwaEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRQYXJhZ3JhcGhGb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woJycpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignd2lkdGgnLCAxMDApO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3Jvd3MnLCA2KTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7U2VsZWN0aW9uTW9kZWx9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XG5pbXBvcnQge0Jvb3RzdGFydENoaXBzRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1jaGlwcy1mb3JtLW9wdGlvbnMnO1xuaW1wb3J0IHtGb3JtQXJyYXksIEZvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWNoaXBzLWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1jaGlwLWxpc3Q+XG4gIDxtYXQtY2hpcCAqbmdGb3I9XCJsZXQgY2hvaWNlIG9mIG9wdGlvbnMuY2hvaWNlc1wiXG4gICAgICAgICAgICAoY2xpY2spPVwic2VsZWN0aW9uQ2hhbmdlcyhjaG9pY2UpXCIgc2VsZWN0ZWRcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJzZWxlY3Rpb24uc2VsZWN0ZWQubGVuZ3RoID49IG9wdGlvbnMubWF4aW11bVNlbGVjdGFibGUgJiYgIXNlbGVjdGlvbi5pc1NlbGVjdGVkKGNob2ljZSlcIlxuICAgICAgICAgICAgW25nQ2xhc3NdPVwiaXNDbGlja2FibGUoY2hvaWNlKSA/ICdib290c3RhcnQtY2xpY2thYmxlLWNoaXAnIDogJ2Jvb3RzdGFydC1ub24tY2xpY2thYmxlLWNoaXAnXCJcbiAgICAgICAgICAgIFtjb2xvcl09XCJzZWxlY3Rpb24uaXNTZWxlY3RlZChjaG9pY2UpID8gJ3ByaW1hcnknIDogJ25vbmUnXCI+XG5cbiAgICA8bWF0LWNoaXAtYXZhdGFyICpuZ0lmPVwic2VsZWN0aW9uLmlzU2VsZWN0ZWQoY2hvaWNlKVwiPlxuICAgICAgPHNwYW4gY2xhc3M9XCJmYXMgZmEtY2hlY2tcIj48L3NwYW4+XG4gICAgPC9tYXQtY2hpcC1hdmF0YXI+XG5cbiAgICB7e29wdGlvbnMuY2hvaWNlc1RyYW5zbGF0ZVByZWZpeCArIGNob2ljZXx0cmFuc2xhdGV9fVxuICA8L21hdC1jaGlwPlxuPC9tYXQtY2hpcC1saXN0PlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgLmJvb3RzdGFydC1jbGlja2FibGUtY2hpcHtjdXJzb3I6cG9pbnRlcn0uYm9vdHN0YXJ0LW5vbi1jbGlja2FibGUtY2hpcHtjdXJzb3I6bm90LWFsbG93ZWR9YF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRDaGlwc0Zvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGZvcm1BcnJheTogRm9ybUFycmF5O1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRDaGlwc0Zvcm1PcHRpb25zO1xuXG4gIHNlbGVjdGlvbiA9IG5ldyBTZWxlY3Rpb25Nb2RlbDxzdHJpbmc+KHRydWUpO1xuXG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdjaG9pY2VzVHJhbnNsYXRlUHJlZml4JywgJycpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ21heGltdW1TZWxlY3RhYmxlJywgSW5maW5pdHkpO1xuICAgIHRoaXMuc2VsZWN0aW9uLnNlbGVjdCguLi50aGlzLmZvcm1BcnJheS52YWx1ZSk7XG4gIH1cblxuICAvKipcbiAgICogV2hhdCBoYXBwZW5zIHdoZW4gc2VsZWN0aW9uIGNoYW5nZXNcbiAgICogQHBhcmFtIHZhbHVlIENoaXAncyB2YWx1ZSB0aGF0IGhhcyBjaGFuZ2VkLlxuICAgKi9cbiAgcHVibGljIHNlbGVjdGlvbkNoYW5nZXModmFsdWU6IHN0cmluZyk6IHZvaWQge1xuICAgIGlmICh0aGlzLnNlbGVjdGlvbi5zZWxlY3RlZC5sZW5ndGggPj0gdGhpcy5vcHRpb25zLm1heGltdW1TZWxlY3RhYmxlICYmICF0aGlzLnNlbGVjdGlvbi5pc1NlbGVjdGVkKHZhbHVlKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnNlbGVjdGlvbi50b2dnbGUodmFsdWUpO1xuICAgIGlmICh0aGlzLnNlbGVjdGlvbi5pc1NlbGVjdGVkKHZhbHVlKSkge1xuICAgICAgdGhpcy5mb3JtQXJyYXkucHVzaChuZXcgRm9ybUNvbnRyb2wodmFsdWUpKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5mb3JtQXJyYXkucmVtb3ZlQXQodGhpcy5mb3JtQXJyYXkuY29udHJvbHMuZmluZEluZGV4KHggPT4geC52YWx1ZSA9PT0gdmFsdWUpKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgaXNDbGlja2FibGUodmFsdWU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIGlmICh0aGlzLnNlbGVjdGlvbi5zZWxlY3RlZC5sZW5ndGggPj0gdGhpcy5vcHRpb25zLm1heGltdW1TZWxlY3RhYmxlKSB7XG4gICAgICByZXR1cm4gdGhpcy5zZWxlY3Rpb24uaXNTZWxlY3RlZCh2YWx1ZSk7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIiwiaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Qm9vdHN0YXJ0U2VsZWN0Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS1vcHRpb25zJztcbmltcG9ydCB7TWF0U2VsZWN0aW9uTGlzdENoYW5nZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1zZWxlY3QtZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWZvcm0tZmllbGQgW25nU3R5bGVdPVwieyd3aWR0aCcgOiBvcHRpb25zLndpZHRoKyclJ31cIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxtYXQtc2VsZWN0IFtyZXF1aXJlZF09XCJvcHRpb25zLnJlcXVpcmVkXCIgW2Zvcm1Db250cm9sXT1cImNvbnRyb2xcIiAoc2VsZWN0aW9uQ2hhbmdlKT1cInNlbGVjdGlvbkNoYW5nZXMoJGV2ZW50KVwiPlxuICAgIDxtYXQtb3B0aW9uICpuZ0Zvcj1cImxldCBjaG9pY2Ugb2Ygb3B0aW9ucy5jaG9pY2VzXCIgdmFsdWU9XCJ7e2Nob2ljZX19XCI+XG4gICAgICB7e29wdGlvbnMuY2hvaWNlc1RyYW5zbGF0ZVByZWZpeCArIGNob2ljZXx0cmFuc2xhdGV9fVxuICAgIDwvbWF0LW9wdGlvbj5cbiAgPC9tYXQtc2VsZWN0PlxuPC9tYXQtZm9ybS1maWVsZD5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYGBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgLyoqIEFzc29jaWF0ZWQgZm9ybSBjb250cm9sICovXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuXG4gIC8qKiBTZWxlY3Qgb3B0aW9ucyAqL1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRTZWxlY3RGb3JtT3B0aW9ucztcblxuICAvKiogT24gQ2hhbmdlIGVtaXR0ZXIgKi9cbiAgQE91dHB1dCgpIHNlbGVjdGlvbkNoYW5nZTogRXZlbnRFbWl0dGVyPE1hdFNlbGVjdGlvbkxpc3RDaGFuZ2U+O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuY29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgnJyk7XG4gICAgdGhpcy5zZWxlY3Rpb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPE1hdFNlbGVjdGlvbkxpc3RDaGFuZ2U+KCk7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdjaG9pY2VzVHJhbnNsYXRlUHJlZml4JywgJycpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgfVxuXG4gIHB1YmxpYyBzZWxlY3Rpb25DaGFuZ2VzKGV2ZW50OiBNYXRTZWxlY3Rpb25MaXN0Q2hhbmdlKTogdm9pZCB7XG4gICAgdGhpcy5zZWxlY3Rpb25DaGFuZ2UuZW1pdChldmVudCk7XG4gIH1cblxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Zvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0Jvb3RzdGFydE51bWJlckZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtbnVtYmVyLWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LW51bWJlci1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPGlucHV0IG1hdElucHV0IHR5cGU9XCJudW1iZXJcIiBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiIFtyZXF1aXJlZF09XCJvcHRpb25zLnJlcXVpcmVkXCI+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnROdW1iZXJGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBjb250cm9sOiBGb3JtQ29udHJvbDtcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0TnVtYmVyRm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgJzEwMCcpO1xuICB9XG5cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIiwiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnRCb29sZWFuRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LXNsaWRlLXRvZ2dsZSBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiIFtjb2xvcl09XCJvcHRpb25zLmNvbG9yXCI+XG4gIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuPC9tYXQtc2xpZGUtdG9nZ2xlPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRCb29sZWFuRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgY29udHJvbDogRm9ybUNvbnRyb2w7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydEJvb2xlYW5Gb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woZmFsc2UpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignY29sb3InLCAncHJpbWFyeScpO1xuICB9XG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1jaGlwcy10ZXh0LWZvcm0tb3B0aW9ucyc7XG5pbXBvcnQge0Zvcm1BcnJheSwgRm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Q09NTUEsIEVOVEVSfSBmcm9tICdAYW5ndWxhci9jZGsva2V5Y29kZXMnO1xuaW1wb3J0IHtNYXRDaGlwSW5wdXRFdmVudH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1jaGlwcy10ZXh0LWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1sYWJlbD5cbiAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG48L21hdC1sYWJlbD5cblxuPG1hdC1jaGlwLWxpc3QgI2NoaXBMaXN0PlxuICA8bWF0LWNoaXAgKm5nRm9yPVwibGV0IGNoaXAgb2YgZm9ybUFycmF5LnZhbHVlXCJcbiAgICAgICAgICAgIHNlbGVjdGVkXG4gICAgICAgICAgICBbY29sb3JdPVwib3B0aW9ucy5jaGlwQ29sb3JcIlxuICAgICAgICAgICAgW3NlbGVjdGFibGVdPVwiZmFsc2VcIlxuICAgICAgICAgICAgW3JlbW92YWJsZV09XCJ0cnVlXCJcbiAgICAgICAgICAgIChyZW1vdmVkKT1cInJlbW92ZUNoaXAoY2hpcClcIj5cbiAgICB7e2NoaXB9fVxuICAgIDxtYXQtaWNvbiBtYXRDaGlwUmVtb3ZlPmNhbmNlbDwvbWF0LWljb24+XG4gIDwvbWF0LWNoaXA+XG5cbiAgPGlucHV0IG1hdElucHV0IHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICpuZ0lmPVwiZm9ybUFycmF5LmNvbnRyb2xzLmxlbmd0aCA8IG9wdGlvbnMubWF4aW11bVwiXG4gICAgICAgICBwbGFjZWhvbGRlcj1cInt7b3B0aW9ucy5wbGFjZWhvbGRlcnx0cmFuc2xhdGV9fVwiXG4gICAgICAgICBbbWF0Q2hpcElucHV0Rm9yXT1cImNoaXBMaXN0XCJcbiAgICAgICAgIFttYXRDaGlwSW5wdXRTZXBhcmF0b3JLZXlDb2Rlc109XCJvcHRpb25zLnNlcGFyYXRvclwiXG4gICAgICAgICAobWF0Q2hpcElucHV0VG9rZW5FbmQpPVwiYWRkQ2hpcCgkZXZlbnQpXCI+XG5cbjwvbWF0LWNoaXAtbGlzdD5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYGBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybU9wdGlvbnM7XG4gIEBJbnB1dCgpIGZvcm1BcnJheTogRm9ybUFycmF5O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignbWF4aW11bScsIEluZmluaXR5KTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdzZXBhcmF0b3InLCBbRU5URVIsIENPTU1BXSk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignY2hpcENvbG9yJywgJ3ByaW1hcnknKTtcbiAgfVxuXG5cbiAgcHVibGljIGFkZENoaXAoZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZm9ybUFycmF5LmNvbnRyb2xzLmxlbmd0aCA8IHRoaXMub3B0aW9ucy5tYXhpbXVtKSB7XG4gICAgICBjb25zdCBpbnB1dCA9IGV2ZW50LmlucHV0O1xuICAgICAgY29uc3QgdmFsdWUgPSBldmVudC52YWx1ZTtcblxuICAgICAgaWYgKCh2YWx1ZSB8fCAnJykudHJpbSgpKSB7XG4gICAgICAgIHRoaXMuZm9ybUFycmF5LnB1c2gobmV3IEZvcm1Db250cm9sKHZhbHVlLnRyaW0oKSkpO1xuICAgICAgfVxuICAgICAgLy8gUmVzZXQgdGhlIGlucHV0IHZhbHVlXG4gICAgICBpZiAoaW5wdXQpIHtcbiAgICAgICAgaW5wdXQudmFsdWUgPSAnJztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgcmVtb3ZlQ2hpcChjaGlwOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBjb25zdCBpbmRleCA9IHRoaXMuZm9ybUFycmF5LnZhbHVlLmluZGV4T2YoY2hpcCk7XG5cbiAgICBpZiAoaW5kZXggPj0gMCkge1xuICAgICAgdGhpcy5mb3JtQXJyYXkucmVtb3ZlQXQoaW5kZXgpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydEN1cnJlbmN5Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscyc7XG5pbXBvcnQge0Zvcm1Hcm91cH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1jdXJyZW5jeS1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPGZvcm0gW2Zvcm1Hcm91cF09XCJncm91cFwiPlxuICAgIDxpbnB1dCBjdXJyZW5jeU1hc2sgbWF0SW5wdXRcbiAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwie3tjb250cm9sTmFtZX19XCJcbiAgICAgICAgICAgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIlxuICAgICAgICAgICBbb3B0aW9uc109XCJ7XG4gICAgICAgICAgIGFsaWduOiBvcHRpb25zLmFsaWduLFxuICAgICAgICAgICBhbGxvd05lZ2F0aXZlOiBvcHRpb25zLmFsbG93TmVnYXRpdmUsXG4gICAgICAgICAgIGRlY2ltYWw6IG9wdGlvbnMuZGVjaW1hbCxcbiAgICAgICAgICAgcHJlY2lzaW9uOiBvcHRpb25zLnByZWNpc2lvbixcbiAgICAgICAgICAgcHJlZml4OiBvcHRpb25zLnByZWZpeCxcbiAgICAgICAgICAgc3VmZml4OiBvcHRpb25zLnN1ZmZpeCxcbiAgICAgICAgICAgdGhvdXNhbmRzOiBvcHRpb25zLnRob3VzYW5kcyxcbiAgICAgICAgICAgbnVsbGFibGU6IG9wdGlvbnMubnVsbGFibGVcbiAgICAgICAgICAgfVwiPlxuICA8L2Zvcm0+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRDdXJyZW5jeUZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGdyb3VwOiBGb3JtR3JvdXA7XG4gIEBJbnB1dCgpIGNvbnRyb2xOYW1lOiBzdHJpbmc7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydEN1cnJlbmN5Rm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCd3aWR0aCcsIDEwMCk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignYWxpZ24nLCAnbGVmdCcpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2FsbG93TmVnYXRpdmUnLCB0cnVlKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdkZWNpbWFsJywgJy4nKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdwcmVjaXNpb24nLCAyKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdwcmVmaXgnLCAnJCAnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdzdWZmaXgnLCAnJyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbigndGhvdXNhbmRzJywgJywnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdudWxsYWJsZScsIHRydWUpO1xuICB9XG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRUZXh0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC10ZXh0LWZvcm0vYm9vdHN0YXJ0LXRleHQtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge0Zvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge1xuICBNYXRDaGlwc01vZHVsZSxcbiAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICBNYXRJY29uTW9kdWxlLFxuICBNYXRJbnB1dE1vZHVsZSxcbiAgTWF0T3B0aW9uTW9kdWxlLFxuICBNYXRTZWxlY3RNb2R1bGUsXG4gIE1hdFNsaWRlVG9nZ2xlTW9kdWxlXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7VHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7Qm9vdHN0YXJ0UGFyYWdyYXBoRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS9ib290c3RhcnQtcGFyYWdyYXBoLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWNoaXBzLWZvcm0vYm9vdHN0YXJ0LWNoaXBzLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS9ib290c3RhcnQtc2VsZWN0LWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1udW1iZXItZm9ybS9ib290c3RhcnQtbnVtYmVyLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtYm9vbGVhbi1mb3JtL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1jaGlwcy10ZXh0LWZvcm0vYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRDdXJyZW5jeUZvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtY3VycmVuY3ktZm9ybS9ib290c3RhcnQtY3VycmVuY3ktZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtOZ3hDdXJyZW5jeU1vZHVsZX0gZnJvbSAnbmd4LWN1cnJlbmN5JztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIEZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJY29uTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRDaGlwc01vZHVsZSxcbiAgICAgICAgICAgICAgTWF0U2VsZWN0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRPcHRpb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdFNsaWRlVG9nZ2xlTW9kdWxlLFxuICAgICAgICAgICAgICBOZ3hDdXJyZW5jeU1vZHVsZSxcbiAgICAgICAgICAgICAgVHJhbnNsYXRlTW9kdWxlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydFRleHRGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRQYXJhZ3JhcGhGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDaGlwc0Zvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydFNlbGVjdEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydE51bWJlckZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydEJvb2xlYW5Gb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDaGlwc1RleHRGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDdXJyZW5jeUZvcm1Db21wb25lbnRcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBleHBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0VGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydFBhcmFncmFwaEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzVGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudFxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0Rm9ybU1vZHVsZSB7XG59XG4iLCJpbXBvcnQge0Zvcm1BcnJheSwgRm9ybUJ1aWxkZXJ9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuZXhwb3J0IGZ1bmN0aW9uIFNhZmVTZXRGb3JtQXJyYXlWYWx1ZSh2YWx1ZTogYW55KTogRm9ybUFycmF5IHtcbiAgY29uc3QgYnVpbGRlciA9IG5ldyBGb3JtQnVpbGRlcigpO1xuICBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiBidWlsZGVyLmFycmF5KHZhbHVlKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gYnVpbGRlci5hcnJheShbXSk7XG4gIH1cbn1cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBO0lBdUJFO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQzs7OztJQUVELDZDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7S0FDNUM7Ozs7OztJQUdPLDREQUF1Qjs7Ozs7Y0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7O2dCQS9CSixTQUFTLFNBQUM7b0JBQ0UsUUFBUSxFQUFLLHFCQUFxQjtvQkFDbEMsUUFBUSxFQUFFLHdZQVN0QjtvQkFDWSxNQUFNLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztpQkFDcEM7Ozs7OzBCQUdULEtBQUs7MEJBQ0wsS0FBSzs7cUNBckJSOzs7Ozs7O0FDQUE7SUEwQkU7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3BDOzs7O0lBRUQsa0RBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ3pDOzs7Ozs7SUFHTyxpRUFBdUI7Ozs7O2NBQUMsS0FBVSxFQUFFLFlBQWlCO1FBQzNELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLEVBQUU7WUFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDcEM7OztnQkFuQ0osU0FBUyxTQUFDO29CQUNFLFFBQVEsRUFBSywwQkFBMEI7b0JBQ3ZDLFFBQVEsRUFBRSw0ZEFZdEI7b0JBQ1ksTUFBTSxFQUFFLENBQUMseUJBQXlCLENBQUM7aUJBQ3BDOzs7OzswQkFHVCxLQUFLOzBCQUNMLEtBQUs7OzBDQXhCUjs7Ozs7Ozs7SUNnQ0U7eUJBSFksSUFBSSxjQUFjLENBQVMsSUFBSSxDQUFDO0tBSTNDOzs7O0lBRUQsOENBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLHdCQUF3QixFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxtQkFBbUIsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUM1RCxDQUFBLEtBQUEsSUFBSSxDQUFDLFNBQVMsRUFBQyxNQUFNLG9CQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFFOztLQUNoRDs7Ozs7O0lBTU0sc0RBQWdCOzs7OztjQUFDLEtBQWE7UUFDbkMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3pHLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztTQUM3QzthQUFNO1lBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxLQUFLLEdBQUEsQ0FBQyxDQUFDLENBQUM7U0FDcEY7Ozs7OztJQUdJLGlEQUFXOzs7O2NBQUMsS0FBYTtRQUM5QixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFO1lBQ3BFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDekM7UUFDRCxPQUFPLElBQUksQ0FBQzs7Ozs7OztJQUlOLDZEQUF1Qjs7Ozs7Y0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7O2dCQS9ESixTQUFTLFNBQUM7b0JBQ0UsUUFBUSxFQUFLLHNCQUFzQjtvQkFDbkMsUUFBUSxFQUFFLG1wQkFjdEI7b0JBQ1ksTUFBTSxFQUFFLENBQUMsNEZBQTRGLENBQUM7aUJBQ3ZHOzs7Ozs0QkFHVCxLQUFLOzBCQUNMLEtBQUs7O3NDQTNCUjs7Ozs7OztBQ0FBO0lBa0NFO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksWUFBWSxFQUEwQixDQUFDO0tBQ25FOzs7O0lBRUQsK0NBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLHdCQUF3QixFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7S0FDNUM7Ozs7O0lBRU0sdURBQWdCOzs7O2NBQUMsS0FBNkI7UUFDbkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Ozs7Ozs7SUFJM0IsOERBQXVCOzs7OztjQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7Z0JBL0NKLFNBQVMsU0FBQztvQkFDRSxRQUFRLEVBQUssdUJBQXVCO29CQUNwQyxRQUFRLEVBQUUsc2xCQWF0QjtvQkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7aUJBQ2I7Ozs7OzBCQUlULEtBQUs7MEJBR0wsS0FBSztrQ0FHTCxNQUFNOzt1Q0FoQ1Q7Ozs7Ozs7QUNBQTtJQXVCRTtRQUNFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDcEM7Ozs7SUFFRCwrQ0FBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO0tBQzlDOzs7Ozs7SUFHTyw4REFBdUI7Ozs7O2NBQUMsS0FBVSxFQUFFLFlBQWlCO1FBQzNELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLEVBQUU7WUFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDcEM7OztnQkEvQkosU0FBUyxTQUFDO29CQUNFLFFBQVEsRUFBSyx1QkFBdUI7b0JBQ3BDLFFBQVEsRUFBRSwwWUFTdEI7b0JBQ1ksTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO2lCQUNiOzs7OzswQkFHVCxLQUFLOzBCQUNMLEtBQUs7O3VDQXJCUjs7Ozs7OztBQ0FBO0lBbUJFO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUN2Qzs7OztJQUVELGdEQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7S0FDbEQ7Ozs7OztJQUVPLCtEQUF1Qjs7Ozs7Y0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7O2dCQTFCSixTQUFTLFNBQUM7b0JBQ0UsUUFBUSxFQUFLLHdCQUF3QjtvQkFDckMsUUFBUSxFQUFFLGlSQUt0QjtvQkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7aUJBQ2I7Ozs7OzBCQUdULEtBQUs7MEJBQ0wsS0FBSzs7d0NBakJSOzs7Ozs7O0FDQUE7SUF5Q0U7S0FDQzs7OztJQUVELGtEQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7S0FDdEQ7Ozs7O0lBR00saURBQU87Ozs7Y0FBQyxLQUF3QjtRQUNyQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTs7WUFDekQsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQzs7WUFDMUIsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUUxQixJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQzthQUNwRDs7WUFFRCxJQUFJLEtBQUssRUFBRTtnQkFDVCxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQzthQUNsQjtTQUNGOzs7Ozs7SUFHSSxvREFBVTs7OztjQUFDLElBQVk7O1FBQzVCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVqRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoQzs7Ozs7OztJQUdLLGlFQUF1Qjs7Ozs7Y0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7O2dCQXZFSixTQUFTLFNBQUM7b0JBQ0UsUUFBUSxFQUFLLDJCQUEyQjtvQkFDeEMsUUFBUSxFQUFFLGszQkF5QnRCO29CQUNZLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztpQkFDYjs7Ozs7MEJBR1QsS0FBSzs0QkFDTCxLQUFLOzswQ0F2Q1I7Ozs7Ozs7QUNBQTtJQXNDRTtLQUNDOzs7O0lBRUQsaURBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDaEQ7Ozs7OztJQUVPLGdFQUF1Qjs7Ozs7Y0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7O2dCQXBESixTQUFTLFNBQUM7b0JBQ0UsUUFBUSxFQUFLLHlCQUF5QjtvQkFDdEMsUUFBUSxFQUFFLDZ6QkF1QnRCO29CQUNZLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztpQkFDYjs7Ozs7d0JBR1QsS0FBSzs4QkFDTCxLQUFLOzBCQUNMLEtBQUs7O3lDQXBDUjs7Ozs7OztBQ0FBOzs7O2dCQXVCQyxRQUFRLFNBQUM7b0JBQ0UsT0FBTyxFQUFPO3dCQUNaLFlBQVk7d0JBQ1osV0FBVzt3QkFDWCxtQkFBbUI7d0JBQ25CLGNBQWM7d0JBQ2Qsa0JBQWtCO3dCQUNsQixhQUFhO3dCQUNiLGNBQWM7d0JBQ2QsZUFBZTt3QkFDZixlQUFlO3dCQUNmLG9CQUFvQjt3QkFDcEIsaUJBQWlCO3dCQUNqQixlQUFlO3FCQUNoQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osMEJBQTBCO3dCQUMxQiwrQkFBK0I7d0JBQy9CLDJCQUEyQjt3QkFDM0IsNEJBQTRCO3dCQUM1Qiw0QkFBNEI7d0JBQzVCLDZCQUE2Qjt3QkFDN0IsK0JBQStCO3dCQUMvQiw4QkFBOEI7cUJBQy9CO29CQUNELE9BQU8sRUFBTzt3QkFDWiwwQkFBMEI7d0JBQzFCLCtCQUErQjt3QkFDL0IsMkJBQTJCO3dCQUMzQiw0QkFBNEI7d0JBQzVCLDRCQUE0Qjt3QkFDNUIsNkJBQTZCO3dCQUM3QiwrQkFBK0I7d0JBQy9CLDhCQUE4QjtxQkFDL0I7aUJBQ0Y7OzhCQTFEWDs7Ozs7OztBQ0FBOzs7O0FBRUEsK0JBQXNDLEtBQVU7O0lBQzlDLElBQU0sT0FBTyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7SUFDbEMsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7UUFDN0IsT0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQzdCO1NBQU07UUFDTCxPQUFPLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDMUI7Q0FDRjs7Ozs7Ozs7Ozs7Ozs7In0=