/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { BootstartTextFormComponent } from './components/bootstart-text-form/bootstart-text-form.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatOptionModule, MatSelectModule, MatSlideToggleModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { BootstartParagraphFormComponent } from './components/bootstart-paragraph-form/bootstart-paragraph-form.component';
import { BootstartChipsFormComponent } from './components/bootstart-chips-form/bootstart-chips-form.component';
import { BootstartSelectFormComponent } from './components/bootstart-select-form/bootstart-select-form.component';
import { BootstartNumberFormComponent } from './components/bootstart-number-form/bootstart-number-form.component';
import { BootstartBooleanFormComponent } from './components/bootstart-boolean-form/bootstart-boolean-form.component';
import { BootstartChipsTextFormComponent } from './components/bootstart-chips-text-form/bootstart-chips-text-form.component';
import { BootstartCurrencyFormComponent } from './components/bootstart-currency-form/bootstart-currency-form.component';
import { NgxCurrencyModule } from 'ngx-currency';
var BootstartFormModule = /** @class */ (function () {
    function BootstartFormModule() {
    }
    BootstartFormModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                        MatInputModule,
                        MatFormFieldModule,
                        MatIconModule,
                        MatChipsModule,
                        MatSelectModule,
                        MatOptionModule,
                        MatSlideToggleModule,
                        NgxCurrencyModule,
                        TranslateModule
                    ],
                    declarations: [
                        BootstartTextFormComponent,
                        BootstartParagraphFormComponent,
                        BootstartChipsFormComponent,
                        BootstartSelectFormComponent,
                        BootstartNumberFormComponent,
                        BootstartBooleanFormComponent,
                        BootstartChipsTextFormComponent,
                        BootstartCurrencyFormComponent
                    ],
                    exports: [
                        BootstartTextFormComponent,
                        BootstartParagraphFormComponent,
                        BootstartChipsFormComponent,
                        BootstartSelectFormComponent,
                        BootstartNumberFormComponent,
                        BootstartBooleanFormComponent,
                        BootstartChipsTextFormComponent,
                        BootstartCurrencyFormComponent
                    ]
                },] },
    ];
    return BootstartFormModule;
}());
export { BootstartFormModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZvcm0ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvYm9vdHN0YXJ0LWZvcm0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQywwQkFBMEIsRUFBQyxNQUFNLGdFQUFnRSxDQUFDO0FBQzFHLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsV0FBVyxFQUFFLG1CQUFtQixFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDaEUsT0FBTyxFQUNMLGNBQWMsRUFDZCxrQkFBa0IsRUFDbEIsYUFBYSxFQUNiLGNBQWMsRUFDZCxlQUFlLEVBQ2YsZUFBZSxFQUNmLG9CQUFvQixFQUNyQixNQUFNLG1CQUFtQixDQUFDO0FBQzNCLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUNwRCxPQUFPLEVBQUMsK0JBQStCLEVBQUMsTUFBTSwwRUFBMEUsQ0FBQztBQUN6SCxPQUFPLEVBQUMsMkJBQTJCLEVBQUMsTUFBTSxrRUFBa0UsQ0FBQztBQUM3RyxPQUFPLEVBQUMsNEJBQTRCLEVBQUMsTUFBTSxvRUFBb0UsQ0FBQztBQUNoSCxPQUFPLEVBQUMsNEJBQTRCLEVBQUMsTUFBTSxvRUFBb0UsQ0FBQztBQUNoSCxPQUFPLEVBQUMsNkJBQTZCLEVBQUMsTUFBTSxzRUFBc0UsQ0FBQztBQUNuSCxPQUFPLEVBQUMsK0JBQStCLEVBQUMsTUFBTSw0RUFBNEUsQ0FBQztBQUMzSCxPQUFPLEVBQUMsOEJBQThCLEVBQUMsTUFBTSx3RUFBd0UsQ0FBQztBQUN0SCxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxjQUFjLENBQUM7Ozs7O2dCQUU5QyxRQUFRLFNBQUM7b0JBQ0UsT0FBTyxFQUFPO3dCQUNaLFlBQVk7d0JBQ1osV0FBVzt3QkFDWCxtQkFBbUI7d0JBQ25CLGNBQWM7d0JBQ2Qsa0JBQWtCO3dCQUNsQixhQUFhO3dCQUNiLGNBQWM7d0JBQ2QsZUFBZTt3QkFDZixlQUFlO3dCQUNmLG9CQUFvQjt3QkFDcEIsaUJBQWlCO3dCQUNqQixlQUFlO3FCQUNoQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1osMEJBQTBCO3dCQUMxQiwrQkFBK0I7d0JBQy9CLDJCQUEyQjt3QkFDM0IsNEJBQTRCO3dCQUM1Qiw0QkFBNEI7d0JBQzVCLDZCQUE2Qjt3QkFDN0IsK0JBQStCO3dCQUMvQiw4QkFBOEI7cUJBQy9CO29CQUNELE9BQU8sRUFBTzt3QkFDWiwwQkFBMEI7d0JBQzFCLCtCQUErQjt3QkFDL0IsMkJBQTJCO3dCQUMzQiw0QkFBNEI7d0JBQzVCLDRCQUE0Qjt3QkFDNUIsNkJBQTZCO3dCQUM3QiwrQkFBK0I7d0JBQy9CLDhCQUE4QjtxQkFDL0I7aUJBQ0Y7OzhCQTFEWDs7U0EyRGEsbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydFRleHRGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXRleHQtZm9ybS9ib290c3RhcnQtdGV4dC1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7XG4gIE1hdENoaXBzTW9kdWxlLFxuICBNYXRGb3JtRmllbGRNb2R1bGUsXG4gIE1hdEljb25Nb2R1bGUsXG4gIE1hdElucHV0TW9kdWxlLFxuICBNYXRPcHRpb25Nb2R1bGUsXG4gIE1hdFNlbGVjdE1vZHVsZSxcbiAgTWF0U2xpZGVUb2dnbGVNb2R1bGVcbn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHtUcmFuc2xhdGVNb2R1bGV9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRQYXJhZ3JhcGhGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXBhcmFncmFwaC1mb3JtL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRDaGlwc0Zvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtY2hpcHMtZm9ybS9ib290c3RhcnQtY2hpcHMtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRTZWxlY3RGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXNlbGVjdC1mb3JtL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnROdW1iZXJGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LW51bWJlci1mb3JtL2Jvb3RzdGFydC1udW1iZXItZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRCb29sZWFuRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0vYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRDaGlwc1RleHRGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS9ib290c3RhcnQtY2hpcHMtdGV4dC1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1jdXJyZW5jeS1mb3JtL2Jvb3RzdGFydC1jdXJyZW5jeS1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge05neEN1cnJlbmN5TW9kdWxlfSBmcm9tICduZ3gtY3VycmVuY3knO1xuXG5ATmdNb2R1bGUoe1xuICAgICAgICAgICAgaW1wb3J0cyAgICAgOiBbXG4gICAgICAgICAgICAgIENvbW1vbk1vZHVsZSxcbiAgICAgICAgICAgICAgRm9ybXNNb2R1bGUsXG4gICAgICAgICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdElucHV0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRGb3JtRmllbGRNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdEljb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdENoaXBzTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRTZWxlY3RNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdE9wdGlvbk1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0U2xpZGVUb2dnbGVNb2R1bGUsXG4gICAgICAgICAgICAgIE5neEN1cnJlbmN5TW9kdWxlLFxuICAgICAgICAgICAgICBUcmFuc2xhdGVNb2R1bGVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0VGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydFBhcmFncmFwaEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzVGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGV4cG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBCb290c3RhcnRUZXh0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0UGFyYWdyYXBoRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q2hpcHNGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRTZWxlY3RGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnROdW1iZXJGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRCb29sZWFuRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q3VycmVuY3lGb3JtQ29tcG9uZW50XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRGb3JtTW9kdWxlIHtcbn1cbiJdfQ==