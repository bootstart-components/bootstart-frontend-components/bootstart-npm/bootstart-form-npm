/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
var BootstartTextFormComponent = /** @class */ (function () {
    function BootstartTextFormComponent() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    BootstartTextFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('width', 100);
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartTextFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartTextFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-text-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <input matInput type=\"text\" [formControl]=\"control\" [required]=\"options.required\">\n</mat-form-field>\n",
                    styles: [".full-width{width:100%}"]
                },] },
    ];
    /** @nocollapse */
    BootstartTextFormComponent.ctorParameters = function () { return []; };
    BootstartTextFormComponent.propDecorators = {
        control: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartTextFormComponent;
}());
export { BootstartTextFormComponent };
if (false) {
    /** @type {?} */
    BootstartTextFormComponent.prototype.control;
    /** @type {?} */
    BootstartTextFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LXRleHQtZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Jvb3RzdGFydC10ZXh0LWZvcm0vYm9vdHN0YXJ0LXRleHQtZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQzs7SUFzQnpDO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQzs7OztJQUVELDZDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7S0FDNUM7Ozs7OztJQUdPLDREQUF1Qjs7Ozs7Y0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7Z0JBL0JKLFNBQVMsU0FBQztvQkFDRSxRQUFRLEVBQUsscUJBQXFCO29CQUNsQyxRQUFRLEVBQUUsd1lBU3RCO29CQUNZLE1BQU0sRUFBRSxDQUFDLHlCQUF5QixDQUFDO2lCQUNwQzs7Ozs7MEJBR1QsS0FBSzswQkFDTCxLQUFLOztxQ0FyQlI7O1NBa0JhLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Qm9vdHN0YXJ0VGV4dEZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtdGV4dC1mb3JtLW9wdGlvbnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC10ZXh0LWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8aW5wdXQgbWF0SW5wdXQgdHlwZT1cInRleHRcIiBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiIFtyZXF1aXJlZF09XCJvcHRpb25zLnJlcXVpcmVkXCI+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgLmZ1bGwtd2lkdGh7d2lkdGg6MTAwJX1gXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydFRleHRGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBjb250cm9sOiBGb3JtQ29udHJvbDtcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0VGV4dEZvcm1PcHRpb25zO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuY29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgnJyk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCd3aWR0aCcsIDEwMCk7XG4gIH1cblxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iXX0=