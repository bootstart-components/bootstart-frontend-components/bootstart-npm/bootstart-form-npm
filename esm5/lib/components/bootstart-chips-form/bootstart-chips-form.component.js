/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { FormArray, FormControl } from '@angular/forms';
var BootstartChipsFormComponent = /** @class */ (function () {
    function BootstartChipsFormComponent() {
        this.selection = new SelectionModel(true);
    }
    /**
     * @return {?}
     */
    BootstartChipsFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('choicesTranslatePrefix', '');
        this._replaceUndefinedOption('maximumSelectable', Infinity);
        (_a = this.selection).select.apply(_a, tslib_1.__spread(this.formArray.value));
        var _a;
    };
    /**
     * What happens when selection changes
     * @param {?} value Chip's value that has changed.
     * @return {?}
     */
    BootstartChipsFormComponent.prototype.selectionChanges = /**
     * What happens when selection changes
     * @param {?} value Chip's value that has changed.
     * @return {?}
     */
    function (value) {
        if (this.selection.selected.length >= this.options.maximumSelectable && !this.selection.isSelected(value)) {
            return;
        }
        this.selection.toggle(value);
        if (this.selection.isSelected(value)) {
            this.formArray.push(new FormControl(value));
        }
        else {
            this.formArray.removeAt(this.formArray.controls.findIndex(function (x) { return x.value === value; }));
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    BootstartChipsFormComponent.prototype.isClickable = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (this.selection.selected.length >= this.options.maximumSelectable) {
            return this.selection.isSelected(value);
        }
        return true;
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartChipsFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartChipsFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-chips-form',
                    template: "<mat-chip-list>\n  <mat-chip *ngFor=\"let choice of options.choices\"\n            (click)=\"selectionChanges(choice)\" selected\n            [disabled]=\"selection.selected.length >= options.maximumSelectable && !selection.isSelected(choice)\"\n            [ngClass]=\"isClickable(choice) ? 'bootstart-clickable-chip' : 'bootstart-non-clickable-chip'\"\n            [color]=\"selection.isSelected(choice) ? 'primary' : 'none'\">\n\n    <mat-chip-avatar *ngIf=\"selection.isSelected(choice)\">\n      <span class=\"fas fa-check\"></span>\n    </mat-chip-avatar>\n\n    {{options.choicesTranslatePrefix + choice|translate}}\n  </mat-chip>\n</mat-chip-list>\n",
                    styles: [".bootstart-clickable-chip{cursor:pointer}.bootstart-non-clickable-chip{cursor:not-allowed}"]
                },] },
    ];
    /** @nocollapse */
    BootstartChipsFormComponent.ctorParameters = function () { return []; };
    BootstartChipsFormComponent.propDecorators = {
        formArray: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartChipsFormComponent;
}());
export { BootstartChipsFormComponent };
if (false) {
    /** @type {?} */
    BootstartChipsFormComponent.prototype.formArray;
    /** @type {?} */
    BootstartChipsFormComponent.prototype.options;
    /** @type {?} */
    BootstartChipsFormComponent.prototype.selection;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWNoaXBzLWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ib290c3RhcnQtY2hpcHMtZm9ybS9ib290c3RhcnQtY2hpcHMtZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUN2RCxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFFeEQsT0FBTyxFQUFDLFNBQVMsRUFBRSxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQzs7SUE2QnBEO3lCQUhZLElBQUksY0FBYyxDQUFTLElBQUksQ0FBQztLQUkzQzs7OztJQUVELDhDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsbUJBQW1CLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDNUQsQ0FBQSxLQUFBLElBQUksQ0FBQyxTQUFTLENBQUEsQ0FBQyxNQUFNLDRCQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFFOztLQUNoRDs7Ozs7O0lBTU0sc0RBQWdCOzs7OztjQUFDLEtBQWE7UUFDbkMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUcsTUFBTSxDQUFDO1NBQ1I7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztTQUM3QztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxLQUFLLEVBQWpCLENBQWlCLENBQUMsQ0FBQyxDQUFDO1NBQ3BGOzs7Ozs7SUFHSSxpREFBVzs7OztjQUFDLEtBQWE7UUFDOUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN6QztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7Ozs7Ozs7SUFJTiw2REFBdUI7Ozs7O2NBQUMsS0FBVSxFQUFFLFlBQWlCO1FBQzNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7O2dCQS9ESixTQUFTLFNBQUM7b0JBQ0UsUUFBUSxFQUFLLHNCQUFzQjtvQkFDbkMsUUFBUSxFQUFFLG1wQkFjdEI7b0JBQ1ksTUFBTSxFQUFFLENBQUMsNEZBQTRGLENBQUM7aUJBQ3ZHOzs7Ozs0QkFHVCxLQUFLOzBCQUNMLEtBQUs7O3NDQTNCUjs7U0F3QmEsMkJBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtTZWxlY3Rpb25Nb2RlbH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zJztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LWNoaXBzLWZvcm0tb3B0aW9ucyc7XG5pbXBvcnQge0Zvcm1BcnJheSwgRm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtY2hpcHMtZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWNoaXAtbGlzdD5cbiAgPG1hdC1jaGlwICpuZ0Zvcj1cImxldCBjaG9pY2Ugb2Ygb3B0aW9ucy5jaG9pY2VzXCJcbiAgICAgICAgICAgIChjbGljayk9XCJzZWxlY3Rpb25DaGFuZ2VzKGNob2ljZSlcIiBzZWxlY3RlZFxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInNlbGVjdGlvbi5zZWxlY3RlZC5sZW5ndGggPj0gb3B0aW9ucy5tYXhpbXVtU2VsZWN0YWJsZSAmJiAhc2VsZWN0aW9uLmlzU2VsZWN0ZWQoY2hvaWNlKVwiXG4gICAgICAgICAgICBbbmdDbGFzc109XCJpc0NsaWNrYWJsZShjaG9pY2UpID8gJ2Jvb3RzdGFydC1jbGlja2FibGUtY2hpcCcgOiAnYm9vdHN0YXJ0LW5vbi1jbGlja2FibGUtY2hpcCdcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInNlbGVjdGlvbi5pc1NlbGVjdGVkKGNob2ljZSkgPyAncHJpbWFyeScgOiAnbm9uZSdcIj5cblxuICAgIDxtYXQtY2hpcC1hdmF0YXIgKm5nSWY9XCJzZWxlY3Rpb24uaXNTZWxlY3RlZChjaG9pY2UpXCI+XG4gICAgICA8c3BhbiBjbGFzcz1cImZhcyBmYS1jaGVja1wiPjwvc3Bhbj5cbiAgICA8L21hdC1jaGlwLWF2YXRhcj5cblxuICAgIHt7b3B0aW9ucy5jaG9pY2VzVHJhbnNsYXRlUHJlZml4ICsgY2hvaWNlfHRyYW5zbGF0ZX19XG4gIDwvbWF0LWNoaXA+XG48L21hdC1jaGlwLWxpc3Q+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2AuYm9vdHN0YXJ0LWNsaWNrYWJsZS1jaGlwe2N1cnNvcjpwb2ludGVyfS5ib290c3RhcnQtbm9uLWNsaWNrYWJsZS1jaGlwe2N1cnNvcjpub3QtYWxsb3dlZH1gXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydENoaXBzRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgZm9ybUFycmF5OiBGb3JtQXJyYXk7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydENoaXBzRm9ybU9wdGlvbnM7XG5cbiAgc2VsZWN0aW9uID0gbmV3IFNlbGVjdGlvbk1vZGVsPHN0cmluZz4odHJ1ZSk7XG5cblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2Nob2ljZXNUcmFuc2xhdGVQcmVmaXgnLCAnJyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignbWF4aW11bVNlbGVjdGFibGUnLCBJbmZpbml0eSk7XG4gICAgdGhpcy5zZWxlY3Rpb24uc2VsZWN0KC4uLnRoaXMuZm9ybUFycmF5LnZhbHVlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBXaGF0IGhhcHBlbnMgd2hlbiBzZWxlY3Rpb24gY2hhbmdlc1xuICAgKiBAcGFyYW0gdmFsdWUgQ2hpcCdzIHZhbHVlIHRoYXQgaGFzIGNoYW5nZWQuXG4gICAqL1xuICBwdWJsaWMgc2VsZWN0aW9uQ2hhbmdlcyh2YWx1ZTogc3RyaW5nKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLnNlbGVjdGVkLmxlbmd0aCA+PSB0aGlzLm9wdGlvbnMubWF4aW11bVNlbGVjdGFibGUgJiYgIXRoaXMuc2VsZWN0aW9uLmlzU2VsZWN0ZWQodmFsdWUpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuc2VsZWN0aW9uLnRvZ2dsZSh2YWx1ZSk7XG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLmlzU2VsZWN0ZWQodmFsdWUpKSB7XG4gICAgICB0aGlzLmZvcm1BcnJheS5wdXNoKG5ldyBGb3JtQ29udHJvbCh2YWx1ZSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmZvcm1BcnJheS5yZW1vdmVBdCh0aGlzLmZvcm1BcnJheS5jb250cm9scy5maW5kSW5kZXgoeCA9PiB4LnZhbHVlID09PSB2YWx1ZSkpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBpc0NsaWNrYWJsZSh2YWx1ZTogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLnNlbGVjdGVkLmxlbmd0aCA+PSB0aGlzLm9wdGlvbnMubWF4aW11bVNlbGVjdGFibGUpIHtcbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGlvbi5pc1NlbGVjdGVkKHZhbHVlKTtcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iXX0=