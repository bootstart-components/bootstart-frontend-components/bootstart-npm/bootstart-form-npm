/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
var BootstartChipsTextFormComponent = /** @class */ (function () {
    function BootstartChipsTextFormComponent() {
    }
    /**
     * @return {?}
     */
    BootstartChipsTextFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('maximum', Infinity);
        this._replaceUndefinedOption('separator', [ENTER, COMMA]);
        this._replaceUndefinedOption('chipColor', 'primary');
    };
    /**
     * @param {?} event
     * @return {?}
     */
    BootstartChipsTextFormComponent.prototype.addChip = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.formArray.controls.length < this.options.maximum) {
            /** @type {?} */
            var input = event.input;
            /** @type {?} */
            var value = event.value;
            if ((value || '').trim()) {
                this.formArray.push(new FormControl(value.trim()));
            }
            // Reset the input value
            if (input) {
                input.value = '';
            }
        }
    };
    /**
     * @param {?} chip
     * @return {?}
     */
    BootstartChipsTextFormComponent.prototype.removeChip = /**
     * @param {?} chip
     * @return {?}
     */
    function (chip) {
        /** @type {?} */
        var index = this.formArray.value.indexOf(chip);
        if (index >= 0) {
            this.formArray.removeAt(index);
        }
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartChipsTextFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartChipsTextFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-chips-text-form',
                    template: "<mat-label>\n  <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n  <span *ngIf=\"options.icon\">&nbsp;</span>\n  <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n</mat-label>\n\n<mat-chip-list #chipList>\n  <mat-chip *ngFor=\"let chip of formArray.value\"\n            selected\n            [color]=\"options.chipColor\"\n            [selectable]=\"false\"\n            [removable]=\"true\"\n            (removed)=\"removeChip(chip)\">\n    {{chip}}\n    <mat-icon matChipRemove>cancel</mat-icon>\n  </mat-chip>\n\n  <input matInput type=\"text\"\n         *ngIf=\"formArray.controls.length < options.maximum\"\n         placeholder=\"{{options.placeholder|translate}}\"\n         [matChipInputFor]=\"chipList\"\n         [matChipInputSeparatorKeyCodes]=\"options.separator\"\n         (matChipInputTokenEnd)=\"addChip($event)\">\n\n</mat-chip-list>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartChipsTextFormComponent.ctorParameters = function () { return []; };
    BootstartChipsTextFormComponent.propDecorators = {
        options: [{ type: Input }],
        formArray: [{ type: Input }]
    };
    return BootstartChipsTextFormComponent;
}());
export { BootstartChipsTextFormComponent };
if (false) {
    /** @type {?} */
    BootstartChipsTextFormComponent.prototype.options;
    /** @type {?} */
    BootstartChipsTextFormComponent.prototype.formArray;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1jaGlwcy10ZXh0LWZvcm0vYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBRXZELE9BQU8sRUFBQyxTQUFTLEVBQUUsV0FBVyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEQsT0FBTyxFQUFDLEtBQUssRUFBRSxLQUFLLEVBQUMsTUFBTSx1QkFBdUIsQ0FBQzs7SUFzQ2pEO0tBQ0M7Ozs7SUFFRCxrREFBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0tBQ3REOzs7OztJQUdNLGlEQUFPOzs7O2NBQUMsS0FBd0I7UUFDckMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs7WUFDMUQsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQzs7WUFDMUIsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUUxQixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDcEQ7O1lBRUQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDVixLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQzthQUNsQjtTQUNGOzs7Ozs7SUFHSSxvREFBVTs7OztjQUFDLElBQVk7O1FBQzVCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVqRCxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hDOzs7Ozs7O0lBR0ssaUVBQXVCOzs7OztjQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDcEM7OztnQkF2RUosU0FBUyxTQUFDO29CQUNFLFFBQVEsRUFBSywyQkFBMkI7b0JBQ3hDLFFBQVEsRUFBRSxrM0JBeUJ0QjtvQkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7aUJBQ2I7Ozs7OzBCQUdULEtBQUs7NEJBQ0wsS0FBSzs7MENBdkNSOztTQW9DYSwrQkFBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydENoaXBzVGV4dEZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtY2hpcHMtdGV4dC1mb3JtLW9wdGlvbnMnO1xuaW1wb3J0IHtGb3JtQXJyYXksIEZvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0NPTU1BLCBFTlRFUn0gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcbmltcG9ydCB7TWF0Q2hpcElucHV0RXZlbnR9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtY2hpcHMtdGV4dC1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtbGFiZWw+XG4gIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuPC9tYXQtbGFiZWw+XG5cbjxtYXQtY2hpcC1saXN0ICNjaGlwTGlzdD5cbiAgPG1hdC1jaGlwICpuZ0Zvcj1cImxldCBjaGlwIG9mIGZvcm1BcnJheS52YWx1ZVwiXG4gICAgICAgICAgICBzZWxlY3RlZFxuICAgICAgICAgICAgW2NvbG9yXT1cIm9wdGlvbnMuY2hpcENvbG9yXCJcbiAgICAgICAgICAgIFtzZWxlY3RhYmxlXT1cImZhbHNlXCJcbiAgICAgICAgICAgIFtyZW1vdmFibGVdPVwidHJ1ZVwiXG4gICAgICAgICAgICAocmVtb3ZlZCk9XCJyZW1vdmVDaGlwKGNoaXApXCI+XG4gICAge3tjaGlwfX1cbiAgICA8bWF0LWljb24gbWF0Q2hpcFJlbW92ZT5jYW5jZWw8L21hdC1pY29uPlxuICA8L21hdC1jaGlwPlxuXG4gIDxpbnB1dCBtYXRJbnB1dCB0eXBlPVwidGV4dFwiXG4gICAgICAgICAqbmdJZj1cImZvcm1BcnJheS5jb250cm9scy5sZW5ndGggPCBvcHRpb25zLm1heGltdW1cIlxuICAgICAgICAgcGxhY2Vob2xkZXI9XCJ7e29wdGlvbnMucGxhY2Vob2xkZXJ8dHJhbnNsYXRlfX1cIlxuICAgICAgICAgW21hdENoaXBJbnB1dEZvcl09XCJjaGlwTGlzdFwiXG4gICAgICAgICBbbWF0Q2hpcElucHV0U2VwYXJhdG9yS2V5Q29kZXNdPVwib3B0aW9ucy5zZXBhcmF0b3JcIlxuICAgICAgICAgKG1hdENoaXBJbnB1dFRva2VuRW5kKT1cImFkZENoaXAoJGV2ZW50KVwiPlxuXG48L21hdC1jaGlwLWxpc3Q+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2BgXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydENoaXBzVGV4dEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydENoaXBzVGV4dEZvcm1PcHRpb25zO1xuICBASW5wdXQoKSBmb3JtQXJyYXk6IEZvcm1BcnJheTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ21heGltdW0nLCBJbmZpbml0eSk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignc2VwYXJhdG9yJywgW0VOVEVSLCBDT01NQV0pO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2NoaXBDb2xvcicsICdwcmltYXJ5Jyk7XG4gIH1cblxuXG4gIHB1YmxpYyBhZGRDaGlwKGV2ZW50OiBNYXRDaGlwSW5wdXRFdmVudCk6IHZvaWQge1xuICAgIGlmICh0aGlzLmZvcm1BcnJheS5jb250cm9scy5sZW5ndGggPCB0aGlzLm9wdGlvbnMubWF4aW11bSkge1xuICAgICAgY29uc3QgaW5wdXQgPSBldmVudC5pbnB1dDtcbiAgICAgIGNvbnN0IHZhbHVlID0gZXZlbnQudmFsdWU7XG5cbiAgICAgIGlmICgodmFsdWUgfHwgJycpLnRyaW0oKSkge1xuICAgICAgICB0aGlzLmZvcm1BcnJheS5wdXNoKG5ldyBGb3JtQ29udHJvbCh2YWx1ZS50cmltKCkpKTtcbiAgICAgIH1cbiAgICAgIC8vIFJlc2V0IHRoZSBpbnB1dCB2YWx1ZVxuICAgICAgaWYgKGlucHV0KSB7XG4gICAgICAgIGlucHV0LnZhbHVlID0gJyc7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHVibGljIHJlbW92ZUNoaXAoY2hpcDogc3RyaW5nKTogdm9pZCB7XG4gICAgY29uc3QgaW5kZXggPSB0aGlzLmZvcm1BcnJheS52YWx1ZS5pbmRleE9mKGNoaXApO1xuXG4gICAgaWYgKGluZGV4ID49IDApIHtcbiAgICAgIHRoaXMuZm9ybUFycmF5LnJlbW92ZUF0KGluZGV4KTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIl19