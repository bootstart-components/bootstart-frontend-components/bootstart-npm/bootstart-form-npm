/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
var BootstartSelectFormComponent = /** @class */ (function () {
    function BootstartSelectFormComponent() {
        this.control = new FormControl('');
        this.selectionChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    BootstartSelectFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('choicesTranslatePrefix', '');
        this._replaceUndefinedOption('width', 100);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    BootstartSelectFormComponent.prototype.selectionChanges = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.selectionChange.emit(event);
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartSelectFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartSelectFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-select-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <mat-select [required]=\"options.required\" [formControl]=\"control\" (selectionChange)=\"selectionChanges($event)\">\n    <mat-option *ngFor=\"let choice of options.choices\" value=\"{{choice}}\">\n      {{options.choicesTranslatePrefix + choice|translate}}\n    </mat-option>\n  </mat-select>\n</mat-form-field>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartSelectFormComponent.ctorParameters = function () { return []; };
    BootstartSelectFormComponent.propDecorators = {
        control: [{ type: Input }],
        options: [{ type: Input }],
        selectionChange: [{ type: Output }]
    };
    return BootstartSelectFormComponent;
}());
export { BootstartSelectFormComponent };
if (false) {
    /**
     * Associated form control
     * @type {?}
     */
    BootstartSelectFormComponent.prototype.control;
    /**
     * Select options
     * @type {?}
     */
    BootstartSelectFormComponent.prototype.options;
    /**
     * On Change emitter
     * @type {?}
     */
    BootstartSelectFormComponent.prototype.selectionChange;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LXNlbGVjdC1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXNlbGVjdC1mb3JtL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDN0UsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDOztJQWlDekM7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxZQUFZLEVBQTBCLENBQUM7S0FDbkU7Ozs7SUFFRCwrQ0FBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztLQUM1Qzs7Ozs7SUFFTSx1REFBZ0I7Ozs7Y0FBQyxLQUE2QjtRQUNuRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Ozs7OztJQUkzQiw4REFBdUI7Ozs7O2NBQUMsS0FBVSxFQUFFLFlBQWlCO1FBQzNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7O2dCQS9DSixTQUFTLFNBQUM7b0JBQ0UsUUFBUSxFQUFLLHVCQUF1QjtvQkFDcEMsUUFBUSxFQUFFLHNsQkFhdEI7b0JBQ1ksTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO2lCQUNiOzs7OzswQkFJVCxLQUFLOzBCQUdMLEtBQUs7a0NBR0wsTUFBTTs7dUNBaENUOztTQXVCYSw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnRTZWxlY3RGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LXNlbGVjdC1mb3JtLW9wdGlvbnMnO1xuaW1wb3J0IHtNYXRTZWxlY3Rpb25MaXN0Q2hhbmdlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LXNlbGVjdC1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPG1hdC1zZWxlY3QgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIiBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiIChzZWxlY3Rpb25DaGFuZ2UpPVwic2VsZWN0aW9uQ2hhbmdlcygkZXZlbnQpXCI+XG4gICAgPG1hdC1vcHRpb24gKm5nRm9yPVwibGV0IGNob2ljZSBvZiBvcHRpb25zLmNob2ljZXNcIiB2YWx1ZT1cInt7Y2hvaWNlfX1cIj5cbiAgICAgIHt7b3B0aW9ucy5jaG9pY2VzVHJhbnNsYXRlUHJlZml4ICsgY2hvaWNlfHRyYW5zbGF0ZX19XG4gICAgPC9tYXQtb3B0aW9uPlxuICA8L21hdC1zZWxlY3Q+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRTZWxlY3RGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAvKiogQXNzb2NpYXRlZCBmb3JtIGNvbnRyb2wgKi9cbiAgQElucHV0KCkgY29udHJvbDogRm9ybUNvbnRyb2w7XG5cbiAgLyoqIFNlbGVjdCBvcHRpb25zICovXG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydFNlbGVjdEZvcm1PcHRpb25zO1xuXG4gIC8qKiBPbiBDaGFuZ2UgZW1pdHRlciAqL1xuICBAT3V0cHV0KCkgc2VsZWN0aW9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8TWF0U2VsZWN0aW9uTGlzdENoYW5nZT47XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgICB0aGlzLnNlbGVjdGlvbkNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8TWF0U2VsZWN0aW9uTGlzdENoYW5nZT4oKTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2Nob2ljZXNUcmFuc2xhdGVQcmVmaXgnLCAnJyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignd2lkdGgnLCAxMDApO1xuICB9XG5cbiAgcHVibGljIHNlbGVjdGlvbkNoYW5nZXMoZXZlbnQ6IE1hdFNlbGVjdGlvbkxpc3RDaGFuZ2UpOiB2b2lkIHtcbiAgICB0aGlzLnNlbGVjdGlvbkNoYW5nZS5lbWl0KGV2ZW50KTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==