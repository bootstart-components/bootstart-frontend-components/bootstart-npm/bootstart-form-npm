/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
var BootstartBooleanFormComponent = /** @class */ (function () {
    function BootstartBooleanFormComponent() {
        this.control = new FormControl(false);
    }
    /**
     * @return {?}
     */
    BootstartBooleanFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('color', 'primary');
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartBooleanFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartBooleanFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-boolean-form',
                    template: "<mat-slide-toggle [formControl]=\"control\" [color]=\"options.color\">\n  <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n  <span *ngIf=\"options.icon\">&nbsp;</span>\n  <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n</mat-slide-toggle>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartBooleanFormComponent.ctorParameters = function () { return []; };
    BootstartBooleanFormComponent.propDecorators = {
        control: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartBooleanFormComponent;
}());
export { BootstartBooleanFormComponent };
if (false) {
    /** @type {?} */
    BootstartBooleanFormComponent.prototype.control;
    /** @type {?} */
    BootstartBooleanFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0vYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQzs7SUFrQnpDO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUN2Qzs7OztJQUVELGdEQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7S0FDbEQ7Ozs7OztJQUVPLCtEQUF1Qjs7Ozs7Y0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7Z0JBMUJKLFNBQVMsU0FBQztvQkFDRSxRQUFRLEVBQUssd0JBQXdCO29CQUNyQyxRQUFRLEVBQUUsaVJBS3RCO29CQUNZLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztpQkFDYjs7Ozs7MEJBR1QsS0FBSzswQkFDTCxLQUFLOzt3Q0FqQlI7O1NBY2EsNkJBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnRCb29sZWFuRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LXNsaWRlLXRvZ2dsZSBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiIFtjb2xvcl09XCJvcHRpb25zLmNvbG9yXCI+XG4gIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuPC9tYXQtc2xpZGUtdG9nZ2xlPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRCb29sZWFuRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgY29udHJvbDogRm9ybUNvbnRyb2w7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydEJvb2xlYW5Gb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woZmFsc2UpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignY29sb3InLCAncHJpbWFyeScpO1xuICB9XG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==