/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
var BootstartCurrencyFormComponent = /** @class */ (function () {
    function BootstartCurrencyFormComponent() {
    }
    /**
     * @return {?}
     */
    BootstartCurrencyFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._replaceUndefinedOption('width', 100);
        this._replaceUndefinedOption('align', 'left');
        this._replaceUndefinedOption('allowNegative', true);
        this._replaceUndefinedOption('decimal', '.');
        this._replaceUndefinedOption('precision', 2);
        this._replaceUndefinedOption('prefix', '$ ');
        this._replaceUndefinedOption('suffix', '');
        this._replaceUndefinedOption('thousands', ',');
        this._replaceUndefinedOption('nullable', true);
    };
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    BootstartCurrencyFormComponent.prototype._replaceUndefinedOption = /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    function (field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    };
    BootstartCurrencyFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-currency-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <form [formGroup]=\"group\">\n    <input currencyMask matInput\n           formControlName=\"{{controlName}}\"\n           [required]=\"options.required\"\n           [options]=\"{\n           align: options.align,\n           allowNegative: options.allowNegative,\n           decimal: options.decimal,\n           precision: options.precision,\n           prefix: options.prefix,\n           suffix: options.suffix,\n           thousands: options.thousands,\n           nullable: options.nullable\n           }\">\n  </form>\n</mat-form-field>\n",
                    styles: [""]
                },] },
    ];
    /** @nocollapse */
    BootstartCurrencyFormComponent.ctorParameters = function () { return []; };
    BootstartCurrencyFormComponent.propDecorators = {
        group: [{ type: Input }],
        controlName: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartCurrencyFormComponent;
}());
export { BootstartCurrencyFormComponent };
if (false) {
    /** @type {?} */
    BootstartCurrencyFormComponent.prototype.group;
    /** @type {?} */
    BootstartCurrencyFormComponent.prototype.controlName;
    /** @type {?} */
    BootstartCurrencyFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ib290c3RhcnQtY3VycmVuY3ktZm9ybS9ib290c3RhcnQtY3VycmVuY3ktZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBRXZELE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQzs7SUFvQ3ZDO0tBQ0M7Ozs7SUFFRCxpREFBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztLQUNoRDs7Ozs7O0lBRU8sZ0VBQXVCOzs7OztjQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDcEM7OztnQkFwREosU0FBUyxTQUFDO29CQUNFLFFBQVEsRUFBSyx5QkFBeUI7b0JBQ3RDLFFBQVEsRUFBRSw2ekJBdUJ0QjtvQkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7aUJBQ2I7Ozs7O3dCQUdULEtBQUs7OEJBQ0wsS0FBSzswQkFDTCxLQUFLOzt5Q0FwQ1I7O1NBZ0NhLDhCQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Qm9vdHN0YXJ0Q3VycmVuY3lGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzJztcbmltcG9ydCB7Rm9ybUdyb3VwfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8Zm9ybSBbZm9ybUdyb3VwXT1cImdyb3VwXCI+XG4gICAgPGlucHV0IGN1cnJlbmN5TWFzayBtYXRJbnB1dFxuICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJ7e2NvbnRyb2xOYW1lfX1cIlxuICAgICAgICAgICBbcmVxdWlyZWRdPVwib3B0aW9ucy5yZXF1aXJlZFwiXG4gICAgICAgICAgIFtvcHRpb25zXT1cIntcbiAgICAgICAgICAgYWxpZ246IG9wdGlvbnMuYWxpZ24sXG4gICAgICAgICAgIGFsbG93TmVnYXRpdmU6IG9wdGlvbnMuYWxsb3dOZWdhdGl2ZSxcbiAgICAgICAgICAgZGVjaW1hbDogb3B0aW9ucy5kZWNpbWFsLFxuICAgICAgICAgICBwcmVjaXNpb246IG9wdGlvbnMucHJlY2lzaW9uLFxuICAgICAgICAgICBwcmVmaXg6IG9wdGlvbnMucHJlZml4LFxuICAgICAgICAgICBzdWZmaXg6IG9wdGlvbnMuc3VmZml4LFxuICAgICAgICAgICB0aG91c2FuZHM6IG9wdGlvbnMudGhvdXNhbmRzLFxuICAgICAgICAgICBudWxsYWJsZTogb3B0aW9ucy5udWxsYWJsZVxuICAgICAgICAgICB9XCI+XG4gIDwvZm9ybT5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2BgXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgZ3JvdXA6IEZvcm1Hcm91cDtcbiAgQElucHV0KCkgY29udHJvbE5hbWU6IHN0cmluZztcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0Q3VycmVuY3lGb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdhbGlnbicsICdsZWZ0Jyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignYWxsb3dOZWdhdGl2ZScsIHRydWUpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2RlY2ltYWwnLCAnLicpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3ByZWNpc2lvbicsIDIpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3ByZWZpeCcsICckICcpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3N1ZmZpeCcsICcnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCd0aG91c2FuZHMnLCAnLCcpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ251bGxhYmxlJywgdHJ1ZSk7XG4gIH1cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIl19