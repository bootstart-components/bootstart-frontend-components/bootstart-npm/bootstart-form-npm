/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ChipFomOptions() { }
/**
 * Optional question label
 * @type {?|undefined}
 */
ChipFomOptions.prototype.questionLabel;
/**
 * Optional question icon
 * @type {?|undefined}
 */
ChipFomOptions.prototype.questionIcon;
/**
 * Maximum number of selectable items
 * @type {?}
 */
ChipFomOptions.prototype.maximumSelectable;
/**
 * List of possible choices
 * @type {?}
 */
ChipFomOptions.prototype.choices;
/**
 * Optional prefix for choice translation
 * @type {?|undefined}
 */
ChipFomOptions.prototype.choicesTranslatePrefix;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpcC1mb20tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9jaGlwcy9jaGlwLWZvbS1vcHRpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIENoaXBGb21PcHRpb25zIHtcbiAgLyoqIE9wdGlvbmFsIHF1ZXN0aW9uIGxhYmVsICovXG4gIHF1ZXN0aW9uTGFiZWw/OiBzdHJpbmc7XG5cbiAgLyoqIE9wdGlvbmFsIHF1ZXN0aW9uIGljb24gKi9cbiAgcXVlc3Rpb25JY29uPzogc3RyaW5nO1xuXG4gIC8qKiBNYXhpbXVtIG51bWJlciBvZiBzZWxlY3RhYmxlIGl0ZW1zICovXG4gIG1heGltdW1TZWxlY3RhYmxlOiBudW1iZXI7XG5cbiAgLyoqIExpc3Qgb2YgcG9zc2libGUgY2hvaWNlcyAqL1xuICBjaG9pY2VzOiBzdHJpbmdbXTtcblxuICAvKiogT3B0aW9uYWwgcHJlZml4IGZvciBjaG9pY2UgdHJhbnNsYXRpb24gKi9cbiAgY2hvaWNlc1RyYW5zbGF0ZVByZWZpeD86IHN0cmluZztcbn1cbiJdfQ==