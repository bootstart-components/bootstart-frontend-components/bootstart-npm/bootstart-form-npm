/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BooleanFormOptions() { }
/**
 * Optional question label
 * @type {?|undefined}
 */
BooleanFormOptions.prototype.label;
/**
 * Optional question icon
 * @type {?|undefined}
 */
BooleanFormOptions.prototype.icon;
/**
 * Toggler's color
 * @type {?|undefined}
 */
BooleanFormOptions.prototype.color;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vbGVhbi1mb3JtLW9wdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvb3B0aW9ucy9ib29sZWFuLWZvcm0tb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBCb29sZWFuRm9ybU9wdGlvbnMge1xuXG4gIC8qKiBPcHRpb25hbCBxdWVzdGlvbiBsYWJlbCAqL1xuICBsYWJlbD86IHN0cmluZztcblxuICAvKiogT3B0aW9uYWwgcXVlc3Rpb24gaWNvbiAqL1xuICBpY29uPzogc3RyaW5nO1xuXG4gIC8qKiBUb2dnbGVyJ3MgY29sb3IgKi9cbiAgY29sb3I/OiBzdHJpbmc7XG5cbn1cbiJdfQ==