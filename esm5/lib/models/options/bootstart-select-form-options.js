/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartSelectFormOptions() { }
/**
 * Question label.
 * @type {?|undefined}
 */
BootstartSelectFormOptions.prototype.label;
/**
 * Icon of the question.
 * @type {?|undefined}
 */
BootstartSelectFormOptions.prototype.icon;
/**
 * Is this question required?
 * @type {?|undefined}
 */
BootstartSelectFormOptions.prototype.required;
/**
 * Width of the field (as percentage)
 * @type {?|undefined}
 */
BootstartSelectFormOptions.prototype.width;
/**
 * List of possible choices.
 * @type {?}
 */
BootstartSelectFormOptions.prototype.choices;
/**
 * Translation prefix for each choice.
 * @type {?|undefined}
 */
BootstartSelectFormOptions.prototype.choicesTranslatePrefix;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LXNlbGVjdC1mb3JtLW9wdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtc2VsZWN0LWZvcm0tb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBCb290c3RhcnRTZWxlY3RGb3JtT3B0aW9ucyB7XG5cbiAgLyoqXG4gICAqIFF1ZXN0aW9uIGxhYmVsLlxuICAgKi9cbiAgbGFiZWw/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJY29uIG9mIHRoZSBxdWVzdGlvbi5cbiAgICovXG4gIGljb24/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJcyB0aGlzIHF1ZXN0aW9uIHJlcXVpcmVkP1xuICAgKi9cbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xuICAvKipcbiAgICogV2lkdGggb2YgdGhlIGZpZWxkIChhcyBwZXJjZW50YWdlKVxuICAgKi9cbiAgd2lkdGg/OiBudW1iZXI7XG4gIC8qKlxuICAgKiBMaXN0IG9mIHBvc3NpYmxlIGNob2ljZXMuXG4gICAqL1xuICBjaG9pY2VzOiBzdHJpbmdbXTtcbiAgLyoqXG4gICAqIFRyYW5zbGF0aW9uIHByZWZpeCBmb3IgZWFjaCBjaG9pY2UuXG4gICAqL1xuICBjaG9pY2VzVHJhbnNsYXRlUHJlZml4Pzogc3RyaW5nO1xuXG59XG4iXX0=