/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ChipsFormOptions() { }
/**
 * List of possible choices
 * @type {?}
 */
ChipsFormOptions.prototype.choices;
/**
 * Maximum number of selectable items
 * @type {?|undefined}
 */
ChipsFormOptions.prototype.maximumSelectable;
/**
 * Optional prefix for choice translation
 * @type {?|undefined}
 */
ChipsFormOptions.prototype.choicesTranslatePrefix;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpcHMtZm9ybS1vcHRpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL29wdGlvbnMvY2hpcHMtZm9ybS1vcHRpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIENoaXBzRm9ybU9wdGlvbnMge1xuXG4gIC8qKiBMaXN0IG9mIHBvc3NpYmxlIGNob2ljZXMgKi9cbiAgY2hvaWNlczogc3RyaW5nW107XG5cbiAgLyoqIE1heGltdW0gbnVtYmVyIG9mIHNlbGVjdGFibGUgaXRlbXMgKi9cbiAgbWF4aW11bVNlbGVjdGFibGU/OiBudW1iZXI7XG5cbiAgLyoqIE9wdGlvbmFsIHByZWZpeCBmb3IgY2hvaWNlIHRyYW5zbGF0aW9uICovXG4gIGNob2ljZXNUcmFuc2xhdGVQcmVmaXg/OiBzdHJpbmc7XG5cbn1cbiJdfQ==