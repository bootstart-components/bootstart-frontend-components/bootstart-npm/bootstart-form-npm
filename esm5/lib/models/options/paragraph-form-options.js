/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ParagraphFormOptions() { }
/**
 * Optional question label
 * @type {?|undefined}
 */
ParagraphFormOptions.prototype.label;
/**
 * Optional question icon
 * @type {?|undefined}
 */
ParagraphFormOptions.prototype.icon;
/**
 * Is the question required?
 * @type {?|undefined}
 */
ParagraphFormOptions.prototype.required;
/**
 * Question field's width in percent
 * @type {?|undefined}
 */
ParagraphFormOptions.prototype.width;
/**
 * Initial number of rows
 * @type {?|undefined}
 */
ParagraphFormOptions.prototype.rows;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFyYWdyYXBoLWZvcm0tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9vcHRpb25zL3BhcmFncmFwaC1mb3JtLW9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgUGFyYWdyYXBoRm9ybU9wdGlvbnMge1xuXG4gIC8qKiBPcHRpb25hbCBxdWVzdGlvbiBsYWJlbCAqL1xuICBsYWJlbD86IHN0cmluZztcblxuICAvKiogT3B0aW9uYWwgcXVlc3Rpb24gaWNvbiAqL1xuICBpY29uPzogc3RyaW5nO1xuXG4gIC8qKiBJcyB0aGUgcXVlc3Rpb24gcmVxdWlyZWQ/Ki9cbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xuXG4gIC8qKiBRdWVzdGlvbiBmaWVsZCdzIHdpZHRoIGluIHBlcmNlbnQgKi9cbiAgd2lkdGg/OiBudW1iZXI7XG5cbiAgLyoqIEluaXRpYWwgbnVtYmVyIG9mIHJvd3MgKi9cbiAgcm93cz86IG51bWJlcjtcblxufVxuIl19