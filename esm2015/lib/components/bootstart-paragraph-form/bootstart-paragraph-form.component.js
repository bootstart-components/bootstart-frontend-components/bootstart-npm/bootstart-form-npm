/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
export class BootstartParagraphFormComponent {
    constructor() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('width', 100);
        this._replaceUndefinedOption('rows', 6);
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartParagraphFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-paragraph-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <textarea matInput type="paragraph"
            [formControl]="control"
            [required]="options.required"
            rows="{{options.rows}}"></textarea>
</mat-form-field>
`,
                styles: [`.full-width{width:100%}`]
            },] },
];
/** @nocollapse */
BootstartParagraphFormComponent.ctorParameters = () => [];
BootstartParagraphFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BootstartParagraphFormComponent.prototype.control;
    /** @type {?} */
    BootstartParagraphFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LXBhcmFncmFwaC1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXBhcmFncmFwaC1mb3JtL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQW9CM0MsTUFBTTtJQUtKO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FDekM7Ozs7OztJQUdPLHVCQUF1QixDQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDcEM7Ozs7WUFuQ0osU0FBUyxTQUFDO2dCQUNFLFFBQVEsRUFBSywwQkFBMEI7Z0JBQ3ZDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7O0NBWXRCO2dCQUNZLE1BQU0sRUFBRSxDQUFDLHlCQUF5QixDQUFDO2FBQ3BDOzs7OztzQkFHVCxLQUFLO3NCQUNMLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Zvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0Jvb3RzdGFydFBhcmFncmFwaEZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtcGFyYWdyYXBoLWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LXBhcmFncmFwaC1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPHRleHRhcmVhIG1hdElucHV0IHR5cGU9XCJwYXJhZ3JhcGhcIlxuICAgICAgICAgICAgW2Zvcm1Db250cm9sXT1cImNvbnRyb2xcIlxuICAgICAgICAgICAgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIlxuICAgICAgICAgICAgcm93cz1cInt7b3B0aW9ucy5yb3dzfX1cIj48L3RleHRhcmVhPlxuPC9tYXQtZm9ybS1maWVsZD5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYC5mdWxsLXdpZHRoe3dpZHRoOjEwMCV9YF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRQYXJhZ3JhcGhGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBjb250cm9sOiBGb3JtQ29udHJvbDtcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0UGFyYWdyYXBoRm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdyb3dzJywgNik7XG4gIH1cblxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iXX0=