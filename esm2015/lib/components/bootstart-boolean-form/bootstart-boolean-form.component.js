/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
export class BootstartBooleanFormComponent {
    constructor() {
        this.control = new FormControl(false);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('color', 'primary');
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartBooleanFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-boolean-form',
                template: `<mat-slide-toggle [formControl]="control" [color]="options.color">
  <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
  <span *ngIf="options.icon">&nbsp;</span>
  <span *ngIf="options.label">{{options.label|translate}}</span>
</mat-slide-toggle>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartBooleanFormComponent.ctorParameters = () => [];
BootstartBooleanFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BootstartBooleanFormComponent.prototype.control;
    /** @type {?} */
    BootstartBooleanFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0vYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQWEzQyxNQUFNO0lBS0o7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ3ZDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7S0FDbEQ7Ozs7OztJQUVPLHVCQUF1QixDQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDcEM7Ozs7WUExQkosU0FBUyxTQUFDO2dCQUNFLFFBQVEsRUFBSyx3QkFBd0I7Z0JBQ3JDLFFBQVEsRUFBRTs7Ozs7Q0FLdEI7Z0JBQ1ksTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO2FBQ2I7Ozs7O3NCQUdULEtBQUs7c0JBQ0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Qm9vdHN0YXJ0Qm9vbGVhbkZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtYm9vbGVhbi1mb3JtLW9wdGlvbnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1ib29sZWFuLWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1zbGlkZS10b2dnbGUgW2Zvcm1Db250cm9sXT1cImNvbnRyb2xcIiBbY29sb3JdPVwib3B0aW9ucy5jb2xvclwiPlxuICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbjwvbWF0LXNsaWRlLXRvZ2dsZT5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYGBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRCb29sZWFuRm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKGZhbHNlKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2NvbG9yJywgJ3ByaW1hcnknKTtcbiAgfVxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iXX0=