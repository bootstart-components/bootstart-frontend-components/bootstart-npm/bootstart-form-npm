/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { FormArray, FormControl } from '@angular/forms';
export class BootstartChipsFormComponent {
    constructor() {
        this.selection = new SelectionModel(true);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('choicesTranslatePrefix', '');
        this._replaceUndefinedOption('maximumSelectable', Infinity);
        this.selection.select(...this.formArray.value);
    }
    /**
     * What happens when selection changes
     * @param {?} value Chip's value that has changed.
     * @return {?}
     */
    selectionChanges(value) {
        if (this.selection.selected.length >= this.options.maximumSelectable && !this.selection.isSelected(value)) {
            return;
        }
        this.selection.toggle(value);
        if (this.selection.isSelected(value)) {
            this.formArray.push(new FormControl(value));
        }
        else {
            this.formArray.removeAt(this.formArray.controls.findIndex(x => x.value === value));
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    isClickable(value) {
        if (this.selection.selected.length >= this.options.maximumSelectable) {
            return this.selection.isSelected(value);
        }
        return true;
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartChipsFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-chips-form',
                template: `<mat-chip-list>
  <mat-chip *ngFor="let choice of options.choices"
            (click)="selectionChanges(choice)" selected
            [disabled]="selection.selected.length >= options.maximumSelectable && !selection.isSelected(choice)"
            [ngClass]="isClickable(choice) ? 'bootstart-clickable-chip' : 'bootstart-non-clickable-chip'"
            [color]="selection.isSelected(choice) ? 'primary' : 'none'">

    <mat-chip-avatar *ngIf="selection.isSelected(choice)">
      <span class="fas fa-check"></span>
    </mat-chip-avatar>

    {{options.choicesTranslatePrefix + choice|translate}}
  </mat-chip>
</mat-chip-list>
`,
                styles: [`.bootstart-clickable-chip{cursor:pointer}.bootstart-non-clickable-chip{cursor:not-allowed}`]
            },] },
];
/** @nocollapse */
BootstartChipsFormComponent.ctorParameters = () => [];
BootstartChipsFormComponent.propDecorators = {
    formArray: [{ type: Input }],
    options: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BootstartChipsFormComponent.prototype.formArray;
    /** @type {?} */
    BootstartChipsFormComponent.prototype.options;
    /** @type {?} */
    BootstartChipsFormComponent.prototype.selection;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWNoaXBzLWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ib290c3RhcnQtY2hpcHMtZm9ybS9ib290c3RhcnQtY2hpcHMtZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUV4RCxPQUFPLEVBQUMsU0FBUyxFQUFFLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBcUJ0RCxNQUFNO0lBUUo7eUJBSFksSUFBSSxjQUFjLENBQVMsSUFBSSxDQUFDO0tBSTNDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsbUJBQW1CLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQ2hEOzs7Ozs7SUFNTSxnQkFBZ0IsQ0FBQyxLQUFhO1FBQ25DLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFHLE1BQU0sQ0FBQztTQUNSO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDN0M7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztTQUNwRjs7Ozs7O0lBR0ksV0FBVyxDQUFDLEtBQWE7UUFDOUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN6QztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7Ozs7Ozs7SUFJTix1QkFBdUIsQ0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7O1lBL0RKLFNBQVMsU0FBQztnQkFDRSxRQUFRLEVBQUssc0JBQXNCO2dCQUNuQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7O0NBY3RCO2dCQUNZLE1BQU0sRUFBRSxDQUFDLDRGQUE0RixDQUFDO2FBQ3ZHOzs7Ozt3QkFHVCxLQUFLO3NCQUNMLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1NlbGVjdGlvbk1vZGVsfSBmcm9tICdAYW5ndWxhci9jZGsvY29sbGVjdGlvbnMnO1xuaW1wb3J0IHtCb290c3RhcnRDaGlwc0Zvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtY2hpcHMtZm9ybS1vcHRpb25zJztcbmltcG9ydCB7Rm9ybUFycmF5LCBGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1jaGlwcy1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtY2hpcC1saXN0PlxuICA8bWF0LWNoaXAgKm5nRm9yPVwibGV0IGNob2ljZSBvZiBvcHRpb25zLmNob2ljZXNcIlxuICAgICAgICAgICAgKGNsaWNrKT1cInNlbGVjdGlvbkNoYW5nZXMoY2hvaWNlKVwiIHNlbGVjdGVkXG4gICAgICAgICAgICBbZGlzYWJsZWRdPVwic2VsZWN0aW9uLnNlbGVjdGVkLmxlbmd0aCA+PSBvcHRpb25zLm1heGltdW1TZWxlY3RhYmxlICYmICFzZWxlY3Rpb24uaXNTZWxlY3RlZChjaG9pY2UpXCJcbiAgICAgICAgICAgIFtuZ0NsYXNzXT1cImlzQ2xpY2thYmxlKGNob2ljZSkgPyAnYm9vdHN0YXJ0LWNsaWNrYWJsZS1jaGlwJyA6ICdib290c3RhcnQtbm9uLWNsaWNrYWJsZS1jaGlwJ1wiXG4gICAgICAgICAgICBbY29sb3JdPVwic2VsZWN0aW9uLmlzU2VsZWN0ZWQoY2hvaWNlKSA/ICdwcmltYXJ5JyA6ICdub25lJ1wiPlxuXG4gICAgPG1hdC1jaGlwLWF2YXRhciAqbmdJZj1cInNlbGVjdGlvbi5pc1NlbGVjdGVkKGNob2ljZSlcIj5cbiAgICAgIDxzcGFuIGNsYXNzPVwiZmFzIGZhLWNoZWNrXCI+PC9zcGFuPlxuICAgIDwvbWF0LWNoaXAtYXZhdGFyPlxuXG4gICAge3tvcHRpb25zLmNob2ljZXNUcmFuc2xhdGVQcmVmaXggKyBjaG9pY2V8dHJhbnNsYXRlfX1cbiAgPC9tYXQtY2hpcD5cbjwvbWF0LWNoaXAtbGlzdD5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYC5ib290c3RhcnQtY2xpY2thYmxlLWNoaXB7Y3Vyc29yOnBvaW50ZXJ9LmJvb3RzdGFydC1ub24tY2xpY2thYmxlLWNoaXB7Y3Vyc29yOm5vdC1hbGxvd2VkfWBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0Q2hpcHNGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBmb3JtQXJyYXk6IEZvcm1BcnJheTtcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0Q2hpcHNGb3JtT3B0aW9ucztcblxuICBzZWxlY3Rpb24gPSBuZXcgU2VsZWN0aW9uTW9kZWw8c3RyaW5nPih0cnVlKTtcblxuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignY2hvaWNlc1RyYW5zbGF0ZVByZWZpeCcsICcnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdtYXhpbXVtU2VsZWN0YWJsZScsIEluZmluaXR5KTtcbiAgICB0aGlzLnNlbGVjdGlvbi5zZWxlY3QoLi4udGhpcy5mb3JtQXJyYXkudmFsdWUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFdoYXQgaGFwcGVucyB3aGVuIHNlbGVjdGlvbiBjaGFuZ2VzXG4gICAqIEBwYXJhbSB2YWx1ZSBDaGlwJ3MgdmFsdWUgdGhhdCBoYXMgY2hhbmdlZC5cbiAgICovXG4gIHB1YmxpYyBzZWxlY3Rpb25DaGFuZ2VzKHZhbHVlOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5zZWxlY3Rpb24uc2VsZWN0ZWQubGVuZ3RoID49IHRoaXMub3B0aW9ucy5tYXhpbXVtU2VsZWN0YWJsZSAmJiAhdGhpcy5zZWxlY3Rpb24uaXNTZWxlY3RlZCh2YWx1ZSkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5zZWxlY3Rpb24udG9nZ2xlKHZhbHVlKTtcbiAgICBpZiAodGhpcy5zZWxlY3Rpb24uaXNTZWxlY3RlZCh2YWx1ZSkpIHtcbiAgICAgIHRoaXMuZm9ybUFycmF5LnB1c2gobmV3IEZvcm1Db250cm9sKHZhbHVlKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZm9ybUFycmF5LnJlbW92ZUF0KHRoaXMuZm9ybUFycmF5LmNvbnRyb2xzLmZpbmRJbmRleCh4ID0+IHgudmFsdWUgPT09IHZhbHVlKSk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGlzQ2xpY2thYmxlKHZhbHVlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBpZiAodGhpcy5zZWxlY3Rpb24uc2VsZWN0ZWQubGVuZ3RoID49IHRoaXMub3B0aW9ucy5tYXhpbXVtU2VsZWN0YWJsZSkge1xuICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0aW9uLmlzU2VsZWN0ZWQodmFsdWUpO1xuICAgIH1cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==