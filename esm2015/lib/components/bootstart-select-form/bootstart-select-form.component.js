/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
export class BootstartSelectFormComponent {
    constructor() {
        this.control = new FormControl('');
        this.selectionChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('choicesTranslatePrefix', '');
        this._replaceUndefinedOption('width', 100);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    selectionChanges(event) {
        this.selectionChange.emit(event);
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartSelectFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-select-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <mat-select [required]="options.required" [formControl]="control" (selectionChange)="selectionChanges($event)">
    <mat-option *ngFor="let choice of options.choices" value="{{choice}}">
      {{options.choicesTranslatePrefix + choice|translate}}
    </mat-option>
  </mat-select>
</mat-form-field>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartSelectFormComponent.ctorParameters = () => [];
BootstartSelectFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }],
    selectionChange: [{ type: Output }]
};
if (false) {
    /**
     * Associated form control
     * @type {?}
     */
    BootstartSelectFormComponent.prototype.control;
    /**
     * Select options
     * @type {?}
     */
    BootstartSelectFormComponent.prototype.options;
    /**
     * On Change emitter
     * @type {?}
     */
    BootstartSelectFormComponent.prototype.selectionChange;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LXNlbGVjdC1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXNlbGVjdC1mb3JtL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDN0UsT0FBTyxFQUFDLFdBQVcsRUFBQyxNQUFNLGdCQUFnQixDQUFDO0FBc0IzQyxNQUFNO0lBV0o7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxZQUFZLEVBQTBCLENBQUM7S0FDbkU7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLHVCQUF1QixDQUFDLHdCQUF3QixFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7S0FDNUM7Ozs7O0lBRU0sZ0JBQWdCLENBQUMsS0FBNkI7UUFDbkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Ozs7Ozs7SUFJM0IsdUJBQXVCLENBQUMsS0FBVSxFQUFFLFlBQWlCO1FBQzNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7OztZQS9DSixTQUFTLFNBQUM7Z0JBQ0UsUUFBUSxFQUFLLHVCQUF1QjtnQkFDcEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7O0NBYXRCO2dCQUNZLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQzthQUNiOzs7OztzQkFJVCxLQUFLO3NCQUdMLEtBQUs7OEJBR0wsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Zvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0Jvb3RzdGFydFNlbGVjdEZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtc2VsZWN0LWZvcm0tb3B0aW9ucyc7XG5pbXBvcnQge01hdFNlbGVjdGlvbkxpc3RDaGFuZ2V9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtc2VsZWN0LWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8bWF0LXNlbGVjdCBbcmVxdWlyZWRdPVwib3B0aW9ucy5yZXF1aXJlZFwiIFtmb3JtQ29udHJvbF09XCJjb250cm9sXCIgKHNlbGVjdGlvbkNoYW5nZSk9XCJzZWxlY3Rpb25DaGFuZ2VzKCRldmVudClcIj5cbiAgICA8bWF0LW9wdGlvbiAqbmdGb3I9XCJsZXQgY2hvaWNlIG9mIG9wdGlvbnMuY2hvaWNlc1wiIHZhbHVlPVwie3tjaG9pY2V9fVwiPlxuICAgICAge3tvcHRpb25zLmNob2ljZXNUcmFuc2xhdGVQcmVmaXggKyBjaG9pY2V8dHJhbnNsYXRlfX1cbiAgICA8L21hdC1vcHRpb24+XG4gIDwvbWF0LXNlbGVjdD5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2BgXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydFNlbGVjdEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIC8qKiBBc3NvY2lhdGVkIGZvcm0gY29udHJvbCAqL1xuICBASW5wdXQoKSBjb250cm9sOiBGb3JtQ29udHJvbDtcblxuICAvKiogU2VsZWN0IG9wdGlvbnMgKi9cbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0U2VsZWN0Rm9ybU9wdGlvbnM7XG5cbiAgLyoqIE9uIENoYW5nZSBlbWl0dGVyICovXG4gIEBPdXRwdXQoKSBzZWxlY3Rpb25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxNYXRTZWxlY3Rpb25MaXN0Q2hhbmdlPjtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woJycpO1xuICAgIHRoaXMuc2VsZWN0aW9uQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxNYXRTZWxlY3Rpb25MaXN0Q2hhbmdlPigpO1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignY2hvaWNlc1RyYW5zbGF0ZVByZWZpeCcsICcnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCd3aWR0aCcsIDEwMCk7XG4gIH1cblxuICBwdWJsaWMgc2VsZWN0aW9uQ2hhbmdlcyhldmVudDogTWF0U2VsZWN0aW9uTGlzdENoYW5nZSk6IHZvaWQge1xuICAgIHRoaXMuc2VsZWN0aW9uQ2hhbmdlLmVtaXQoZXZlbnQpO1xuICB9XG5cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIl19