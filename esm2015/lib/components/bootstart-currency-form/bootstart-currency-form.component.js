/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
export class BootstartCurrencyFormComponent {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('width', 100);
        this._replaceUndefinedOption('align', 'left');
        this._replaceUndefinedOption('allowNegative', true);
        this._replaceUndefinedOption('decimal', '.');
        this._replaceUndefinedOption('precision', 2);
        this._replaceUndefinedOption('prefix', '$ ');
        this._replaceUndefinedOption('suffix', '');
        this._replaceUndefinedOption('thousands', ',');
        this._replaceUndefinedOption('nullable', true);
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartCurrencyFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-currency-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <form [formGroup]="group">
    <input currencyMask matInput
           formControlName="{{controlName}}"
           [required]="options.required"
           [options]="{
           align: options.align,
           allowNegative: options.allowNegative,
           decimal: options.decimal,
           precision: options.precision,
           prefix: options.prefix,
           suffix: options.suffix,
           thousands: options.thousands,
           nullable: options.nullable
           }">
  </form>
</mat-form-field>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartCurrencyFormComponent.ctorParameters = () => [];
BootstartCurrencyFormComponent.propDecorators = {
    group: [{ type: Input }],
    controlName: [{ type: Input }],
    options: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BootstartCurrencyFormComponent.prototype.group;
    /** @type {?} */
    BootstartCurrencyFormComponent.prototype.controlName;
    /** @type {?} */
    BootstartCurrencyFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9ib290c3RhcnQtY3VycmVuY3ktZm9ybS9ib290c3RhcnQtY3VycmVuY3ktZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBRXZELE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQThCekMsTUFBTTtJQU1KO0tBQ0M7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDaEQ7Ozs7OztJQUVPLHVCQUF1QixDQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDcEM7Ozs7WUFwREosU0FBUyxTQUFDO2dCQUNFLFFBQVEsRUFBSyx5QkFBeUI7Z0JBQ3RDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0F1QnRCO2dCQUNZLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQzthQUNiOzs7OztvQkFHVCxLQUFLOzBCQUNMLEtBQUs7c0JBQ0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Qm9vdHN0YXJ0Q3VycmVuY3lGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzJztcbmltcG9ydCB7Rm9ybUdyb3VwfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8Zm9ybSBbZm9ybUdyb3VwXT1cImdyb3VwXCI+XG4gICAgPGlucHV0IGN1cnJlbmN5TWFzayBtYXRJbnB1dFxuICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJ7e2NvbnRyb2xOYW1lfX1cIlxuICAgICAgICAgICBbcmVxdWlyZWRdPVwib3B0aW9ucy5yZXF1aXJlZFwiXG4gICAgICAgICAgIFtvcHRpb25zXT1cIntcbiAgICAgICAgICAgYWxpZ246IG9wdGlvbnMuYWxpZ24sXG4gICAgICAgICAgIGFsbG93TmVnYXRpdmU6IG9wdGlvbnMuYWxsb3dOZWdhdGl2ZSxcbiAgICAgICAgICAgZGVjaW1hbDogb3B0aW9ucy5kZWNpbWFsLFxuICAgICAgICAgICBwcmVjaXNpb246IG9wdGlvbnMucHJlY2lzaW9uLFxuICAgICAgICAgICBwcmVmaXg6IG9wdGlvbnMucHJlZml4LFxuICAgICAgICAgICBzdWZmaXg6IG9wdGlvbnMuc3VmZml4LFxuICAgICAgICAgICB0aG91c2FuZHM6IG9wdGlvbnMudGhvdXNhbmRzLFxuICAgICAgICAgICBudWxsYWJsZTogb3B0aW9ucy5udWxsYWJsZVxuICAgICAgICAgICB9XCI+XG4gIDwvZm9ybT5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2BgXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgZ3JvdXA6IEZvcm1Hcm91cDtcbiAgQElucHV0KCkgY29udHJvbE5hbWU6IHN0cmluZztcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0Q3VycmVuY3lGb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdhbGlnbicsICdsZWZ0Jyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignYWxsb3dOZWdhdGl2ZScsIHRydWUpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2RlY2ltYWwnLCAnLicpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3ByZWNpc2lvbicsIDIpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3ByZWZpeCcsICckICcpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3N1ZmZpeCcsICcnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCd0aG91c2FuZHMnLCAnLCcpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ251bGxhYmxlJywgdHJ1ZSk7XG4gIH1cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIl19