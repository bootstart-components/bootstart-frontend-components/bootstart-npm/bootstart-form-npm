/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
export class BootstartNumberFormComponent {
    constructor() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('width', '100');
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartNumberFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-number-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <input matInput type="number" [formControl]="control" [required]="options.required">
</mat-form-field>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartNumberFormComponent.ctorParameters = () => [];
BootstartNumberFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BootstartNumberFormComponent.prototype.control;
    /** @type {?} */
    BootstartNumberFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LW51bWJlci1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LW51bWJlci1mb3JtL2Jvb3RzdGFydC1udW1iZXItZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3ZELE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQWlCM0MsTUFBTTtJQUtKO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO0tBQzlDOzs7Ozs7SUFHTyx1QkFBdUIsQ0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7O1lBL0JKLFNBQVMsU0FBQztnQkFDRSxRQUFRLEVBQUssdUJBQXVCO2dCQUNwQyxRQUFRLEVBQUU7Ozs7Ozs7OztDQVN0QjtnQkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7YUFDYjs7Ozs7c0JBR1QsS0FBSztzQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnROdW1iZXJGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LW51bWJlci1mb3JtLW9wdGlvbnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1udW1iZXItZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWZvcm0tZmllbGQgW25nU3R5bGVdPVwieyd3aWR0aCcgOiBvcHRpb25zLndpZHRoKyclJ31cIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxpbnB1dCBtYXRJbnB1dCB0eXBlPVwibnVtYmVyXCIgW2Zvcm1Db250cm9sXT1cImNvbnRyb2xcIiBbcmVxdWlyZWRdPVwib3B0aW9ucy5yZXF1aXJlZFwiPlxuPC9tYXQtZm9ybS1maWVsZD5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYGBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgY29udHJvbDogRm9ybUNvbnRyb2w7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydE51bWJlckZvcm1PcHRpb25zO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuY29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgnJyk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCd3aWR0aCcsICcxMDAnKTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==