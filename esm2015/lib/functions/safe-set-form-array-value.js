/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { FormBuilder } from '@angular/forms';
/**
 * @param {?} value
 * @return {?}
 */
export function SafeSetFormArrayValue(value) {
    /** @type {?} */
    const builder = new FormBuilder();
    if (typeof value === 'object') {
        return builder.array(value);
    }
    else {
        return builder.array([]);
    }
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FmZS1zZXQtZm9ybS1hcnJheS12YWx1ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL2Z1bmN0aW9ucy9zYWZlLXNldC1mb3JtLWFycmF5LXZhbHVlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQVksV0FBVyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7Ozs7O0FBRXRELE1BQU0sZ0NBQWdDLEtBQVU7O0lBQzlDLE1BQU0sT0FBTyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7SUFDbEMsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztRQUM5QixNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUM3QjtJQUFDLElBQUksQ0FBQyxDQUFDO1FBQ04sTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDMUI7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Rm9ybUFycmF5LCBGb3JtQnVpbGRlcn0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5leHBvcnQgZnVuY3Rpb24gU2FmZVNldEZvcm1BcnJheVZhbHVlKHZhbHVlOiBhbnkpOiBGb3JtQXJyYXkge1xuICBjb25zdCBidWlsZGVyID0gbmV3IEZvcm1CdWlsZGVyKCk7XG4gIGlmICh0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSB7XG4gICAgcmV0dXJuIGJ1aWxkZXIuYXJyYXkodmFsdWUpO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBidWlsZGVyLmFycmF5KFtdKTtcbiAgfVxufVxuIl19