/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { BootstartTextFormComponent } from './components/bootstart-text-form/bootstart-text-form.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatOptionModule, MatSelectModule, MatSlideToggleModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { BootstartParagraphFormComponent } from './components/bootstart-paragraph-form/bootstart-paragraph-form.component';
import { BootstartChipsFormComponent } from './components/bootstart-chips-form/bootstart-chips-form.component';
import { BootstartSelectFormComponent } from './components/bootstart-select-form/bootstart-select-form.component';
import { BootstartNumberFormComponent } from './components/bootstart-number-form/bootstart-number-form.component';
import { BootstartBooleanFormComponent } from './components/bootstart-boolean-form/bootstart-boolean-form.component';
import { BootstartChipsTextFormComponent } from './components/bootstart-chips-text-form/bootstart-chips-text-form.component';
import { BootstartCurrencyFormComponent } from './components/bootstart-currency-form/bootstart-currency-form.component';
import { NgxCurrencyModule } from 'ngx-currency';
export class BootstartFormModule {
}
BootstartFormModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatInputModule,
                    MatFormFieldModule,
                    MatIconModule,
                    MatChipsModule,
                    MatSelectModule,
                    MatOptionModule,
                    MatSlideToggleModule,
                    NgxCurrencyModule,
                    TranslateModule
                ],
                declarations: [
                    BootstartTextFormComponent,
                    BootstartParagraphFormComponent,
                    BootstartChipsFormComponent,
                    BootstartSelectFormComponent,
                    BootstartNumberFormComponent,
                    BootstartBooleanFormComponent,
                    BootstartChipsTextFormComponent,
                    BootstartCurrencyFormComponent
                ],
                exports: [
                    BootstartTextFormComponent,
                    BootstartParagraphFormComponent,
                    BootstartChipsFormComponent,
                    BootstartSelectFormComponent,
                    BootstartNumberFormComponent,
                    BootstartBooleanFormComponent,
                    BootstartChipsTextFormComponent,
                    BootstartCurrencyFormComponent
                ]
            },] },
];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZvcm0ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvYm9vdHN0YXJ0LWZvcm0ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQywwQkFBMEIsRUFBQyxNQUFNLGdFQUFnRSxDQUFDO0FBQzFHLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsV0FBVyxFQUFFLG1CQUFtQixFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDaEUsT0FBTyxFQUNMLGNBQWMsRUFDZCxrQkFBa0IsRUFDbEIsYUFBYSxFQUNiLGNBQWMsRUFDZCxlQUFlLEVBQ2YsZUFBZSxFQUNmLG9CQUFvQixFQUNyQixNQUFNLG1CQUFtQixDQUFDO0FBQzNCLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQztBQUNwRCxPQUFPLEVBQUMsK0JBQStCLEVBQUMsTUFBTSwwRUFBMEUsQ0FBQztBQUN6SCxPQUFPLEVBQUMsMkJBQTJCLEVBQUMsTUFBTSxrRUFBa0UsQ0FBQztBQUM3RyxPQUFPLEVBQUMsNEJBQTRCLEVBQUMsTUFBTSxvRUFBb0UsQ0FBQztBQUNoSCxPQUFPLEVBQUMsNEJBQTRCLEVBQUMsTUFBTSxvRUFBb0UsQ0FBQztBQUNoSCxPQUFPLEVBQUMsNkJBQTZCLEVBQUMsTUFBTSxzRUFBc0UsQ0FBQztBQUNuSCxPQUFPLEVBQUMsK0JBQStCLEVBQUMsTUFBTSw0RUFBNEUsQ0FBQztBQUMzSCxPQUFPLEVBQUMsOEJBQThCLEVBQUMsTUFBTSx3RUFBd0UsQ0FBQztBQUN0SCxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxjQUFjLENBQUM7QUFzQy9DLE1BQU07OztZQXBDTCxRQUFRLFNBQUM7Z0JBQ0UsT0FBTyxFQUFPO29CQUNaLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGNBQWM7b0JBQ2Qsa0JBQWtCO29CQUNsQixhQUFhO29CQUNiLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixlQUFlO29CQUNmLG9CQUFvQjtvQkFDcEIsaUJBQWlCO29CQUNqQixlQUFlO2lCQUNoQjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1osMEJBQTBCO29CQUMxQiwrQkFBK0I7b0JBQy9CLDJCQUEyQjtvQkFDM0IsNEJBQTRCO29CQUM1Qiw0QkFBNEI7b0JBQzVCLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQiw4QkFBOEI7aUJBQy9CO2dCQUNELE9BQU8sRUFBTztvQkFDWiwwQkFBMEI7b0JBQzFCLCtCQUErQjtvQkFDL0IsMkJBQTJCO29CQUMzQiw0QkFBNEI7b0JBQzVCLDRCQUE0QjtvQkFDNUIsNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLDhCQUE4QjtpQkFDL0I7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRUZXh0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC10ZXh0LWZvcm0vYm9vdHN0YXJ0LXRleHQtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge0Zvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge1xuICBNYXRDaGlwc01vZHVsZSxcbiAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICBNYXRJY29uTW9kdWxlLFxuICBNYXRJbnB1dE1vZHVsZSxcbiAgTWF0T3B0aW9uTW9kdWxlLFxuICBNYXRTZWxlY3RNb2R1bGUsXG4gIE1hdFNsaWRlVG9nZ2xlTW9kdWxlXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7VHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7Qm9vdHN0YXJ0UGFyYWdyYXBoRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS9ib290c3RhcnQtcGFyYWdyYXBoLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWNoaXBzLWZvcm0vYm9vdHN0YXJ0LWNoaXBzLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS9ib290c3RhcnQtc2VsZWN0LWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1udW1iZXItZm9ybS9ib290c3RhcnQtbnVtYmVyLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtYm9vbGVhbi1mb3JtL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1jaGlwcy10ZXh0LWZvcm0vYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRDdXJyZW5jeUZvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtY3VycmVuY3ktZm9ybS9ib290c3RhcnQtY3VycmVuY3ktZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtOZ3hDdXJyZW5jeU1vZHVsZX0gZnJvbSAnbmd4LWN1cnJlbmN5JztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIEZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJY29uTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRDaGlwc01vZHVsZSxcbiAgICAgICAgICAgICAgTWF0U2VsZWN0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRPcHRpb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdFNsaWRlVG9nZ2xlTW9kdWxlLFxuICAgICAgICAgICAgICBOZ3hDdXJyZW5jeU1vZHVsZSxcbiAgICAgICAgICAgICAgVHJhbnNsYXRlTW9kdWxlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydFRleHRGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRQYXJhZ3JhcGhGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDaGlwc0Zvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydFNlbGVjdEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydE51bWJlckZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydEJvb2xlYW5Gb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDaGlwc1RleHRGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDdXJyZW5jeUZvcm1Db21wb25lbnRcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBleHBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0VGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydFBhcmFncmFwaEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzVGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudFxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0Rm9ybU1vZHVsZSB7XG59XG4iXX0=