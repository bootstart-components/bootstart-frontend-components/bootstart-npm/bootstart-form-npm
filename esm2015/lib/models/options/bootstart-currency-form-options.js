/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartCurrencyFormOptions() { }
/**
 * Question label.
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.label;
/**
 * Icon of the question.
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.icon;
/**
 * Is this question required?
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.required;
/**
 * Width of the field (as percentage)
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.width;
/**
 * Text alignment in input. (default: right)
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.align;
/**
 * If true can input negative values. (default: true)
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.allowNegative;
/**
 * Separator of decimals (default: '.')
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.decimal;
/**
 * Number of decimal places (default: 2)
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.precision;
/**
 * Money prefix (default: '$ ')
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.prefix;
/**
 * Money suffix (default: '')
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.suffix;
/**
 * Separator of thousands (default: ',')
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.thousands;
/**
 * When true (default), the value of the clean field will be null, when false the value will be 0
 * @type {?|undefined}
 */
BootstartCurrencyFormOptions.prototype.nullable;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1jdXJyZW5jeS1mb3JtLW9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgQm9vdHN0YXJ0Q3VycmVuY3lGb3JtT3B0aW9ucyB7XG5cbiAgLyoqXG4gICAqIFF1ZXN0aW9uIGxhYmVsLlxuICAgKi9cbiAgbGFiZWw/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJY29uIG9mIHRoZSBxdWVzdGlvbi5cbiAgICovXG4gIGljb24/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJcyB0aGlzIHF1ZXN0aW9uIHJlcXVpcmVkP1xuICAgKi9cbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xuICAvKipcbiAgICogV2lkdGggb2YgdGhlIGZpZWxkIChhcyBwZXJjZW50YWdlKVxuICAgKi9cbiAgd2lkdGg/OiBudW1iZXI7XG5cblxuICAvLyBPcHRpb25zIGZyb20gbmd4LWN1cnJlbmN5XG5cbiAgLyoqXG4gICAqIFRleHQgYWxpZ25tZW50IGluIGlucHV0LiAoZGVmYXVsdDogcmlnaHQpXG4gICAqL1xuICBhbGlnbj86IHN0cmluZztcbiAgLyoqXG4gICAqIElmIHRydWUgY2FuIGlucHV0IG5lZ2F0aXZlIHZhbHVlcy4gKGRlZmF1bHQ6IHRydWUpXG4gICAqL1xuICBhbGxvd05lZ2F0aXZlPzogYm9vbGVhbjtcbiAgLyoqXG4gICAqIFNlcGFyYXRvciBvZiBkZWNpbWFscyAoZGVmYXVsdDogJy4nKVxuICAgKi9cbiAgZGVjaW1hbD86IHN0cmluZztcbiAgLyoqXG4gICAqIE51bWJlciBvZiBkZWNpbWFsIHBsYWNlcyAoZGVmYXVsdDogMilcbiAgICovXG4gIHByZWNpc2lvbj86IG51bWJlcjtcbiAgLyoqXG4gICAqIE1vbmV5IHByZWZpeCAoZGVmYXVsdDogJyQgJylcbiAgICovXG4gIHByZWZpeD86IHN0cmluZztcbiAgLyoqXG4gICAqIE1vbmV5IHN1ZmZpeCAoZGVmYXVsdDogJycpXG4gICAqL1xuICBzdWZmaXg/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBTZXBhcmF0b3Igb2YgdGhvdXNhbmRzIChkZWZhdWx0OiAnLCcpXG4gICAqL1xuICB0aG91c2FuZHM/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBXaGVuIHRydWUgKGRlZmF1bHQpLCB0aGUgdmFsdWUgb2YgdGhlIGNsZWFuIGZpZWxkIHdpbGwgYmUgbnVsbCwgd2hlbiBmYWxzZSB0aGUgdmFsdWUgd2lsbCBiZSAwXG4gICAqL1xuICBudWxsYWJsZT86IGJvb2xlYW47XG5cbn1cbiJdfQ==