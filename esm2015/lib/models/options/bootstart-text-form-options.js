/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartTextFormOptions() { }
/**
 * Question label.
 * @type {?|undefined}
 */
BootstartTextFormOptions.prototype.label;
/**
 * Icon of the question.
 * @type {?|undefined}
 */
BootstartTextFormOptions.prototype.icon;
/**
 * Is this question required?
 * @type {?|undefined}
 */
BootstartTextFormOptions.prototype.required;
/**
 * Width of the field (as percentage)
 * @type {?|undefined}
 */
BootstartTextFormOptions.prototype.width;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LXRleHQtZm9ybS1vcHRpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LXRleHQtZm9ybS1vcHRpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEJvb3RzdGFydFRleHRGb3JtT3B0aW9ucyB7XG5cbiAgLyoqXG4gICAqIFF1ZXN0aW9uIGxhYmVsLlxuICAgKi9cbiAgbGFiZWw/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJY29uIG9mIHRoZSBxdWVzdGlvbi5cbiAgICovXG4gIGljb24/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJcyB0aGlzIHF1ZXN0aW9uIHJlcXVpcmVkP1xuICAgKi9cbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xuICAvKipcbiAgICogV2lkdGggb2YgdGhlIGZpZWxkIChhcyBwZXJjZW50YWdlKVxuICAgKi9cbiAgd2lkdGg/OiBudW1iZXI7XG5cbn1cbiJdfQ==