/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ChipFomOptions() { }
/**
 * List of possible choices
 * @type {?}
 */
ChipFomOptions.prototype.choices;
/**
 * Optional question label
 * @type {?|undefined}
 */
ChipFomOptions.prototype.questionLabel;
/**
 * Optional question icon
 * @type {?|undefined}
 */
ChipFomOptions.prototype.questionIcon;
/**
 * Maximum number of selectable items
 * @type {?|undefined}
 */
ChipFomOptions.prototype.maximumSelectable;
/**
 * Optional prefix for choice translation
 * @type {?|undefined}
 */
ChipFomOptions.prototype.choicesTranslatePrefix;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpcC1mb20tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9vcHRpb25zL2NoaXAtZm9tLW9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgQ2hpcEZvbU9wdGlvbnMge1xuXG4gIC8qKiBMaXN0IG9mIHBvc3NpYmxlIGNob2ljZXMgKi9cbiAgY2hvaWNlczogc3RyaW5nW107XG5cbiAgLyoqIE9wdGlvbmFsIHF1ZXN0aW9uIGxhYmVsICovXG4gIHF1ZXN0aW9uTGFiZWw/OiBzdHJpbmc7XG5cbiAgLyoqIE9wdGlvbmFsIHF1ZXN0aW9uIGljb24gKi9cbiAgcXVlc3Rpb25JY29uPzogc3RyaW5nO1xuXG4gIC8qKiBNYXhpbXVtIG51bWJlciBvZiBzZWxlY3RhYmxlIGl0ZW1zICovXG4gIG1heGltdW1TZWxlY3RhYmxlPzogbnVtYmVyO1xuXG4gIC8qKiBPcHRpb25hbCBwcmVmaXggZm9yIGNob2ljZSB0cmFuc2xhdGlvbiAqL1xuICBjaG9pY2VzVHJhbnNsYXRlUHJlZml4Pzogc3RyaW5nO1xuXG59XG4iXX0=