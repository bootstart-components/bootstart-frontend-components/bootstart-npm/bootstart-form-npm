/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function TextFormOptions() { }
/**
 * Optional question label
 * @type {?|undefined}
 */
TextFormOptions.prototype.label;
/**
 * Optional question icon
 * @type {?|undefined}
 */
TextFormOptions.prototype.icon;
/**
 * Is the question required?
 * @type {?|undefined}
 */
TextFormOptions.prototype.required;
/**
 * Question field's width in percent
 * @type {?|undefined}
 */
TextFormOptions.prototype.width;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1mb3JtLW9wdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvb3B0aW9ucy90ZXh0LWZvcm0tb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBUZXh0Rm9ybU9wdGlvbnMge1xuXG4gIC8qKiBPcHRpb25hbCBxdWVzdGlvbiBsYWJlbCAqL1xuICBsYWJlbD86IHN0cmluZztcblxuICAvKiogT3B0aW9uYWwgcXVlc3Rpb24gaWNvbiAqL1xuICBpY29uPzogc3RyaW5nO1xuXG4gIC8qKiBJcyB0aGUgcXVlc3Rpb24gcmVxdWlyZWQ/ICovXG4gIHJlcXVpcmVkPzogYm9vbGVhbjtcblxuICAvKiogUXVlc3Rpb24gZmllbGQncyB3aWR0aCBpbiBwZXJjZW50ICovXG4gIHdpZHRoPzogbnVtYmVyO1xuXG59XG4iXX0=