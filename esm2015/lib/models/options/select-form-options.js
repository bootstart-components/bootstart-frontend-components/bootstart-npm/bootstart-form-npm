/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function SelectFormOptions() { }
/**
 * List of possible choices
 * @type {?}
 */
SelectFormOptions.prototype.choices;
/**
 * Optional question label
 * @type {?|undefined}
 */
SelectFormOptions.prototype.label;
/**
 * Optional question icon
 * @type {?|undefined}
 */
SelectFormOptions.prototype.icon;
/**
 * Optional prefix for choice translation
 * @type {?|undefined}
 */
SelectFormOptions.prototype.choicesTranslatePrefix;
/**
 * Is the question required?
 * @type {?|undefined}
 */
SelectFormOptions.prototype.required;
/**
 * Question field's width in percent
 * @type {?|undefined}
 */
SelectFormOptions.prototype.width;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LWZvcm0tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9vcHRpb25zL3NlbGVjdC1mb3JtLW9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgU2VsZWN0Rm9ybU9wdGlvbnMge1xuXG4gIC8qKiBMaXN0IG9mIHBvc3NpYmxlIGNob2ljZXMgKi9cbiAgY2hvaWNlczogc3RyaW5nW107XG5cbiAgLyoqIE9wdGlvbmFsIHF1ZXN0aW9uIGxhYmVsICovXG4gIGxhYmVsPzogc3RyaW5nO1xuXG4gIC8qKiBPcHRpb25hbCBxdWVzdGlvbiBpY29uICovXG4gIGljb24/OiBzdHJpbmc7XG5cbiAgLyoqIE9wdGlvbmFsIHByZWZpeCBmb3IgY2hvaWNlIHRyYW5zbGF0aW9uICovXG4gIGNob2ljZXNUcmFuc2xhdGVQcmVmaXg/OiBzdHJpbmc7XG5cbiAgLyoqIElzIHRoZSBxdWVzdGlvbiByZXF1aXJlZD8gKi9cbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xuXG4gIC8qKiBRdWVzdGlvbiBmaWVsZCdzIHdpZHRoIGluIHBlcmNlbnQgKi9cbiAgd2lkdGg/OiBudW1iZXI7XG5cbn1cbiJdfQ==