/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ChipsTextFormOptions() { }
/**
 * Question Label
 * @type {?|undefined}
 */
ChipsTextFormOptions.prototype.label;
/**
 * Question Icon
 * @type {?|undefined}
 */
ChipsTextFormOptions.prototype.icon;
/**
 * Input placeholder
 * @type {?|undefined}
 */
ChipsTextFormOptions.prototype.placeholder;
/**
 * Separator values
 * @type {?}
 */
ChipsTextFormOptions.prototype.separator;
/**
 * Maximum input
 * @type {?|undefined}
 */
ChipsTextFormOptions.prototype.maximum;
/**
 * Chip Color
 * @type {?}
 */
ChipsTextFormOptions.prototype.chipColor;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpcHMtdGV4dC1mb3JtLW9wdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvb3B0aW9ucy9jaGlwcy10ZXh0LWZvcm0tb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBDaGlwc1RleHRGb3JtT3B0aW9ucyB7XG5cbiAgLyoqIFF1ZXN0aW9uIExhYmVsICovXG4gIGxhYmVsPzogc3RyaW5nO1xuXG4gIC8qKiBRdWVzdGlvbiBJY29uICovXG4gIGljb24/OiBzdHJpbmc7XG5cbiAgLyoqIElucHV0IHBsYWNlaG9sZGVyICovXG4gIHBsYWNlaG9sZGVyPzogc3RyaW5nO1xuXG4gIC8qKiBTZXBhcmF0b3IgdmFsdWVzICovXG4gIHNlcGFyYXRvcjogbnVtYmVyW107XG5cbiAgLyoqIE1heGltdW0gaW5wdXQgKi9cbiAgbWF4aW11bT86IG51bWJlcjtcblxuICAvKiogQ2hpcCBDb2xvciAqL1xuICBjaGlwQ29sb3I6IHN0cmluZztcblxufVxuIl19