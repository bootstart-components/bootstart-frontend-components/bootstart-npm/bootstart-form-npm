/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartParagraphFormOptions() { }
/**
 * Question label.
 * @type {?|undefined}
 */
BootstartParagraphFormOptions.prototype.label;
/**
 * Icon of the question.
 * @type {?|undefined}
 */
BootstartParagraphFormOptions.prototype.icon;
/**
 * Is this question required?
 * @type {?|undefined}
 */
BootstartParagraphFormOptions.prototype.required;
/**
 * Width of the field (as percentage)
 * @type {?|undefined}
 */
BootstartParagraphFormOptions.prototype.width;
/**
 * Number of rows (initialization). Default to 6.
 * @type {?|undefined}
 */
BootstartParagraphFormOptions.prototype.rows;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LXBhcmFncmFwaC1mb3JtLW9wdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtcGFyYWdyYXBoLWZvcm0tb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBCb290c3RhcnRQYXJhZ3JhcGhGb3JtT3B0aW9ucyB7XG5cblxuICAvKipcbiAgICogUXVlc3Rpb24gbGFiZWwuXG4gICAqL1xuICBsYWJlbD86IHN0cmluZztcbiAgLyoqXG4gICAqIEljb24gb2YgdGhlIHF1ZXN0aW9uLlxuICAgKi9cbiAgaWNvbj86IHN0cmluZztcbiAgLyoqXG4gICAqIElzIHRoaXMgcXVlc3Rpb24gcmVxdWlyZWQ/XG4gICAqL1xuICByZXF1aXJlZD86IGJvb2xlYW47XG4gIC8qKlxuICAgKiBXaWR0aCBvZiB0aGUgZmllbGQgKGFzIHBlcmNlbnRhZ2UpXG4gICAqL1xuICB3aWR0aD86IG51bWJlcjtcbiAgLyoqXG4gICAqIE51bWJlciBvZiByb3dzIChpbml0aWFsaXphdGlvbikuIERlZmF1bHQgdG8gNi5cbiAgICovXG4gIHJvd3M/OiBudW1iZXI7XG5cbn1cbiJdfQ==