/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartChipsFormOptions() { }
/**
 * List of possible choices.
 * @type {?}
 */
BootstartChipsFormOptions.prototype.choices;
/**
 * Maximum number of selectable items (default to Infinity)
 * @type {?|undefined}
 */
BootstartChipsFormOptions.prototype.maximumSelectable;
/**
 * Prefix for translating each choice.
 * @type {?|undefined}
 */
BootstartChipsFormOptions.prototype.choicesTranslatePrefix;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWNoaXBzLWZvcm0tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1jaGlwcy1mb3JtLW9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgQm9vdHN0YXJ0Q2hpcHNGb3JtT3B0aW9ucyB7XG5cbiAgLyoqXG4gICAqIExpc3Qgb2YgcG9zc2libGUgY2hvaWNlcy5cbiAgICovXG4gIGNob2ljZXM6IHN0cmluZ1tdO1xuICAvKipcbiAgICogTWF4aW11bSBudW1iZXIgb2Ygc2VsZWN0YWJsZSBpdGVtcyAoZGVmYXVsdCB0byBJbmZpbml0eSlcbiAgICovXG4gIG1heGltdW1TZWxlY3RhYmxlPzogbnVtYmVyO1xuICAvKipcbiAgICogUHJlZml4IGZvciB0cmFuc2xhdGluZyBlYWNoIGNob2ljZS5cbiAgICovXG4gIGNob2ljZXNUcmFuc2xhdGVQcmVmaXg/OiBzdHJpbmc7XG5cbn1cbiJdfQ==