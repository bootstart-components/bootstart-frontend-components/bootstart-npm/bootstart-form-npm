/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function NumberFormOptions() { }
/**
 * Optional question label
 * @type {?|undefined}
 */
NumberFormOptions.prototype.label;
/**
 * Optional question icon
 * @type {?|undefined}
 */
NumberFormOptions.prototype.icon;
/**
 * Is the question required?
 * @type {?|undefined}
 */
NumberFormOptions.prototype.required;
/**
 * Question field's width in percent
 * @type {?|undefined}
 */
NumberFormOptions.prototype.width;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtYmVyLWZvcm0tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9vcHRpb25zL251bWJlci1mb3JtLW9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgTnVtYmVyRm9ybU9wdGlvbnMge1xuXG4gIC8qKiBPcHRpb25hbCBxdWVzdGlvbiBsYWJlbCAqL1xuICBsYWJlbD86IHN0cmluZztcblxuICAvKiogT3B0aW9uYWwgcXVlc3Rpb24gaWNvbiAqL1xuICBpY29uPzogc3RyaW5nO1xuXG4gIC8qKiBJcyB0aGUgcXVlc3Rpb24gcmVxdWlyZWQ/ICovXG4gIHJlcXVpcmVkPzogYm9vbGVhbjtcblxuICAvKiogUXVlc3Rpb24gZmllbGQncyB3aWR0aCBpbiBwZXJjZW50ICovXG4gIHdpZHRoPzogbnVtYmVyO1xuXG59XG4iXX0=