/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartNumberFormOptions() { }
/**
 * Question label.
 * @type {?|undefined}
 */
BootstartNumberFormOptions.prototype.label;
/**
 * Icon of the question.
 * @type {?|undefined}
 */
BootstartNumberFormOptions.prototype.icon;
/**
 * Is this question required?
 * @type {?|undefined}
 */
BootstartNumberFormOptions.prototype.required;
/**
 * Width of the field (as percentage)
 * @type {?|undefined}
 */
BootstartNumberFormOptions.prototype.width;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LW51bWJlci1mb3JtLW9wdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtbnVtYmVyLWZvcm0tb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBCb290c3RhcnROdW1iZXJGb3JtT3B0aW9ucyB7XG5cbiAgLyoqXG4gICAqIFF1ZXN0aW9uIGxhYmVsLlxuICAgKi9cbiAgbGFiZWw/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJY29uIG9mIHRoZSBxdWVzdGlvbi5cbiAgICovXG4gIGljb24/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJcyB0aGlzIHF1ZXN0aW9uIHJlcXVpcmVkP1xuICAgKi9cbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xuICAvKipcbiAgICogV2lkdGggb2YgdGhlIGZpZWxkIChhcyBwZXJjZW50YWdlKVxuICAgKi9cbiAgd2lkdGg/OiBudW1iZXI7XG5cbn1cbiJdfQ==