/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartChipsTextFormOptions() { }
/**
 * Question label.
 * @type {?|undefined}
 */
BootstartChipsTextFormOptions.prototype.label;
/**
 * Icon of the question.
 * @type {?|undefined}
 */
BootstartChipsTextFormOptions.prototype.icon;
/**
 * Placeholder
 * @type {?|undefined}
 */
BootstartChipsTextFormOptions.prototype.placeholder;
/**
 * List of keys separator (default to ENTER)
 * @type {?|undefined}
 */
BootstartChipsTextFormOptions.prototype.separator;
/**
 * Maximum chips that can be produced
 * @type {?|undefined}
 */
BootstartChipsTextFormOptions.prototype.maximum;
/**
 * Colors of the chips
 * @type {?|undefined}
 */
BootstartChipsTextFormOptions.prototype.chipColor;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS1vcHRpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS1vcHRpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEJvb3RzdGFydENoaXBzVGV4dEZvcm1PcHRpb25zIHtcblxuICAvKipcbiAgICogUXVlc3Rpb24gbGFiZWwuXG4gICAqL1xuICBsYWJlbD86IHN0cmluZztcbiAgLyoqXG4gICAqIEljb24gb2YgdGhlIHF1ZXN0aW9uLlxuICAgKi9cbiAgaWNvbj86IHN0cmluZztcbiAgLyoqXG4gICAqIFBsYWNlaG9sZGVyXG4gICAqL1xuICBwbGFjZWhvbGRlcj86IHN0cmluZztcbiAgLyoqXG4gICAqIExpc3Qgb2Yga2V5cyBzZXBhcmF0b3IgKGRlZmF1bHQgdG8gRU5URVIpXG4gICAqL1xuICBzZXBhcmF0b3I/OiBudW1iZXJbXTtcbiAgLyoqXG4gICAqIE1heGltdW0gY2hpcHMgdGhhdCBjYW4gYmUgcHJvZHVjZWRcbiAgICovXG4gIG1heGltdW0/OiBudW1iZXI7XG4gIC8qKlxuICAgKiBDb2xvcnMgb2YgdGhlIGNoaXBzXG4gICAqL1xuICBjaGlwQ29sb3I/OiBzdHJpbmc7XG5cbn1cbiJdfQ==