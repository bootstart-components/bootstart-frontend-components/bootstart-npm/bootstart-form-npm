/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartBooleanFormOptions() { }
/**
 * Question label.
 * @type {?|undefined}
 */
BootstartBooleanFormOptions.prototype.label;
/**
 * Icon of the question.
 * @type {?|undefined}
 */
BootstartBooleanFormOptions.prototype.icon;
/**
 * Color of the component, when active
 * @type {?|undefined}
 */
BootstartBooleanFormOptions.prototype.color;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS1vcHRpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWZvcm0vIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS1vcHRpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEJvb3RzdGFydEJvb2xlYW5Gb3JtT3B0aW9ucyB7XG5cbiAgLyoqXG4gICAqIFF1ZXN0aW9uIGxhYmVsLlxuICAgKi9cbiAgbGFiZWw/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBJY29uIG9mIHRoZSBxdWVzdGlvbi5cbiAgICovXG4gIGljb24/OiBzdHJpbmc7XG4gIC8qKlxuICAgKiBDb2xvciBvZiB0aGUgY29tcG9uZW50LCB3aGVuIGFjdGl2ZVxuICAgKi9cbiAgY29sb3I/OiBzdHJpbmc7XG5cbn1cbiJdfQ==