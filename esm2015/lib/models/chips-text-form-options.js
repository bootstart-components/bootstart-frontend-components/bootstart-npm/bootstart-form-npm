/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function ChipsTextFormOptions() { }
/**
 * Question Label
 * @type {?|undefined}
 */
ChipsTextFormOptions.prototype.label;
/**
 * Input placeholder
 * @type {?|undefined}
 */
ChipsTextFormOptions.prototype.placeholder;
/**
 * Maximum input
 * @type {?|undefined}
 */
ChipsTextFormOptions.prototype.maximum;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpcHMtdGV4dC1mb3JtLW9wdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvY2hpcHMtdGV4dC1mb3JtLW9wdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgQ2hpcHNUZXh0Rm9ybU9wdGlvbnMge1xuXG4gIC8qKiBRdWVzdGlvbiBMYWJlbCAqL1xuICBsYWJlbD86IHN0cmluZztcblxuICAvKiogSW5wdXQgcGxhY2Vob2xkZXIgKi9cbiAgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XG5cbiAgLyoqIE1heGltdW0gaW5wdXQgKi9cbiAgbWF4aW11bT86IG51bWJlcjtcblxufVxuIl19