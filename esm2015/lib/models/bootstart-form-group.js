/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartFormGroup() { }
/**
 * Form controls loader from a given object.
 * \@param object Object containing information to be loaded.
 * @type {?}
 */
BootstartFormGroup.prototype.loadControls;
/**
 * Form controls saver into an object.
 * @type {?}
 */
BootstartFormGroup.prototype.saveControls;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZvcm0tZ3JvdXAuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZm9ybS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvYm9vdHN0YXJ0LWZvcm0tZ3JvdXAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgQm9vdHN0YXJ0Rm9ybUdyb3VwIHtcblxuICAvKipcbiAgICogRm9ybSBjb250cm9scyBsb2FkZXIgZnJvbSBhIGdpdmVuIG9iamVjdC5cbiAgICogQHBhcmFtIG9iamVjdCBPYmplY3QgY29udGFpbmluZyBpbmZvcm1hdGlvbiB0byBiZSBsb2FkZWQuXG4gICAqL1xuICBsb2FkQ29udHJvbHMob2JqZWN0OiBhbnkpOiB2b2lkO1xuXG4gIC8qKlxuICAgKiBGb3JtIGNvbnRyb2xzIHNhdmVyIGludG8gYW4gb2JqZWN0LlxuICAgKi9cbiAgc2F2ZUNvbnRyb2xzKCk6IGFueTtcblxufVxuIl19