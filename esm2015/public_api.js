/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/*
 * Public API Surface of bootstart-form
 */
export { BootstartFormModule } from './lib/bootstart-form.module';
export { SafeSetFormArrayValue } from './lib/functions/safe-set-form-array-value';

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1mb3JtLyIsInNvdXJjZXMiOlsicHVibGljX2FwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUEsb0NBQWMsNkJBQTZCLENBQUM7QUFDNUMsc0NBQWMsMkNBQTJDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIGJvb3RzdGFydC1mb3JtXG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvYm9vdHN0YXJ0LWZvcm0ubW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2Z1bmN0aW9ucy9zYWZlLXNldC1mb3JtLWFycmF5LXZhbHVlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21vZGVscy9pbmRleCc7XG4iXX0=