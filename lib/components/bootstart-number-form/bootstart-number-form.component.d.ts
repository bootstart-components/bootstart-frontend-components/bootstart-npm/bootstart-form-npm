import { OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BootstartNumberFormOptions } from '../../models/options/bootstart-number-form-options';
export declare class BootstartNumberFormComponent implements OnInit {
    control: FormControl;
    options: BootstartNumberFormOptions;
    constructor();
    ngOnInit(): void;
    private _replaceUndefinedOption(field, defaultValue);
}
