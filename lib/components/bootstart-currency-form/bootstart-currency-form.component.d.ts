import { OnInit } from '@angular/core';
import { BootstartCurrencyFormOptions } from '../../models';
import { FormGroup } from '@angular/forms';
export declare class BootstartCurrencyFormComponent implements OnInit {
    group: FormGroup;
    controlName: string;
    options: BootstartCurrencyFormOptions;
    constructor();
    ngOnInit(): void;
    private _replaceUndefinedOption(field, defaultValue);
}
