import { EventEmitter, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BootstartSelectFormOptions } from '../../models/options/bootstart-select-form-options';
import { MatSelectionListChange } from '@angular/material';
export declare class BootstartSelectFormComponent implements OnInit {
    /** Associated form control */
    control: FormControl;
    /** Select options */
    options: BootstartSelectFormOptions;
    /** On Change emitter */
    selectionChange: EventEmitter<MatSelectionListChange>;
    constructor();
    ngOnInit(): void;
    selectionChanges(event: MatSelectionListChange): void;
    private _replaceUndefinedOption(field, defaultValue);
}
