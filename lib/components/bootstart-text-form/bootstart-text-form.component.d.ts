import { OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BootstartTextFormOptions } from '../../models/options/bootstart-text-form-options';
export declare class BootstartTextFormComponent implements OnInit {
    control: FormControl;
    options: BootstartTextFormOptions;
    constructor();
    ngOnInit(): void;
    private _replaceUndefinedOption(field, defaultValue);
}
