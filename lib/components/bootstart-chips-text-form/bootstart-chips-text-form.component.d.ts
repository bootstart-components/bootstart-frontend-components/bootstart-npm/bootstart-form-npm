import { OnInit } from '@angular/core';
import { BootstartChipsTextFormOptions } from '../../models/options/bootstart-chips-text-form-options';
import { FormArray } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material';
export declare class BootstartChipsTextFormComponent implements OnInit {
    options: BootstartChipsTextFormOptions;
    formArray: FormArray;
    constructor();
    ngOnInit(): void;
    addChip(event: MatChipInputEvent): void;
    removeChip(chip: string): void;
    private _replaceUndefinedOption(field, defaultValue);
}
