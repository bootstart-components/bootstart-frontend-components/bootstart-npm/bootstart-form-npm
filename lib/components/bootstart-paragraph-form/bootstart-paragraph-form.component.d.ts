import { OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BootstartParagraphFormOptions } from '../../models/options/bootstart-paragraph-form-options';
export declare class BootstartParagraphFormComponent implements OnInit {
    control: FormControl;
    options: BootstartParagraphFormOptions;
    constructor();
    ngOnInit(): void;
    private _replaceUndefinedOption(field, defaultValue);
}
