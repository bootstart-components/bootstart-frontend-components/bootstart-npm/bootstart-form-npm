import { OnInit } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { BootstartChipsFormOptions } from '../../models/options/bootstart-chips-form-options';
import { FormArray } from '@angular/forms';
export declare class BootstartChipsFormComponent implements OnInit {
    formArray: FormArray;
    options: BootstartChipsFormOptions;
    selection: SelectionModel<string>;
    constructor();
    ngOnInit(): void;
    /**
     * What happens when selection changes
     * @param value Chip's value that has changed.
     */
    selectionChanges(value: string): void;
    isClickable(value: string): boolean;
    private _replaceUndefinedOption(field, defaultValue);
}
