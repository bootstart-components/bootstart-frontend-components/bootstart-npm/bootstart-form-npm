import { OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BootstartBooleanFormOptions } from '../../models/options/bootstart-boolean-form-options';
export declare class BootstartBooleanFormComponent implements OnInit {
    control: FormControl;
    options: BootstartBooleanFormOptions;
    constructor();
    ngOnInit(): void;
    private _replaceUndefinedOption(field, defaultValue);
}
