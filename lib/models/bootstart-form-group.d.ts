export interface BootstartFormGroup {
    /**
     * Form controls loader from a given object.
     * @param object Object containing information to be loaded.
     */
    loadControls(object: any): void;
    /**
     * Form controls saver into an object.
     */
    saveControls(): any;
}
