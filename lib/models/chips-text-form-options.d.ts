export interface ChipsTextFormOptions {
    /** Question Label */
    label?: string;
    /** Input placeholder */
    placeholder?: string;
    /** Maximum input */
    maximum?: number;
}
