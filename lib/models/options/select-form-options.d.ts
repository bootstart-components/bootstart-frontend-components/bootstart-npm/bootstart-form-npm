export interface SelectFormOptions {
    /** List of possible choices */
    choices: string[];
    /** Optional question label */
    label?: string;
    /** Optional question icon */
    icon?: string;
    /** Optional prefix for choice translation */
    choicesTranslatePrefix?: string;
    /** Is the question required? */
    required?: boolean;
    /** Question field's width in percent */
    width?: number;
}
