export interface BootstartChipsFormOptions {
    /**
     * List of possible choices.
     */
    choices: string[];
    /**
     * Maximum number of selectable items (default to Infinity)
     */
    maximumSelectable?: number;
    /**
     * Prefix for translating each choice.
     */
    choicesTranslatePrefix?: string;
}
