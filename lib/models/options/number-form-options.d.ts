export interface NumberFormOptions {
    /** Optional question label */
    label?: string;
    /** Optional question icon */
    icon?: string;
    /** Is the question required? */
    required?: boolean;
    /** Question field's width in percent */
    width?: number;
}
