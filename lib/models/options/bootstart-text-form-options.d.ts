export interface BootstartTextFormOptions {
    /**
     * Question label.
     */
    label?: string;
    /**
     * Icon of the question.
     */
    icon?: string;
    /**
     * Is this question required?
     */
    required?: boolean;
    /**
     * Width of the field (as percentage)
     */
    width?: number;
}
