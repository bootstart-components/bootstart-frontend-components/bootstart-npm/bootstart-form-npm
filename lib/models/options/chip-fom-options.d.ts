export interface ChipFomOptions {
    /** List of possible choices */
    choices: string[];
    /** Optional question label */
    questionLabel?: string;
    /** Optional question icon */
    questionIcon?: string;
    /** Maximum number of selectable items */
    maximumSelectable?: number;
    /** Optional prefix for choice translation */
    choicesTranslatePrefix?: string;
}
