export interface ChipsFormOptions {
    /** List of possible choices */
    choices: string[];
    /** Maximum number of selectable items */
    maximumSelectable?: number;
    /** Optional prefix for choice translation */
    choicesTranslatePrefix?: string;
}
