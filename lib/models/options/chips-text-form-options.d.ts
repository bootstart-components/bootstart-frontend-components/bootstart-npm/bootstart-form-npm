export interface ChipsTextFormOptions {
    /** Question Label */
    label?: string;
    /** Question Icon */
    icon?: string;
    /** Input placeholder */
    placeholder?: string;
    /** Separator values */
    separator: number[];
    /** Maximum input */
    maximum?: number;
    /** Chip Color */
    chipColor: string;
}
