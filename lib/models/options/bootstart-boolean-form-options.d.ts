export interface BootstartBooleanFormOptions {
    /**
     * Question label.
     */
    label?: string;
    /**
     * Icon of the question.
     */
    icon?: string;
    /**
     * Color of the component, when active
     */
    color?: string;
}
