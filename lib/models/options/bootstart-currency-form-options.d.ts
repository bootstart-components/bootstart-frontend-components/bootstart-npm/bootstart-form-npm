export interface BootstartCurrencyFormOptions {
    /**
     * Question label.
     */
    label?: string;
    /**
     * Icon of the question.
     */
    icon?: string;
    /**
     * Is this question required?
     */
    required?: boolean;
    /**
     * Width of the field (as percentage)
     */
    width?: number;
    /**
     * Text alignment in input. (default: right)
     */
    align?: string;
    /**
     * If true can input negative values. (default: true)
     */
    allowNegative?: boolean;
    /**
     * Separator of decimals (default: '.')
     */
    decimal?: string;
    /**
     * Number of decimal places (default: 2)
     */
    precision?: number;
    /**
     * Money prefix (default: '$ ')
     */
    prefix?: string;
    /**
     * Money suffix (default: '')
     */
    suffix?: string;
    /**
     * Separator of thousands (default: ',')
     */
    thousands?: string;
    /**
     * When true (default), the value of the clean field will be null, when false the value will be 0
     */
    nullable?: boolean;
}
