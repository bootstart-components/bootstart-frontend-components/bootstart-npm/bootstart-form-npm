export interface BootstartSelectFormOptions {
    /**
     * Question label.
     */
    label?: string;
    /**
     * Icon of the question.
     */
    icon?: string;
    /**
     * Is this question required?
     */
    required?: boolean;
    /**
     * Width of the field (as percentage)
     */
    width?: number;
    /**
     * List of possible choices.
     */
    choices: string[];
    /**
     * Translation prefix for each choice.
     */
    choicesTranslatePrefix?: string;
}
