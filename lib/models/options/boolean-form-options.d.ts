export interface BooleanFormOptions {
    /** Optional question label */
    label?: string;
    /** Optional question icon */
    icon?: string;
    /** Toggler's color */
    color?: string;
}
