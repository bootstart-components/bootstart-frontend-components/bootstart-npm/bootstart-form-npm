export interface BootstartChipsTextFormOptions {
    /**
     * Question label.
     */
    label?: string;
    /**
     * Icon of the question.
     */
    icon?: string;
    /**
     * Placeholder
     */
    placeholder?: string;
    /**
     * List of keys separator (default to ENTER)
     */
    separator?: number[];
    /**
     * Maximum chips that can be produced
     */
    maximum?: number;
    /**
     * Colors of the chips
     */
    chipColor?: string;
}
