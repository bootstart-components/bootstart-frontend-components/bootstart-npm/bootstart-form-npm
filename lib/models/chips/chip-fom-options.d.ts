export interface ChipFomOptions {
    /** Optional question label */
    questionLabel?: string;
    /** Optional question icon */
    questionIcon?: string;
    /** Maximum number of selectable items */
    maximumSelectable: number;
    /** List of possible choices */
    choices: string[];
    /** Optional prefix for choice translation */
    choicesTranslatePrefix?: string;
}
