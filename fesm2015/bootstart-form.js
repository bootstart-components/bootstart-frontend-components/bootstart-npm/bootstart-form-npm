import { Component, Input, EventEmitter, Output, NgModule } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CommonModule } from '@angular/common';
import { MatChipsModule, MatFormFieldModule, MatIconModule, MatInputModule, MatOptionModule, MatSelectModule, MatSlideToggleModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { NgxCurrencyModule } from 'ngx-currency';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartTextFormComponent {
    constructor() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('width', 100);
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartTextFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-text-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <input matInput type="text" [formControl]="control" [required]="options.required">
</mat-form-field>
`,
                styles: [`.full-width{width:100%}`]
            },] },
];
/** @nocollapse */
BootstartTextFormComponent.ctorParameters = () => [];
BootstartTextFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartParagraphFormComponent {
    constructor() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('width', 100);
        this._replaceUndefinedOption('rows', 6);
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartParagraphFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-paragraph-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <textarea matInput type="paragraph"
            [formControl]="control"
            [required]="options.required"
            rows="{{options.rows}}"></textarea>
</mat-form-field>
`,
                styles: [`.full-width{width:100%}`]
            },] },
];
/** @nocollapse */
BootstartParagraphFormComponent.ctorParameters = () => [];
BootstartParagraphFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartChipsFormComponent {
    constructor() {
        this.selection = new SelectionModel(true);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('choicesTranslatePrefix', '');
        this._replaceUndefinedOption('maximumSelectable', Infinity);
        this.selection.select(...this.formArray.value);
    }
    /**
     * What happens when selection changes
     * @param {?} value Chip's value that has changed.
     * @return {?}
     */
    selectionChanges(value) {
        if (this.selection.selected.length >= this.options.maximumSelectable && !this.selection.isSelected(value)) {
            return;
        }
        this.selection.toggle(value);
        if (this.selection.isSelected(value)) {
            this.formArray.push(new FormControl(value));
        }
        else {
            this.formArray.removeAt(this.formArray.controls.findIndex(x => x.value === value));
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    isClickable(value) {
        if (this.selection.selected.length >= this.options.maximumSelectable) {
            return this.selection.isSelected(value);
        }
        return true;
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartChipsFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-chips-form',
                template: `<mat-chip-list>
  <mat-chip *ngFor="let choice of options.choices"
            (click)="selectionChanges(choice)" selected
            [disabled]="selection.selected.length >= options.maximumSelectable && !selection.isSelected(choice)"
            [ngClass]="isClickable(choice) ? 'bootstart-clickable-chip' : 'bootstart-non-clickable-chip'"
            [color]="selection.isSelected(choice) ? 'primary' : 'none'">

    <mat-chip-avatar *ngIf="selection.isSelected(choice)">
      <span class="fas fa-check"></span>
    </mat-chip-avatar>

    {{options.choicesTranslatePrefix + choice|translate}}
  </mat-chip>
</mat-chip-list>
`,
                styles: [`.bootstart-clickable-chip{cursor:pointer}.bootstart-non-clickable-chip{cursor:not-allowed}`]
            },] },
];
/** @nocollapse */
BootstartChipsFormComponent.ctorParameters = () => [];
BootstartChipsFormComponent.propDecorators = {
    formArray: [{ type: Input }],
    options: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartSelectFormComponent {
    constructor() {
        this.control = new FormControl('');
        this.selectionChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('choicesTranslatePrefix', '');
        this._replaceUndefinedOption('width', 100);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    selectionChanges(event) {
        this.selectionChange.emit(event);
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartSelectFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-select-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <mat-select [required]="options.required" [formControl]="control" (selectionChange)="selectionChanges($event)">
    <mat-option *ngFor="let choice of options.choices" value="{{choice}}">
      {{options.choicesTranslatePrefix + choice|translate}}
    </mat-option>
  </mat-select>
</mat-form-field>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartSelectFormComponent.ctorParameters = () => [];
BootstartSelectFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }],
    selectionChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartNumberFormComponent {
    constructor() {
        this.control = new FormControl('');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('width', '100');
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartNumberFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-number-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <input matInput type="number" [formControl]="control" [required]="options.required">
</mat-form-field>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartNumberFormComponent.ctorParameters = () => [];
BootstartNumberFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartBooleanFormComponent {
    constructor() {
        this.control = new FormControl(false);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('color', 'primary');
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartBooleanFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-boolean-form',
                template: `<mat-slide-toggle [formControl]="control" [color]="options.color">
  <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
  <span *ngIf="options.icon">&nbsp;</span>
  <span *ngIf="options.label">{{options.label|translate}}</span>
</mat-slide-toggle>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartBooleanFormComponent.ctorParameters = () => [];
BootstartBooleanFormComponent.propDecorators = {
    control: [{ type: Input }],
    options: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartChipsTextFormComponent {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('maximum', Infinity);
        this._replaceUndefinedOption('separator', [ENTER, COMMA]);
        this._replaceUndefinedOption('chipColor', 'primary');
    }
    /**
     * @param {?} event
     * @return {?}
     */
    addChip(event) {
        if (this.formArray.controls.length < this.options.maximum) {
            /** @type {?} */
            const input = event.input;
            /** @type {?} */
            const value = event.value;
            if ((value || '').trim()) {
                this.formArray.push(new FormControl(value.trim()));
            }
            // Reset the input value
            if (input) {
                input.value = '';
            }
        }
    }
    /**
     * @param {?} chip
     * @return {?}
     */
    removeChip(chip) {
        /** @type {?} */
        const index = this.formArray.value.indexOf(chip);
        if (index >= 0) {
            this.formArray.removeAt(index);
        }
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartChipsTextFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-chips-text-form',
                template: `<mat-label>
  <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
  <span *ngIf="options.icon">&nbsp;</span>
  <span *ngIf="options.label">{{options.label|translate}}</span>
</mat-label>

<mat-chip-list #chipList>
  <mat-chip *ngFor="let chip of formArray.value"
            selected
            [color]="options.chipColor"
            [selectable]="false"
            [removable]="true"
            (removed)="removeChip(chip)">
    {{chip}}
    <mat-icon matChipRemove>cancel</mat-icon>
  </mat-chip>

  <input matInput type="text"
         *ngIf="formArray.controls.length < options.maximum"
         placeholder="{{options.placeholder|translate}}"
         [matChipInputFor]="chipList"
         [matChipInputSeparatorKeyCodes]="options.separator"
         (matChipInputTokenEnd)="addChip($event)">

</mat-chip-list>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartChipsTextFormComponent.ctorParameters = () => [];
BootstartChipsTextFormComponent.propDecorators = {
    options: [{ type: Input }],
    formArray: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartCurrencyFormComponent {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._replaceUndefinedOption('width', 100);
        this._replaceUndefinedOption('align', 'left');
        this._replaceUndefinedOption('allowNegative', true);
        this._replaceUndefinedOption('decimal', '.');
        this._replaceUndefinedOption('precision', 2);
        this._replaceUndefinedOption('prefix', '$ ');
        this._replaceUndefinedOption('suffix', '');
        this._replaceUndefinedOption('thousands', ',');
        this._replaceUndefinedOption('nullable', true);
    }
    /**
     * @param {?} field
     * @param {?} defaultValue
     * @return {?}
     */
    _replaceUndefinedOption(field, defaultValue) {
        if (this.options[field] === undefined) {
            this.options[field] = defaultValue;
        }
    }
}
BootstartCurrencyFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-currency-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{options.label|translate}}</span>
  </mat-label>

  <form [formGroup]="group">
    <input currencyMask matInput
           formControlName="{{controlName}}"
           [required]="options.required"
           [options]="{
           align: options.align,
           allowNegative: options.allowNegative,
           decimal: options.decimal,
           precision: options.precision,
           prefix: options.prefix,
           suffix: options.suffix,
           thousands: options.thousands,
           nullable: options.nullable
           }">
  </form>
</mat-form-field>
`,
                styles: [``]
            },] },
];
/** @nocollapse */
BootstartCurrencyFormComponent.ctorParameters = () => [];
BootstartCurrencyFormComponent.propDecorators = {
    group: [{ type: Input }],
    controlName: [{ type: Input }],
    options: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartFormModule {
}
BootstartFormModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatInputModule,
                    MatFormFieldModule,
                    MatIconModule,
                    MatChipsModule,
                    MatSelectModule,
                    MatOptionModule,
                    MatSlideToggleModule,
                    NgxCurrencyModule,
                    TranslateModule
                ],
                declarations: [
                    BootstartTextFormComponent,
                    BootstartParagraphFormComponent,
                    BootstartChipsFormComponent,
                    BootstartSelectFormComponent,
                    BootstartNumberFormComponent,
                    BootstartBooleanFormComponent,
                    BootstartChipsTextFormComponent,
                    BootstartCurrencyFormComponent
                ],
                exports: [
                    BootstartTextFormComponent,
                    BootstartParagraphFormComponent,
                    BootstartChipsFormComponent,
                    BootstartSelectFormComponent,
                    BootstartNumberFormComponent,
                    BootstartBooleanFormComponent,
                    BootstartChipsTextFormComponent,
                    BootstartCurrencyFormComponent
                ]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @param {?} value
 * @return {?}
 */
function SafeSetFormArrayValue(value) {
    /** @type {?} */
    const builder = new FormBuilder();
    if (typeof value === 'object') {
        return builder.array(value);
    }
    else {
        return builder.array([]);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { BootstartFormModule, SafeSetFormArrayValue, BootstartBooleanFormComponent as ɵf, BootstartChipsFormComponent as ɵc, BootstartChipsTextFormComponent as ɵg, BootstartCurrencyFormComponent as ɵh, BootstartNumberFormComponent as ɵe, BootstartParagraphFormComponent as ɵb, BootstartSelectFormComponent as ɵd, BootstartTextFormComponent as ɵa };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZvcm0uanMubWFwIiwic291cmNlcyI6WyJuZzovL2Jvb3RzdGFydC1mb3JtL2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC10ZXh0LWZvcm0vYm9vdHN0YXJ0LXRleHQtZm9ybS5jb21wb25lbnQudHMiLCJuZzovL2Jvb3RzdGFydC1mb3JtL2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS9ib290c3RhcnQtcGFyYWdyYXBoLWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvY29tcG9uZW50cy9ib290c3RhcnQtY2hpcHMtZm9ybS9ib290c3RhcnQtY2hpcHMtZm9ybS5jb21wb25lbnQudHMiLCJuZzovL2Jvb3RzdGFydC1mb3JtL2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS9ib290c3RhcnQtc2VsZWN0LWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvY29tcG9uZW50cy9ib290c3RhcnQtbnVtYmVyLWZvcm0vYm9vdHN0YXJ0LW51bWJlci1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS9ib290c3RhcnQtYm9vbGVhbi1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS9ib290c3RhcnQtY2hpcHMtdGV4dC1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0vYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvYm9vdHN0YXJ0LWZvcm0ubW9kdWxlLnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvZnVuY3Rpb25zL3NhZmUtc2V0LWZvcm0tYXJyYXktdmFsdWUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnRUZXh0Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC10ZXh0LWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LXRleHQtZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWZvcm0tZmllbGQgW25nU3R5bGVdPVwieyd3aWR0aCcgOiBvcHRpb25zLndpZHRoKyclJ31cIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxpbnB1dCBtYXRJbnB1dCB0eXBlPVwidGV4dFwiIFtmb3JtQ29udHJvbF09XCJjb250cm9sXCIgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIj5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2AuZnVsbC13aWR0aHt3aWR0aDoxMDAlfWBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0VGV4dEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRUZXh0Rm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Qm9vdHN0YXJ0UGFyYWdyYXBoRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS1vcHRpb25zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtcGFyYWdyYXBoLWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8dGV4dGFyZWEgbWF0SW5wdXQgdHlwZT1cInBhcmFncmFwaFwiXG4gICAgICAgICAgICBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiXG4gICAgICAgICAgICBbcmVxdWlyZWRdPVwib3B0aW9ucy5yZXF1aXJlZFwiXG4gICAgICAgICAgICByb3dzPVwie3tvcHRpb25zLnJvd3N9fVwiPjwvdGV4dGFyZWE+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgLmZ1bGwtd2lkdGh7d2lkdGg6MTAwJX1gXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydFBhcmFncmFwaEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRQYXJhZ3JhcGhGb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woJycpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignd2lkdGgnLCAxMDApO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3Jvd3MnLCA2KTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7U2VsZWN0aW9uTW9kZWx9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XG5pbXBvcnQge0Jvb3RzdGFydENoaXBzRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1jaGlwcy1mb3JtLW9wdGlvbnMnO1xuaW1wb3J0IHtGb3JtQXJyYXksIEZvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWNoaXBzLWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1jaGlwLWxpc3Q+XG4gIDxtYXQtY2hpcCAqbmdGb3I9XCJsZXQgY2hvaWNlIG9mIG9wdGlvbnMuY2hvaWNlc1wiXG4gICAgICAgICAgICAoY2xpY2spPVwic2VsZWN0aW9uQ2hhbmdlcyhjaG9pY2UpXCIgc2VsZWN0ZWRcbiAgICAgICAgICAgIFtkaXNhYmxlZF09XCJzZWxlY3Rpb24uc2VsZWN0ZWQubGVuZ3RoID49IG9wdGlvbnMubWF4aW11bVNlbGVjdGFibGUgJiYgIXNlbGVjdGlvbi5pc1NlbGVjdGVkKGNob2ljZSlcIlxuICAgICAgICAgICAgW25nQ2xhc3NdPVwiaXNDbGlja2FibGUoY2hvaWNlKSA/ICdib290c3RhcnQtY2xpY2thYmxlLWNoaXAnIDogJ2Jvb3RzdGFydC1ub24tY2xpY2thYmxlLWNoaXAnXCJcbiAgICAgICAgICAgIFtjb2xvcl09XCJzZWxlY3Rpb24uaXNTZWxlY3RlZChjaG9pY2UpID8gJ3ByaW1hcnknIDogJ25vbmUnXCI+XG5cbiAgICA8bWF0LWNoaXAtYXZhdGFyICpuZ0lmPVwic2VsZWN0aW9uLmlzU2VsZWN0ZWQoY2hvaWNlKVwiPlxuICAgICAgPHNwYW4gY2xhc3M9XCJmYXMgZmEtY2hlY2tcIj48L3NwYW4+XG4gICAgPC9tYXQtY2hpcC1hdmF0YXI+XG5cbiAgICB7e29wdGlvbnMuY2hvaWNlc1RyYW5zbGF0ZVByZWZpeCArIGNob2ljZXx0cmFuc2xhdGV9fVxuICA8L21hdC1jaGlwPlxuPC9tYXQtY2hpcC1saXN0PlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgLmJvb3RzdGFydC1jbGlja2FibGUtY2hpcHtjdXJzb3I6cG9pbnRlcn0uYm9vdHN0YXJ0LW5vbi1jbGlja2FibGUtY2hpcHtjdXJzb3I6bm90LWFsbG93ZWR9YF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRDaGlwc0Zvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGZvcm1BcnJheTogRm9ybUFycmF5O1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRDaGlwc0Zvcm1PcHRpb25zO1xuXG4gIHNlbGVjdGlvbiA9IG5ldyBTZWxlY3Rpb25Nb2RlbDxzdHJpbmc+KHRydWUpO1xuXG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdjaG9pY2VzVHJhbnNsYXRlUHJlZml4JywgJycpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ21heGltdW1TZWxlY3RhYmxlJywgSW5maW5pdHkpO1xuICAgIHRoaXMuc2VsZWN0aW9uLnNlbGVjdCguLi50aGlzLmZvcm1BcnJheS52YWx1ZSk7XG4gIH1cblxuICAvKipcbiAgICogV2hhdCBoYXBwZW5zIHdoZW4gc2VsZWN0aW9uIGNoYW5nZXNcbiAgICogQHBhcmFtIHZhbHVlIENoaXAncyB2YWx1ZSB0aGF0IGhhcyBjaGFuZ2VkLlxuICAgKi9cbiAgcHVibGljIHNlbGVjdGlvbkNoYW5nZXModmFsdWU6IHN0cmluZyk6IHZvaWQge1xuICAgIGlmICh0aGlzLnNlbGVjdGlvbi5zZWxlY3RlZC5sZW5ndGggPj0gdGhpcy5vcHRpb25zLm1heGltdW1TZWxlY3RhYmxlICYmICF0aGlzLnNlbGVjdGlvbi5pc1NlbGVjdGVkKHZhbHVlKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnNlbGVjdGlvbi50b2dnbGUodmFsdWUpO1xuICAgIGlmICh0aGlzLnNlbGVjdGlvbi5pc1NlbGVjdGVkKHZhbHVlKSkge1xuICAgICAgdGhpcy5mb3JtQXJyYXkucHVzaChuZXcgRm9ybUNvbnRyb2wodmFsdWUpKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5mb3JtQXJyYXkucmVtb3ZlQXQodGhpcy5mb3JtQXJyYXkuY29udHJvbHMuZmluZEluZGV4KHggPT4geC52YWx1ZSA9PT0gdmFsdWUpKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgaXNDbGlja2FibGUodmFsdWU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIGlmICh0aGlzLnNlbGVjdGlvbi5zZWxlY3RlZC5sZW5ndGggPj0gdGhpcy5vcHRpb25zLm1heGltdW1TZWxlY3RhYmxlKSB7XG4gICAgICByZXR1cm4gdGhpcy5zZWxlY3Rpb24uaXNTZWxlY3RlZCh2YWx1ZSk7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIiwiaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Qm9vdHN0YXJ0U2VsZWN0Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS1vcHRpb25zJztcbmltcG9ydCB7TWF0U2VsZWN0aW9uTGlzdENoYW5nZX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1zZWxlY3QtZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWZvcm0tZmllbGQgW25nU3R5bGVdPVwieyd3aWR0aCcgOiBvcHRpb25zLndpZHRoKyclJ31cIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxtYXQtc2VsZWN0IFtyZXF1aXJlZF09XCJvcHRpb25zLnJlcXVpcmVkXCIgW2Zvcm1Db250cm9sXT1cImNvbnRyb2xcIiAoc2VsZWN0aW9uQ2hhbmdlKT1cInNlbGVjdGlvbkNoYW5nZXMoJGV2ZW50KVwiPlxuICAgIDxtYXQtb3B0aW9uICpuZ0Zvcj1cImxldCBjaG9pY2Ugb2Ygb3B0aW9ucy5jaG9pY2VzXCIgdmFsdWU9XCJ7e2Nob2ljZX19XCI+XG4gICAgICB7e29wdGlvbnMuY2hvaWNlc1RyYW5zbGF0ZVByZWZpeCArIGNob2ljZXx0cmFuc2xhdGV9fVxuICAgIDwvbWF0LW9wdGlvbj5cbiAgPC9tYXQtc2VsZWN0PlxuPC9tYXQtZm9ybS1maWVsZD5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYGBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgLyoqIEFzc29jaWF0ZWQgZm9ybSBjb250cm9sICovXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuXG4gIC8qKiBTZWxlY3Qgb3B0aW9ucyAqL1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRTZWxlY3RGb3JtT3B0aW9ucztcblxuICAvKiogT24gQ2hhbmdlIGVtaXR0ZXIgKi9cbiAgQE91dHB1dCgpIHNlbGVjdGlvbkNoYW5nZTogRXZlbnRFbWl0dGVyPE1hdFNlbGVjdGlvbkxpc3RDaGFuZ2U+O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuY29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgnJyk7XG4gICAgdGhpcy5zZWxlY3Rpb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPE1hdFNlbGVjdGlvbkxpc3RDaGFuZ2U+KCk7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdjaG9pY2VzVHJhbnNsYXRlUHJlZml4JywgJycpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgfVxuXG4gIHB1YmxpYyBzZWxlY3Rpb25DaGFuZ2VzKGV2ZW50OiBNYXRTZWxlY3Rpb25MaXN0Q2hhbmdlKTogdm9pZCB7XG4gICAgdGhpcy5zZWxlY3Rpb25DaGFuZ2UuZW1pdChldmVudCk7XG4gIH1cblxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Zvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0Jvb3RzdGFydE51bWJlckZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvb3B0aW9ucy9ib290c3RhcnQtbnVtYmVyLWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LW51bWJlci1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPGlucHV0IG1hdElucHV0IHR5cGU9XCJudW1iZXJcIiBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiIFtyZXF1aXJlZF09XCJvcHRpb25zLnJlcXVpcmVkXCI+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnROdW1iZXJGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBjb250cm9sOiBGb3JtQ29udHJvbDtcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0TnVtYmVyRm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgJzEwMCcpO1xuICB9XG5cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIiwiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnRCb29sZWFuRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LXNsaWRlLXRvZ2dsZSBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiIFtjb2xvcl09XCJvcHRpb25zLmNvbG9yXCI+XG4gIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuPC9tYXQtc2xpZGUtdG9nZ2xlPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRCb29sZWFuRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgY29udHJvbDogRm9ybUNvbnRyb2w7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydEJvb2xlYW5Gb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woZmFsc2UpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignY29sb3InLCAncHJpbWFyeScpO1xuICB9XG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1jaGlwcy10ZXh0LWZvcm0tb3B0aW9ucyc7XG5pbXBvcnQge0Zvcm1BcnJheSwgRm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Q09NTUEsIEVOVEVSfSBmcm9tICdAYW5ndWxhci9jZGsva2V5Y29kZXMnO1xuaW1wb3J0IHtNYXRDaGlwSW5wdXRFdmVudH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1jaGlwcy10ZXh0LWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1sYWJlbD5cbiAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG48L21hdC1sYWJlbD5cblxuPG1hdC1jaGlwLWxpc3QgI2NoaXBMaXN0PlxuICA8bWF0LWNoaXAgKm5nRm9yPVwibGV0IGNoaXAgb2YgZm9ybUFycmF5LnZhbHVlXCJcbiAgICAgICAgICAgIHNlbGVjdGVkXG4gICAgICAgICAgICBbY29sb3JdPVwib3B0aW9ucy5jaGlwQ29sb3JcIlxuICAgICAgICAgICAgW3NlbGVjdGFibGVdPVwiZmFsc2VcIlxuICAgICAgICAgICAgW3JlbW92YWJsZV09XCJ0cnVlXCJcbiAgICAgICAgICAgIChyZW1vdmVkKT1cInJlbW92ZUNoaXAoY2hpcClcIj5cbiAgICB7e2NoaXB9fVxuICAgIDxtYXQtaWNvbiBtYXRDaGlwUmVtb3ZlPmNhbmNlbDwvbWF0LWljb24+XG4gIDwvbWF0LWNoaXA+XG5cbiAgPGlucHV0IG1hdElucHV0IHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICpuZ0lmPVwiZm9ybUFycmF5LmNvbnRyb2xzLmxlbmd0aCA8IG9wdGlvbnMubWF4aW11bVwiXG4gICAgICAgICBwbGFjZWhvbGRlcj1cInt7b3B0aW9ucy5wbGFjZWhvbGRlcnx0cmFuc2xhdGV9fVwiXG4gICAgICAgICBbbWF0Q2hpcElucHV0Rm9yXT1cImNoaXBMaXN0XCJcbiAgICAgICAgIFttYXRDaGlwSW5wdXRTZXBhcmF0b3JLZXlDb2Rlc109XCJvcHRpb25zLnNlcGFyYXRvclwiXG4gICAgICAgICAobWF0Q2hpcElucHV0VG9rZW5FbmQpPVwiYWRkQ2hpcCgkZXZlbnQpXCI+XG5cbjwvbWF0LWNoaXAtbGlzdD5cbmAsXG4gICAgICAgICAgICAgc3R5bGVzOiBbYGBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybU9wdGlvbnM7XG4gIEBJbnB1dCgpIGZvcm1BcnJheTogRm9ybUFycmF5O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignbWF4aW11bScsIEluZmluaXR5KTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdzZXBhcmF0b3InLCBbRU5URVIsIENPTU1BXSk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignY2hpcENvbG9yJywgJ3ByaW1hcnknKTtcbiAgfVxuXG5cbiAgcHVibGljIGFkZENoaXAoZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZm9ybUFycmF5LmNvbnRyb2xzLmxlbmd0aCA8IHRoaXMub3B0aW9ucy5tYXhpbXVtKSB7XG4gICAgICBjb25zdCBpbnB1dCA9IGV2ZW50LmlucHV0O1xuICAgICAgY29uc3QgdmFsdWUgPSBldmVudC52YWx1ZTtcblxuICAgICAgaWYgKCh2YWx1ZSB8fCAnJykudHJpbSgpKSB7XG4gICAgICAgIHRoaXMuZm9ybUFycmF5LnB1c2gobmV3IEZvcm1Db250cm9sKHZhbHVlLnRyaW0oKSkpO1xuICAgICAgfVxuICAgICAgLy8gUmVzZXQgdGhlIGlucHV0IHZhbHVlXG4gICAgICBpZiAoaW5wdXQpIHtcbiAgICAgICAgaW5wdXQudmFsdWUgPSAnJztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgcmVtb3ZlQ2hpcChjaGlwOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBjb25zdCBpbmRleCA9IHRoaXMuZm9ybUFycmF5LnZhbHVlLmluZGV4T2YoY2hpcCk7XG5cbiAgICBpZiAoaW5kZXggPj0gMCkge1xuICAgICAgdGhpcy5mb3JtQXJyYXkucmVtb3ZlQXQoaW5kZXgpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydEN1cnJlbmN5Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscyc7XG5pbXBvcnQge0Zvcm1Hcm91cH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1jdXJyZW5jeS1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPGZvcm0gW2Zvcm1Hcm91cF09XCJncm91cFwiPlxuICAgIDxpbnB1dCBjdXJyZW5jeU1hc2sgbWF0SW5wdXRcbiAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwie3tjb250cm9sTmFtZX19XCJcbiAgICAgICAgICAgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIlxuICAgICAgICAgICBbb3B0aW9uc109XCJ7XG4gICAgICAgICAgIGFsaWduOiBvcHRpb25zLmFsaWduLFxuICAgICAgICAgICBhbGxvd05lZ2F0aXZlOiBvcHRpb25zLmFsbG93TmVnYXRpdmUsXG4gICAgICAgICAgIGRlY2ltYWw6IG9wdGlvbnMuZGVjaW1hbCxcbiAgICAgICAgICAgcHJlY2lzaW9uOiBvcHRpb25zLnByZWNpc2lvbixcbiAgICAgICAgICAgcHJlZml4OiBvcHRpb25zLnByZWZpeCxcbiAgICAgICAgICAgc3VmZml4OiBvcHRpb25zLnN1ZmZpeCxcbiAgICAgICAgICAgdGhvdXNhbmRzOiBvcHRpb25zLnRob3VzYW5kcyxcbiAgICAgICAgICAgbnVsbGFibGU6IG9wdGlvbnMubnVsbGFibGVcbiAgICAgICAgICAgfVwiPlxuICA8L2Zvcm0+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRDdXJyZW5jeUZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGdyb3VwOiBGb3JtR3JvdXA7XG4gIEBJbnB1dCgpIGNvbnRyb2xOYW1lOiBzdHJpbmc7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydEN1cnJlbmN5Rm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCd3aWR0aCcsIDEwMCk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignYWxpZ24nLCAnbGVmdCcpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2FsbG93TmVnYXRpdmUnLCB0cnVlKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdkZWNpbWFsJywgJy4nKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdwcmVjaXNpb24nLCAyKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdwcmVmaXgnLCAnJCAnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdzdWZmaXgnLCAnJyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbigndGhvdXNhbmRzJywgJywnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdudWxsYWJsZScsIHRydWUpO1xuICB9XG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRUZXh0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC10ZXh0LWZvcm0vYm9vdHN0YXJ0LXRleHQtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge0Zvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge1xuICBNYXRDaGlwc01vZHVsZSxcbiAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICBNYXRJY29uTW9kdWxlLFxuICBNYXRJbnB1dE1vZHVsZSxcbiAgTWF0T3B0aW9uTW9kdWxlLFxuICBNYXRTZWxlY3RNb2R1bGUsXG4gIE1hdFNsaWRlVG9nZ2xlTW9kdWxlXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7VHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7Qm9vdHN0YXJ0UGFyYWdyYXBoRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS9ib290c3RhcnQtcGFyYWdyYXBoLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWNoaXBzLWZvcm0vYm9vdHN0YXJ0LWNoaXBzLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS9ib290c3RhcnQtc2VsZWN0LWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1udW1iZXItZm9ybS9ib290c3RhcnQtbnVtYmVyLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtYm9vbGVhbi1mb3JtL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0uY29tcG9uZW50JztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1jaGlwcy10ZXh0LWZvcm0vYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRDdXJyZW5jeUZvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtY3VycmVuY3ktZm9ybS9ib290c3RhcnQtY3VycmVuY3ktZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtOZ3hDdXJyZW5jeU1vZHVsZX0gZnJvbSAnbmd4LWN1cnJlbmN5JztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIEZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJY29uTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRDaGlwc01vZHVsZSxcbiAgICAgICAgICAgICAgTWF0U2VsZWN0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRPcHRpb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdFNsaWRlVG9nZ2xlTW9kdWxlLFxuICAgICAgICAgICAgICBOZ3hDdXJyZW5jeU1vZHVsZSxcbiAgICAgICAgICAgICAgVHJhbnNsYXRlTW9kdWxlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydFRleHRGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRQYXJhZ3JhcGhGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDaGlwc0Zvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydFNlbGVjdEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydE51bWJlckZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydEJvb2xlYW5Gb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDaGlwc1RleHRGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRDdXJyZW5jeUZvcm1Db21wb25lbnRcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBleHBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0VGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydFBhcmFncmFwaEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzVGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudFxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0Rm9ybU1vZHVsZSB7XG59XG4iLCJpbXBvcnQge0Zvcm1BcnJheSwgRm9ybUJ1aWxkZXJ9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuZXhwb3J0IGZ1bmN0aW9uIFNhZmVTZXRGb3JtQXJyYXlWYWx1ZSh2YWx1ZTogYW55KTogRm9ybUFycmF5IHtcbiAgY29uc3QgYnVpbGRlciA9IG5ldyBGb3JtQnVpbGRlcigpO1xuICBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiBidWlsZGVyLmFycmF5KHZhbHVlKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gYnVpbGRlci5hcnJheShbXSk7XG4gIH1cbn1cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7O0FBQUE7SUF1QkU7UUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ3BDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7S0FDNUM7Ozs7OztJQUdPLHVCQUF1QixDQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7O1lBL0JKLFNBQVMsU0FBQztnQkFDRSxRQUFRLEVBQUsscUJBQXFCO2dCQUNsQyxRQUFRLEVBQUU7Ozs7Ozs7OztDQVN0QjtnQkFDWSxNQUFNLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQzthQUNwQzs7Ozs7c0JBR1QsS0FBSztzQkFDTCxLQUFLOzs7Ozs7O0FDckJSO0lBMEJFO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FDekM7Ozs7OztJQUdPLHVCQUF1QixDQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7O1lBbkNKLFNBQVMsU0FBQztnQkFDRSxRQUFRLEVBQUssMEJBQTBCO2dCQUN2QyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7OztDQVl0QjtnQkFDWSxNQUFNLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQzthQUNwQzs7Ozs7c0JBR1QsS0FBSztzQkFDTCxLQUFLOzs7Ozs7O0FDeEJSO0lBZ0NFO3lCQUhZLElBQUksY0FBYyxDQUFTLElBQUksQ0FBQztLQUkzQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLHVCQUF1QixDQUFDLG1CQUFtQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUNoRDs7Ozs7O0lBTU0sZ0JBQWdCLENBQUMsS0FBYTtRQUNuQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDekcsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1NBQzdDO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztTQUNwRjs7Ozs7O0lBR0ksV0FBVyxDQUFDLEtBQWE7UUFDOUIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRTtZQUNwRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3pDO1FBQ0QsT0FBTyxJQUFJLENBQUM7Ozs7Ozs7SUFJTix1QkFBdUIsQ0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7OztZQS9ESixTQUFTLFNBQUM7Z0JBQ0UsUUFBUSxFQUFLLHNCQUFzQjtnQkFDbkMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7OztDQWN0QjtnQkFDWSxNQUFNLEVBQUUsQ0FBQyw0RkFBNEYsQ0FBQzthQUN2Rzs7Ozs7d0JBR1QsS0FBSztzQkFDTCxLQUFLOzs7Ozs7O0FDM0JSO0lBa0NFO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksWUFBWSxFQUEwQixDQUFDO0tBQ25FOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0tBQzVDOzs7OztJQUVNLGdCQUFnQixDQUFDLEtBQTZCO1FBQ25ELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7O0lBSTNCLHVCQUF1QixDQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7O1lBL0NKLFNBQVMsU0FBQztnQkFDRSxRQUFRLEVBQUssdUJBQXVCO2dCQUNwQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Q0FhdEI7Z0JBQ1ksTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO2FBQ2I7Ozs7O3NCQUlULEtBQUs7c0JBR0wsS0FBSzs4QkFHTCxNQUFNOzs7Ozs7O0FDaENUO0lBdUJFO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNwQzs7OztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO0tBQzlDOzs7Ozs7SUFHTyx1QkFBdUIsQ0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7OztZQS9CSixTQUFTLFNBQUM7Z0JBQ0UsUUFBUSxFQUFLLHVCQUF1QjtnQkFDcEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Q0FTdEI7Z0JBQ1ksTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO2FBQ2I7Ozs7O3NCQUdULEtBQUs7c0JBQ0wsS0FBSzs7Ozs7OztBQ3JCUjtJQW1CRTtRQUNFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7S0FDdkM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQztLQUNsRDs7Ozs7O0lBRU8sdUJBQXVCLENBQUMsS0FBVSxFQUFFLFlBQWlCO1FBQzNELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLEVBQUU7WUFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7U0FDcEM7Ozs7WUExQkosU0FBUyxTQUFDO2dCQUNFLFFBQVEsRUFBSyx3QkFBd0I7Z0JBQ3JDLFFBQVEsRUFBRTs7Ozs7Q0FLdEI7Z0JBQ1ksTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO2FBQ2I7Ozs7O3NCQUdULEtBQUs7c0JBQ0wsS0FBSzs7Ozs7OztBQ2pCUjtJQXlDRTtLQUNDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7S0FDdEQ7Ozs7O0lBR00sT0FBTyxDQUFDLEtBQXdCO1FBQ3JDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFOztZQUN6RCxNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDOztZQUMxQixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBRTFCLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFO2dCQUN4QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ3BEOztZQUVELElBQUksS0FBSyxFQUFFO2dCQUNULEtBQUssQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2FBQ2xCO1NBQ0Y7Ozs7OztJQUdJLFVBQVUsQ0FBQyxJQUFZOztRQUM1QixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFakQsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDaEM7Ozs7Ozs7SUFHSyx1QkFBdUIsQ0FBQyxLQUFVLEVBQUUsWUFBaUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztTQUNwQzs7OztZQXZFSixTQUFTLFNBQUM7Z0JBQ0UsUUFBUSxFQUFLLDJCQUEyQjtnQkFDeEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBeUJ0QjtnQkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7YUFDYjs7Ozs7c0JBR1QsS0FBSzt3QkFDTCxLQUFLOzs7Ozs7O0FDdkNSO0lBc0NFO0tBQ0M7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDaEQ7Ozs7OztJQUVPLHVCQUF1QixDQUFDLEtBQVUsRUFBRSxZQUFpQjtRQUMzRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO1NBQ3BDOzs7O1lBcERKLFNBQVMsU0FBQztnQkFDRSxRQUFRLEVBQUsseUJBQXlCO2dCQUN0QyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBdUJ0QjtnQkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7YUFDYjs7Ozs7b0JBR1QsS0FBSzswQkFDTCxLQUFLO3NCQUNMLEtBQUs7Ozs7Ozs7QUNwQ1I7OztZQXVCQyxRQUFRLFNBQUM7Z0JBQ0UsT0FBTyxFQUFPO29CQUNaLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGNBQWM7b0JBQ2Qsa0JBQWtCO29CQUNsQixhQUFhO29CQUNiLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixlQUFlO29CQUNmLG9CQUFvQjtvQkFDcEIsaUJBQWlCO29CQUNqQixlQUFlO2lCQUNoQjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1osMEJBQTBCO29CQUMxQiwrQkFBK0I7b0JBQy9CLDJCQUEyQjtvQkFDM0IsNEJBQTRCO29CQUM1Qiw0QkFBNEI7b0JBQzVCLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQiw4QkFBOEI7aUJBQy9CO2dCQUNELE9BQU8sRUFBTztvQkFDWiwwQkFBMEI7b0JBQzFCLCtCQUErQjtvQkFDL0IsMkJBQTJCO29CQUMzQiw0QkFBNEI7b0JBQzVCLDRCQUE0QjtvQkFDNUIsNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLDhCQUE4QjtpQkFDL0I7YUFDRjs7Ozs7OztBQzFEWDs7OztBQUVBLCtCQUFzQyxLQUFVOztJQUM5QyxNQUFNLE9BQU8sR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO0lBQ2xDLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1FBQzdCLE9BQU8sT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUM3QjtTQUFNO1FBQ0wsT0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQzFCO0NBQ0Y7Ozs7Ozs7Ozs7Ozs7OyJ9