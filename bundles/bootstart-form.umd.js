(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('@angular/cdk/collections'), require('@angular/cdk/keycodes'), require('@angular/common'), require('@angular/material'), require('@ngx-translate/core'), require('ngx-currency')) :
    typeof define === 'function' && define.amd ? define('bootstart-form', ['exports', '@angular/core', '@angular/forms', '@angular/cdk/collections', '@angular/cdk/keycodes', '@angular/common', '@angular/material', '@ngx-translate/core', 'ngx-currency'], factory) :
    (factory((global['bootstart-form'] = {}),global.ng.core,global.ng.forms,global.ng.cdk.collections,global.ng.cdk.keycodes,global.ng.common,global.ng.material,null,null));
}(this, (function (exports,core,forms,collections,keycodes,common,material,core$1,ngxCurrency) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartTextFormComponent = (function () {
        function BootstartTextFormComponent() {
            this.control = new forms.FormControl('');
        }
        /**
         * @return {?}
         */
        BootstartTextFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._replaceUndefinedOption('width', 100);
            };
        /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
        BootstartTextFormComponent.prototype._replaceUndefinedOption = /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
            function (field, defaultValue) {
                if (this.options[field] === undefined) {
                    this.options[field] = defaultValue;
                }
            };
        BootstartTextFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-text-form',
                        template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <input matInput type=\"text\" [formControl]=\"control\" [required]=\"options.required\">\n</mat-form-field>\n",
                        styles: [".full-width{width:100%}"]
                    },] },
        ];
        /** @nocollapse */
        BootstartTextFormComponent.ctorParameters = function () { return []; };
        BootstartTextFormComponent.propDecorators = {
            control: [{ type: core.Input }],
            options: [{ type: core.Input }]
        };
        return BootstartTextFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartParagraphFormComponent = (function () {
        function BootstartParagraphFormComponent() {
            this.control = new forms.FormControl('');
        }
        /**
         * @return {?}
         */
        BootstartParagraphFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._replaceUndefinedOption('width', 100);
                this._replaceUndefinedOption('rows', 6);
            };
        /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
        BootstartParagraphFormComponent.prototype._replaceUndefinedOption = /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
            function (field, defaultValue) {
                if (this.options[field] === undefined) {
                    this.options[field] = defaultValue;
                }
            };
        BootstartParagraphFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-paragraph-form',
                        template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <textarea matInput type=\"paragraph\"\n            [formControl]=\"control\"\n            [required]=\"options.required\"\n            rows=\"{{options.rows}}\"></textarea>\n</mat-form-field>\n",
                        styles: [".full-width{width:100%}"]
                    },] },
        ];
        /** @nocollapse */
        BootstartParagraphFormComponent.ctorParameters = function () { return []; };
        BootstartParagraphFormComponent.propDecorators = {
            control: [{ type: core.Input }],
            options: [{ type: core.Input }]
        };
        return BootstartParagraphFormComponent;
    }());

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartChipsFormComponent = (function () {
        function BootstartChipsFormComponent() {
            this.selection = new collections.SelectionModel(true);
        }
        /**
         * @return {?}
         */
        BootstartChipsFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._replaceUndefinedOption('choicesTranslatePrefix', '');
                this._replaceUndefinedOption('maximumSelectable', Infinity);
                (_a = this.selection).select.apply(_a, __spread(this.formArray.value));
                var _a;
            };
        /**
         * What happens when selection changes
         * @param {?} value Chip's value that has changed.
         * @return {?}
         */
        BootstartChipsFormComponent.prototype.selectionChanges = /**
         * What happens when selection changes
         * @param {?} value Chip's value that has changed.
         * @return {?}
         */
            function (value) {
                if (this.selection.selected.length >= this.options.maximumSelectable && !this.selection.isSelected(value)) {
                    return;
                }
                this.selection.toggle(value);
                if (this.selection.isSelected(value)) {
                    this.formArray.push(new forms.FormControl(value));
                }
                else {
                    this.formArray.removeAt(this.formArray.controls.findIndex(function (x) { return x.value === value; }));
                }
            };
        /**
         * @param {?} value
         * @return {?}
         */
        BootstartChipsFormComponent.prototype.isClickable = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                if (this.selection.selected.length >= this.options.maximumSelectable) {
                    return this.selection.isSelected(value);
                }
                return true;
            };
        /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
        BootstartChipsFormComponent.prototype._replaceUndefinedOption = /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
            function (field, defaultValue) {
                if (this.options[field] === undefined) {
                    this.options[field] = defaultValue;
                }
            };
        BootstartChipsFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-chips-form',
                        template: "<mat-chip-list>\n  <mat-chip *ngFor=\"let choice of options.choices\"\n            (click)=\"selectionChanges(choice)\" selected\n            [disabled]=\"selection.selected.length >= options.maximumSelectable && !selection.isSelected(choice)\"\n            [ngClass]=\"isClickable(choice) ? 'bootstart-clickable-chip' : 'bootstart-non-clickable-chip'\"\n            [color]=\"selection.isSelected(choice) ? 'primary' : 'none'\">\n\n    <mat-chip-avatar *ngIf=\"selection.isSelected(choice)\">\n      <span class=\"fas fa-check\"></span>\n    </mat-chip-avatar>\n\n    {{options.choicesTranslatePrefix + choice|translate}}\n  </mat-chip>\n</mat-chip-list>\n",
                        styles: [".bootstart-clickable-chip{cursor:pointer}.bootstart-non-clickable-chip{cursor:not-allowed}"]
                    },] },
        ];
        /** @nocollapse */
        BootstartChipsFormComponent.ctorParameters = function () { return []; };
        BootstartChipsFormComponent.propDecorators = {
            formArray: [{ type: core.Input }],
            options: [{ type: core.Input }]
        };
        return BootstartChipsFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartSelectFormComponent = (function () {
        function BootstartSelectFormComponent() {
            this.control = new forms.FormControl('');
            this.selectionChange = new core.EventEmitter();
        }
        /**
         * @return {?}
         */
        BootstartSelectFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._replaceUndefinedOption('choicesTranslatePrefix', '');
                this._replaceUndefinedOption('width', 100);
            };
        /**
         * @param {?} event
         * @return {?}
         */
        BootstartSelectFormComponent.prototype.selectionChanges = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                this.selectionChange.emit(event);
            };
        /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
        BootstartSelectFormComponent.prototype._replaceUndefinedOption = /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
            function (field, defaultValue) {
                if (this.options[field] === undefined) {
                    this.options[field] = defaultValue;
                }
            };
        BootstartSelectFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-select-form',
                        template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <mat-select [required]=\"options.required\" [formControl]=\"control\" (selectionChange)=\"selectionChanges($event)\">\n    <mat-option *ngFor=\"let choice of options.choices\" value=\"{{choice}}\">\n      {{options.choicesTranslatePrefix + choice|translate}}\n    </mat-option>\n  </mat-select>\n</mat-form-field>\n",
                        styles: [""]
                    },] },
        ];
        /** @nocollapse */
        BootstartSelectFormComponent.ctorParameters = function () { return []; };
        BootstartSelectFormComponent.propDecorators = {
            control: [{ type: core.Input }],
            options: [{ type: core.Input }],
            selectionChange: [{ type: core.Output }]
        };
        return BootstartSelectFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartNumberFormComponent = (function () {
        function BootstartNumberFormComponent() {
            this.control = new forms.FormControl('');
        }
        /**
         * @return {?}
         */
        BootstartNumberFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._replaceUndefinedOption('width', '100');
            };
        /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
        BootstartNumberFormComponent.prototype._replaceUndefinedOption = /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
            function (field, defaultValue) {
                if (this.options[field] === undefined) {
                    this.options[field] = defaultValue;
                }
            };
        BootstartNumberFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-number-form',
                        template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <input matInput type=\"number\" [formControl]=\"control\" [required]=\"options.required\">\n</mat-form-field>\n",
                        styles: [""]
                    },] },
        ];
        /** @nocollapse */
        BootstartNumberFormComponent.ctorParameters = function () { return []; };
        BootstartNumberFormComponent.propDecorators = {
            control: [{ type: core.Input }],
            options: [{ type: core.Input }]
        };
        return BootstartNumberFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartBooleanFormComponent = (function () {
        function BootstartBooleanFormComponent() {
            this.control = new forms.FormControl(false);
        }
        /**
         * @return {?}
         */
        BootstartBooleanFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._replaceUndefinedOption('color', 'primary');
            };
        /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
        BootstartBooleanFormComponent.prototype._replaceUndefinedOption = /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
            function (field, defaultValue) {
                if (this.options[field] === undefined) {
                    this.options[field] = defaultValue;
                }
            };
        BootstartBooleanFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-boolean-form',
                        template: "<mat-slide-toggle [formControl]=\"control\" [color]=\"options.color\">\n  <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n  <span *ngIf=\"options.icon\">&nbsp;</span>\n  <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n</mat-slide-toggle>\n",
                        styles: [""]
                    },] },
        ];
        /** @nocollapse */
        BootstartBooleanFormComponent.ctorParameters = function () { return []; };
        BootstartBooleanFormComponent.propDecorators = {
            control: [{ type: core.Input }],
            options: [{ type: core.Input }]
        };
        return BootstartBooleanFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartChipsTextFormComponent = (function () {
        function BootstartChipsTextFormComponent() {
        }
        /**
         * @return {?}
         */
        BootstartChipsTextFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._replaceUndefinedOption('maximum', Infinity);
                this._replaceUndefinedOption('separator', [keycodes.ENTER, keycodes.COMMA]);
                this._replaceUndefinedOption('chipColor', 'primary');
            };
        /**
         * @param {?} event
         * @return {?}
         */
        BootstartChipsTextFormComponent.prototype.addChip = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (this.formArray.controls.length < this.options.maximum) {
                    /** @type {?} */
                    var input = event.input;
                    /** @type {?} */
                    var value = event.value;
                    if ((value || '').trim()) {
                        this.formArray.push(new forms.FormControl(value.trim()));
                    }
                    // Reset the input value
                    if (input) {
                        input.value = '';
                    }
                }
            };
        /**
         * @param {?} chip
         * @return {?}
         */
        BootstartChipsTextFormComponent.prototype.removeChip = /**
         * @param {?} chip
         * @return {?}
         */
            function (chip) {
                /** @type {?} */
                var index = this.formArray.value.indexOf(chip);
                if (index >= 0) {
                    this.formArray.removeAt(index);
                }
            };
        /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
        BootstartChipsTextFormComponent.prototype._replaceUndefinedOption = /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
            function (field, defaultValue) {
                if (this.options[field] === undefined) {
                    this.options[field] = defaultValue;
                }
            };
        BootstartChipsTextFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-chips-text-form',
                        template: "<mat-label>\n  <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n  <span *ngIf=\"options.icon\">&nbsp;</span>\n  <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n</mat-label>\n\n<mat-chip-list #chipList>\n  <mat-chip *ngFor=\"let chip of formArray.value\"\n            selected\n            [color]=\"options.chipColor\"\n            [selectable]=\"false\"\n            [removable]=\"true\"\n            (removed)=\"removeChip(chip)\">\n    {{chip}}\n    <mat-icon matChipRemove>cancel</mat-icon>\n  </mat-chip>\n\n  <input matInput type=\"text\"\n         *ngIf=\"formArray.controls.length < options.maximum\"\n         placeholder=\"{{options.placeholder|translate}}\"\n         [matChipInputFor]=\"chipList\"\n         [matChipInputSeparatorKeyCodes]=\"options.separator\"\n         (matChipInputTokenEnd)=\"addChip($event)\">\n\n</mat-chip-list>\n",
                        styles: [""]
                    },] },
        ];
        /** @nocollapse */
        BootstartChipsTextFormComponent.ctorParameters = function () { return []; };
        BootstartChipsTextFormComponent.propDecorators = {
            options: [{ type: core.Input }],
            formArray: [{ type: core.Input }]
        };
        return BootstartChipsTextFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartCurrencyFormComponent = (function () {
        function BootstartCurrencyFormComponent() {
        }
        /**
         * @return {?}
         */
        BootstartCurrencyFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._replaceUndefinedOption('width', 100);
                this._replaceUndefinedOption('align', 'left');
                this._replaceUndefinedOption('allowNegative', true);
                this._replaceUndefinedOption('decimal', '.');
                this._replaceUndefinedOption('precision', 2);
                this._replaceUndefinedOption('prefix', '$ ');
                this._replaceUndefinedOption('suffix', '');
                this._replaceUndefinedOption('thousands', ',');
                this._replaceUndefinedOption('nullable', true);
            };
        /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
        BootstartCurrencyFormComponent.prototype._replaceUndefinedOption = /**
         * @param {?} field
         * @param {?} defaultValue
         * @return {?}
         */
            function (field, defaultValue) {
                if (this.options[field] === undefined) {
                    this.options[field] = defaultValue;
                }
            };
        BootstartCurrencyFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-currency-form',
                        template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{options.label|translate}}</span>\n  </mat-label>\n\n  <form [formGroup]=\"group\">\n    <input currencyMask matInput\n           formControlName=\"{{controlName}}\"\n           [required]=\"options.required\"\n           [options]=\"{\n           align: options.align,\n           allowNegative: options.allowNegative,\n           decimal: options.decimal,\n           precision: options.precision,\n           prefix: options.prefix,\n           suffix: options.suffix,\n           thousands: options.thousands,\n           nullable: options.nullable\n           }\">\n  </form>\n</mat-form-field>\n",
                        styles: [""]
                    },] },
        ];
        /** @nocollapse */
        BootstartCurrencyFormComponent.ctorParameters = function () { return []; };
        BootstartCurrencyFormComponent.propDecorators = {
            group: [{ type: core.Input }],
            controlName: [{ type: core.Input }],
            options: [{ type: core.Input }]
        };
        return BootstartCurrencyFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartFormModule = (function () {
        function BootstartFormModule() {
        }
        BootstartFormModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                            forms.FormsModule,
                            forms.ReactiveFormsModule,
                            material.MatInputModule,
                            material.MatFormFieldModule,
                            material.MatIconModule,
                            material.MatChipsModule,
                            material.MatSelectModule,
                            material.MatOptionModule,
                            material.MatSlideToggleModule,
                            ngxCurrency.NgxCurrencyModule,
                            core$1.TranslateModule
                        ],
                        declarations: [
                            BootstartTextFormComponent,
                            BootstartParagraphFormComponent,
                            BootstartChipsFormComponent,
                            BootstartSelectFormComponent,
                            BootstartNumberFormComponent,
                            BootstartBooleanFormComponent,
                            BootstartChipsTextFormComponent,
                            BootstartCurrencyFormComponent
                        ],
                        exports: [
                            BootstartTextFormComponent,
                            BootstartParagraphFormComponent,
                            BootstartChipsFormComponent,
                            BootstartSelectFormComponent,
                            BootstartNumberFormComponent,
                            BootstartBooleanFormComponent,
                            BootstartChipsTextFormComponent,
                            BootstartCurrencyFormComponent
                        ]
                    },] },
        ];
        return BootstartFormModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    /**
     * @param {?} value
     * @return {?}
     */
    function SafeSetFormArrayValue(value) {
        /** @type {?} */
        var builder = new forms.FormBuilder();
        if (typeof value === 'object') {
            return builder.array(value);
        }
        else {
            return builder.array([]);
        }
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.BootstartFormModule = BootstartFormModule;
    exports.SafeSetFormArrayValue = SafeSetFormArrayValue;
    exports.ɵf = BootstartBooleanFormComponent;
    exports.ɵc = BootstartChipsFormComponent;
    exports.ɵg = BootstartChipsTextFormComponent;
    exports.ɵh = BootstartCurrencyFormComponent;
    exports.ɵe = BootstartNumberFormComponent;
    exports.ɵb = BootstartParagraphFormComponent;
    exports.ɵd = BootstartSelectFormComponent;
    exports.ɵa = BootstartTextFormComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWZvcm0udW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvY29tcG9uZW50cy9ib290c3RhcnQtdGV4dC1mb3JtL2Jvb3RzdGFydC10ZXh0LWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvY29tcG9uZW50cy9ib290c3RhcnQtcGFyYWdyYXBoLWZvcm0vYm9vdHN0YXJ0LXBhcmFncmFwaC1mb3JtLmNvbXBvbmVudC50cyIsbnVsbCwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvY29tcG9uZW50cy9ib290c3RhcnQtY2hpcHMtZm9ybS9ib290c3RhcnQtY2hpcHMtZm9ybS5jb21wb25lbnQudHMiLCJuZzovL2Jvb3RzdGFydC1mb3JtL2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS9ib290c3RhcnQtc2VsZWN0LWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvY29tcG9uZW50cy9ib290c3RhcnQtbnVtYmVyLWZvcm0vYm9vdHN0YXJ0LW51bWJlci1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS9ib290c3RhcnQtYm9vbGVhbi1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS9ib290c3RhcnQtY2hpcHMtdGV4dC1mb3JtLmNvbXBvbmVudC50cyIsIm5nOi8vYm9vdHN0YXJ0LWZvcm0vbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0vYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvYm9vdHN0YXJ0LWZvcm0ubW9kdWxlLnRzIiwibmc6Ly9ib290c3RhcnQtZm9ybS9saWIvZnVuY3Rpb25zL3NhZmUtc2V0LWZvcm0tYXJyYXktdmFsdWUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnRUZXh0Rm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC10ZXh0LWZvcm0tb3B0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LXRleHQtZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWZvcm0tZmllbGQgW25nU3R5bGVdPVwieyd3aWR0aCcgOiBvcHRpb25zLndpZHRoKyclJ31cIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxpbnB1dCBtYXRJbnB1dCB0eXBlPVwidGV4dFwiIFtmb3JtQ29udHJvbF09XCJjb250cm9sXCIgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIj5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2AuZnVsbC13aWR0aHt3aWR0aDoxMDAlfWBdXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0VGV4dEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRUZXh0Rm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Qm9vdHN0YXJ0UGFyYWdyYXBoRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS1vcHRpb25zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtcGFyYWdyYXBoLWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8dGV4dGFyZWEgbWF0SW5wdXQgdHlwZT1cInBhcmFncmFwaFwiXG4gICAgICAgICAgICBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiXG4gICAgICAgICAgICBbcmVxdWlyZWRdPVwib3B0aW9ucy5yZXF1aXJlZFwiXG4gICAgICAgICAgICByb3dzPVwie3tvcHRpb25zLnJvd3N9fVwiPjwvdGV4dGFyZWE+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgLmZ1bGwtd2lkdGh7d2lkdGg6MTAwJX1gXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydFBhcmFncmFwaEZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRQYXJhZ3JhcGhGb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woJycpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignd2lkdGgnLCAxMDApO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3Jvd3MnLCA2KTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsIi8qISAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG5Db3B5cmlnaHQgKGMpIE1pY3Jvc29mdCBDb3Jwb3JhdGlvbi4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlXHJcbnRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlXHJcbkxpY2Vuc2UgYXQgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5USElTIENPREUgSVMgUFJPVklERUQgT04gQU4gKkFTIElTKiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXHJcbktJTkQsIEVJVEhFUiBFWFBSRVNTIE9SIElNUExJRUQsIElOQ0xVRElORyBXSVRIT1VUIExJTUlUQVRJT04gQU5ZIElNUExJRURcclxuV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIFRJVExFLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSxcclxuTUVSQ0hBTlRBQkxJVFkgT1IgTk9OLUlORlJJTkdFTUVOVC5cclxuXHJcblNlZSB0aGUgQXBhY2hlIFZlcnNpb24gMi4wIExpY2Vuc2UgZm9yIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9uc1xyXG5hbmQgbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqICovXHJcbi8qIGdsb2JhbCBSZWZsZWN0LCBQcm9taXNlICovXHJcblxyXG52YXIgZXh0ZW5kU3RhdGljcyA9IGZ1bmN0aW9uKGQsIGIpIHtcclxuICAgIGV4dGVuZFN0YXRpY3MgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHxcclxuICAgICAgICAoeyBfX3Byb3RvX186IFtdIH0gaW5zdGFuY2VvZiBBcnJheSAmJiBmdW5jdGlvbiAoZCwgYikgeyBkLl9fcHJvdG9fXyA9IGI7IH0pIHx8XHJcbiAgICAgICAgZnVuY3Rpb24gKGQsIGIpIHsgZm9yICh2YXIgcCBpbiBiKSBpZiAoYi5oYXNPd25Qcm9wZXJ0eShwKSkgZFtwXSA9IGJbcF07IH07XHJcbiAgICByZXR1cm4gZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4dGVuZHMoZCwgYikge1xyXG4gICAgZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxuICAgIGZ1bmN0aW9uIF9fKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gZDsgfVxyXG4gICAgZC5wcm90b3R5cGUgPSBiID09PSBudWxsID8gT2JqZWN0LmNyZWF0ZShiKSA6IChfXy5wcm90b3R5cGUgPSBiLnByb3RvdHlwZSwgbmV3IF9fKCkpO1xyXG59XHJcblxyXG5leHBvcnQgdmFyIF9fYXNzaWduID0gZnVuY3Rpb24oKSB7XHJcbiAgICBfX2Fzc2lnbiA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gX19hc3NpZ24odCkge1xyXG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xyXG4gICAgICAgICAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkpIHRbcF0gPSBzW3BdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdDtcclxuICAgIH1cclxuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19yZXN0KHMsIGUpIHtcclxuICAgIHZhciB0ID0ge307XHJcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcclxuICAgICAgICB0W3BdID0gc1twXTtcclxuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcclxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMClcclxuICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XHJcbiAgICByZXR1cm4gdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3BhcmFtKHBhcmFtSW5kZXgsIGRlY29yYXRvcikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIGtleSkgeyBkZWNvcmF0b3IodGFyZ2V0LCBrZXksIHBhcmFtSW5kZXgpOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX21ldGFkYXRhKG1ldGFkYXRhS2V5LCBtZXRhZGF0YVZhbHVlKSB7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QubWV0YWRhdGEgPT09IFwiZnVuY3Rpb25cIikgcmV0dXJuIFJlZmxlY3QubWV0YWRhdGEobWV0YWRhdGFLZXksIG1ldGFkYXRhVmFsdWUpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hd2FpdGVyKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xyXG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxyXG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxyXG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZShyZXN1bHQudmFsdWUpOyB9KS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XHJcbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2dlbmVyYXRvcih0aGlzQXJnLCBib2R5KSB7XHJcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xyXG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcclxuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XHJcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XHJcbiAgICAgICAgICAgIGlmIChmID0gMSwgeSAmJiAodCA9IG9wWzBdICYgMiA/IHlbXCJyZXR1cm5cIl0gOiBvcFswXSA/IHlbXCJ0aHJvd1wiXSB8fCAoKHQgPSB5W1wicmV0dXJuXCJdKSAmJiB0LmNhbGwoeSksIDApIDogeS5uZXh0KSAmJiAhKHQgPSB0LmNhbGwoeSwgb3BbMV0pKS5kb25lKSByZXR1cm4gdDtcclxuICAgICAgICAgICAgaWYgKHkgPSAwLCB0KSBvcCA9IFtvcFswXSAmIDIsIHQudmFsdWVdO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDA6IGNhc2UgMTogdCA9IG9wOyBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgNDogXy5sYWJlbCsrOyByZXR1cm4geyB2YWx1ZTogb3BbMV0sIGRvbmU6IGZhbHNlIH07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDc6IG9wID0gXy5vcHMucG9wKCk7IF8udHJ5cy5wb3AoKTsgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gMyAmJiAoIXQgfHwgKG9wWzFdID4gdFswXSAmJiBvcFsxXSA8IHRbM10pKSkgeyBfLmxhYmVsID0gb3BbMV07IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSA2ICYmIF8ubGFiZWwgPCB0WzFdKSB7IF8ubGFiZWwgPSB0WzFdOyB0ID0gb3A7IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0WzJdKSBfLm9wcy5wb3AoKTtcclxuICAgICAgICAgICAgICAgICAgICBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG9wID0gYm9keS5jYWxsKHRoaXNBcmcsIF8pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgb3AgPSBbNiwgZV07IHkgPSAwOyB9IGZpbmFsbHkgeyBmID0gdCA9IDA7IH1cclxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZXhwb3J0U3RhcihtLCBleHBvcnRzKSB7XHJcbiAgICBmb3IgKHZhciBwIGluIG0pIGlmICghZXhwb3J0cy5oYXNPd25Qcm9wZXJ0eShwKSkgZXhwb3J0c1twXSA9IG1bcF07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3ZhbHVlcyhvKSB7XHJcbiAgICB2YXIgbSA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvW1N5bWJvbC5pdGVyYXRvcl0sIGkgPSAwO1xyXG4gICAgaWYgKG0pIHJldHVybiBtLmNhbGwobyk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIG5leHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgaWYgKG8gJiYgaSA+PSBvLmxlbmd0aCkgbyA9IHZvaWQgMDtcclxuICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6IG8gJiYgb1tpKytdLCBkb25lOiAhbyB9O1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3JlYWQobywgbikge1xyXG4gICAgdmFyIG0gPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb1tTeW1ib2wuaXRlcmF0b3JdO1xyXG4gICAgaWYgKCFtKSByZXR1cm4gbztcclxuICAgIHZhciBpID0gbS5jYWxsKG8pLCByLCBhciA9IFtdLCBlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgICB3aGlsZSAoKG4gPT09IHZvaWQgMCB8fCBuLS0gPiAwKSAmJiAhKHIgPSBpLm5leHQoKSkuZG9uZSkgYXIucHVzaChyLnZhbHVlKTtcclxuICAgIH1cclxuICAgIGNhdGNoIChlcnJvcikgeyBlID0geyBlcnJvcjogZXJyb3IgfTsgfVxyXG4gICAgZmluYWxseSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKHIgJiYgIXIuZG9uZSAmJiAobSA9IGlbXCJyZXR1cm5cIl0pKSBtLmNhbGwoaSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZpbmFsbHkgeyBpZiAoZSkgdGhyb3cgZS5lcnJvcjsgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGFyO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19zcHJlYWQoKSB7XHJcbiAgICBmb3IgKHZhciBhciA9IFtdLCBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKylcclxuICAgICAgICBhciA9IGFyLmNvbmNhdChfX3JlYWQoYXJndW1lbnRzW2ldKSk7XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2F3YWl0KHYpIHtcclxuICAgIHJldHVybiB0aGlzIGluc3RhbmNlb2YgX19hd2FpdCA/ICh0aGlzLnYgPSB2LCB0aGlzKSA6IG5ldyBfX2F3YWl0KHYpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY0dlbmVyYXRvcih0aGlzQXJnLCBfYXJndW1lbnRzLCBnZW5lcmF0b3IpIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgZyA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSwgaSwgcSA9IFtdO1xyXG4gICAgcmV0dXJuIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiKSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IGlmIChnW25dKSBpW25dID0gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChhLCBiKSB7IHEucHVzaChbbiwgdiwgYSwgYl0pID4gMSB8fCByZXN1bWUobiwgdik7IH0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiByZXN1bWUobiwgdikgeyB0cnkgeyBzdGVwKGdbbl0odikpOyB9IGNhdGNoIChlKSB7IHNldHRsZShxWzBdWzNdLCBlKTsgfSB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKHIpIHsgci52YWx1ZSBpbnN0YW5jZW9mIF9fYXdhaXQgPyBQcm9taXNlLnJlc29sdmUoci52YWx1ZS52KS50aGVuKGZ1bGZpbGwsIHJlamVjdCkgOiBzZXR0bGUocVswXVsyXSwgcik7IH1cclxuICAgIGZ1bmN0aW9uIGZ1bGZpbGwodmFsdWUpIHsgcmVzdW1lKFwibmV4dFwiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHJlamVjdCh2YWx1ZSkgeyByZXN1bWUoXCJ0aHJvd1wiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShmLCB2KSB7IGlmIChmKHYpLCBxLnNoaWZ0KCksIHEubGVuZ3RoKSByZXN1bWUocVswXVswXSwgcVswXVsxXSk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNEZWxlZ2F0b3Iobykge1xyXG4gICAgdmFyIGksIHA7XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIsIGZ1bmN0aW9uIChlKSB7IHRocm93IGU7IH0pLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuLCBmKSB7IGlbbl0gPSBvW25dID8gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIChwID0gIXApID8geyB2YWx1ZTogX19hd2FpdChvW25dKHYpKSwgZG9uZTogbiA9PT0gXCJyZXR1cm5cIiB9IDogZiA/IGYodikgOiB2OyB9IDogZjsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY1ZhbHVlcyhvKSB7XHJcbiAgICBpZiAoIVN5bWJvbC5hc3luY0l0ZXJhdG9yKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3ltYm9sLmFzeW5jSXRlcmF0b3IgaXMgbm90IGRlZmluZWQuXCIpO1xyXG4gICAgdmFyIG0gPSBvW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSwgaTtcclxuICAgIHJldHVybiBtID8gbS5jYWxsKG8pIDogKG8gPSB0eXBlb2YgX192YWx1ZXMgPT09IFwiZnVuY3Rpb25cIiA/IF9fdmFsdWVzKG8pIDogb1tTeW1ib2wuaXRlcmF0b3JdKCksIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiKSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpKTtcclxuICAgIGZ1bmN0aW9uIHZlcmIobikgeyBpW25dID0gb1tuXSAmJiBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkgeyB2ID0gb1tuXSh2KSwgc2V0dGxlKHJlc29sdmUsIHJlamVjdCwgdi5kb25lLCB2LnZhbHVlKTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShyZXNvbHZlLCByZWplY3QsIGQsIHYpIHsgUHJvbWlzZS5yZXNvbHZlKHYpLnRoZW4oZnVuY3Rpb24odikgeyByZXNvbHZlKHsgdmFsdWU6IHYsIGRvbmU6IGQgfSk7IH0sIHJlamVjdCk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fbWFrZVRlbXBsYXRlT2JqZWN0KGNvb2tlZCwgcmF3KSB7XHJcbiAgICBpZiAoT2JqZWN0LmRlZmluZVByb3BlcnR5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjb29rZWQsIFwicmF3XCIsIHsgdmFsdWU6IHJhdyB9KTsgfSBlbHNlIHsgY29va2VkLnJhdyA9IHJhdzsgfVxyXG4gICAgcmV0dXJuIGNvb2tlZDtcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2ltcG9ydFN0YXIobW9kKSB7XHJcbiAgICBpZiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSByZXR1cm4gbW9kO1xyXG4gICAgdmFyIHJlc3VsdCA9IHt9O1xyXG4gICAgaWYgKG1vZCAhPSBudWxsKSBmb3IgKHZhciBrIGluIG1vZCkgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcclxuICAgIHJlc3VsdC5kZWZhdWx0ID0gbW9kO1xyXG4gICAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9faW1wb3J0RGVmYXVsdChtb2QpIHtcclxuICAgIHJldHVybiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSA/IG1vZCA6IHsgZGVmYXVsdDogbW9kIH07XHJcbn1cclxuIiwiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtTZWxlY3Rpb25Nb2RlbH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zJztcbmltcG9ydCB7Qm9vdHN0YXJ0Q2hpcHNGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LWNoaXBzLWZvcm0tb3B0aW9ucyc7XG5pbXBvcnQge0Zvcm1BcnJheSwgRm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtY2hpcHMtZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWNoaXAtbGlzdD5cbiAgPG1hdC1jaGlwICpuZ0Zvcj1cImxldCBjaG9pY2Ugb2Ygb3B0aW9ucy5jaG9pY2VzXCJcbiAgICAgICAgICAgIChjbGljayk9XCJzZWxlY3Rpb25DaGFuZ2VzKGNob2ljZSlcIiBzZWxlY3RlZFxuICAgICAgICAgICAgW2Rpc2FibGVkXT1cInNlbGVjdGlvbi5zZWxlY3RlZC5sZW5ndGggPj0gb3B0aW9ucy5tYXhpbXVtU2VsZWN0YWJsZSAmJiAhc2VsZWN0aW9uLmlzU2VsZWN0ZWQoY2hvaWNlKVwiXG4gICAgICAgICAgICBbbmdDbGFzc109XCJpc0NsaWNrYWJsZShjaG9pY2UpID8gJ2Jvb3RzdGFydC1jbGlja2FibGUtY2hpcCcgOiAnYm9vdHN0YXJ0LW5vbi1jbGlja2FibGUtY2hpcCdcIlxuICAgICAgICAgICAgW2NvbG9yXT1cInNlbGVjdGlvbi5pc1NlbGVjdGVkKGNob2ljZSkgPyAncHJpbWFyeScgOiAnbm9uZSdcIj5cblxuICAgIDxtYXQtY2hpcC1hdmF0YXIgKm5nSWY9XCJzZWxlY3Rpb24uaXNTZWxlY3RlZChjaG9pY2UpXCI+XG4gICAgICA8c3BhbiBjbGFzcz1cImZhcyBmYS1jaGVja1wiPjwvc3Bhbj5cbiAgICA8L21hdC1jaGlwLWF2YXRhcj5cblxuICAgIHt7b3B0aW9ucy5jaG9pY2VzVHJhbnNsYXRlUHJlZml4ICsgY2hvaWNlfHRyYW5zbGF0ZX19XG4gIDwvbWF0LWNoaXA+XG48L21hdC1jaGlwLWxpc3Q+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2AuYm9vdHN0YXJ0LWNsaWNrYWJsZS1jaGlwe2N1cnNvcjpwb2ludGVyfS5ib290c3RhcnQtbm9uLWNsaWNrYWJsZS1jaGlwe2N1cnNvcjpub3QtYWxsb3dlZH1gXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydENoaXBzRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgZm9ybUFycmF5OiBGb3JtQXJyYXk7XG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydENoaXBzRm9ybU9wdGlvbnM7XG5cbiAgc2VsZWN0aW9uID0gbmV3IFNlbGVjdGlvbk1vZGVsPHN0cmluZz4odHJ1ZSk7XG5cblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2Nob2ljZXNUcmFuc2xhdGVQcmVmaXgnLCAnJyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignbWF4aW11bVNlbGVjdGFibGUnLCBJbmZpbml0eSk7XG4gICAgdGhpcy5zZWxlY3Rpb24uc2VsZWN0KC4uLnRoaXMuZm9ybUFycmF5LnZhbHVlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBXaGF0IGhhcHBlbnMgd2hlbiBzZWxlY3Rpb24gY2hhbmdlc1xuICAgKiBAcGFyYW0gdmFsdWUgQ2hpcCdzIHZhbHVlIHRoYXQgaGFzIGNoYW5nZWQuXG4gICAqL1xuICBwdWJsaWMgc2VsZWN0aW9uQ2hhbmdlcyh2YWx1ZTogc3RyaW5nKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLnNlbGVjdGVkLmxlbmd0aCA+PSB0aGlzLm9wdGlvbnMubWF4aW11bVNlbGVjdGFibGUgJiYgIXRoaXMuc2VsZWN0aW9uLmlzU2VsZWN0ZWQodmFsdWUpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuc2VsZWN0aW9uLnRvZ2dsZSh2YWx1ZSk7XG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLmlzU2VsZWN0ZWQodmFsdWUpKSB7XG4gICAgICB0aGlzLmZvcm1BcnJheS5wdXNoKG5ldyBGb3JtQ29udHJvbCh2YWx1ZSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmZvcm1BcnJheS5yZW1vdmVBdCh0aGlzLmZvcm1BcnJheS5jb250cm9scy5maW5kSW5kZXgoeCA9PiB4LnZhbHVlID09PSB2YWx1ZSkpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBpc0NsaWNrYWJsZSh2YWx1ZTogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgaWYgKHRoaXMuc2VsZWN0aW9uLnNlbGVjdGVkLmxlbmd0aCA+PSB0aGlzLm9wdGlvbnMubWF4aW11bVNlbGVjdGFibGUpIHtcbiAgICAgIHJldHVybiB0aGlzLnNlbGVjdGlvbi5pc1NlbGVjdGVkKHZhbHVlKTtcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtCb290c3RhcnRTZWxlY3RGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LXNlbGVjdC1mb3JtLW9wdGlvbnMnO1xuaW1wb3J0IHtNYXRTZWxlY3Rpb25MaXN0Q2hhbmdlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LXNlbGVjdC1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPG1hdC1zZWxlY3QgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIiBbZm9ybUNvbnRyb2xdPVwiY29udHJvbFwiIChzZWxlY3Rpb25DaGFuZ2UpPVwic2VsZWN0aW9uQ2hhbmdlcygkZXZlbnQpXCI+XG4gICAgPG1hdC1vcHRpb24gKm5nRm9yPVwibGV0IGNob2ljZSBvZiBvcHRpb25zLmNob2ljZXNcIiB2YWx1ZT1cInt7Y2hvaWNlfX1cIj5cbiAgICAgIHt7b3B0aW9ucy5jaG9pY2VzVHJhbnNsYXRlUHJlZml4ICsgY2hvaWNlfHRyYW5zbGF0ZX19XG4gICAgPC9tYXQtb3B0aW9uPlxuICA8L21hdC1zZWxlY3Q+XG48L21hdC1mb3JtLWZpZWxkPlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRTZWxlY3RGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAvKiogQXNzb2NpYXRlZCBmb3JtIGNvbnRyb2wgKi9cbiAgQElucHV0KCkgY29udHJvbDogRm9ybUNvbnRyb2w7XG5cbiAgLyoqIFNlbGVjdCBvcHRpb25zICovXG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydFNlbGVjdEZvcm1PcHRpb25zO1xuXG4gIC8qKiBPbiBDaGFuZ2UgZW1pdHRlciAqL1xuICBAT3V0cHV0KCkgc2VsZWN0aW9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8TWF0U2VsZWN0aW9uTGlzdENoYW5nZT47XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jb250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgICB0aGlzLnNlbGVjdGlvbkNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8TWF0U2VsZWN0aW9uTGlzdENoYW5nZT4oKTtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2Nob2ljZXNUcmFuc2xhdGVQcmVmaXgnLCAnJyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignd2lkdGgnLCAxMDApO1xuICB9XG5cbiAgcHVibGljIHNlbGVjdGlvbkNoYW5nZXMoZXZlbnQ6IE1hdFNlbGVjdGlvbkxpc3RDaGFuZ2UpOiB2b2lkIHtcbiAgICB0aGlzLnNlbGVjdGlvbkNoYW5nZS5lbWl0KGV2ZW50KTtcbiAgfVxuXG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybUNvbnRyb2x9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7Qm9vdHN0YXJ0TnVtYmVyRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9vcHRpb25zL2Jvb3RzdGFydC1udW1iZXItZm9ybS1vcHRpb25zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtbnVtYmVyLWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8aW5wdXQgbWF0SW5wdXQgdHlwZT1cIm51bWJlclwiIFtmb3JtQ29udHJvbF09XCJjb250cm9sXCIgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIj5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2BgXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydE51bWJlckZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnROdW1iZXJGb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woJycpO1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignd2lkdGgnLCAnMTAwJyk7XG4gIH1cblxuXG4gIHByaXZhdGUgX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oZmllbGQ6IGFueSwgZGVmYXVsdFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zW2ZpZWxkXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnNbZmllbGRdID0gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfVxuXG59XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Zvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0Jvb3RzdGFydEJvb2xlYW5Gb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS1vcHRpb25zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtYm9vbGVhbi1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtc2xpZGUtdG9nZ2xlIFtmb3JtQ29udHJvbF09XCJjb250cm9sXCIgW2NvbG9yXT1cIm9wdGlvbnMuY29sb3JcIj5cbiAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57e29wdGlvbnMubGFiZWx8dHJhbnNsYXRlfX08L3NwYW4+XG48L21hdC1zbGlkZS10b2dnbGU+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2BgXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEJvb2xlYW5Gb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBjb250cm9sOiBGb3JtQ29udHJvbDtcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0Qm9vbGVhbkZvcm1PcHRpb25zO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuY29udHJvbCA9IG5ldyBGb3JtQ29udHJvbChmYWxzZSk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdjb2xvcicsICdwcmltYXJ5Jyk7XG4gIH1cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIiwiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRDaGlwc1RleHRGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzL29wdGlvbnMvYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS1vcHRpb25zJztcbmltcG9ydCB7Rm9ybUFycmF5LCBGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtDT01NQSwgRU5URVJ9IGZyb20gJ0Bhbmd1bGFyL2Nkay9rZXljb2Rlcyc7XG5pbXBvcnQge01hdENoaXBJbnB1dEV2ZW50fSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWxhYmVsPlxuICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7b3B0aW9ucy5sYWJlbHx0cmFuc2xhdGV9fTwvc3Bhbj5cbjwvbWF0LWxhYmVsPlxuXG48bWF0LWNoaXAtbGlzdCAjY2hpcExpc3Q+XG4gIDxtYXQtY2hpcCAqbmdGb3I9XCJsZXQgY2hpcCBvZiBmb3JtQXJyYXkudmFsdWVcIlxuICAgICAgICAgICAgc2VsZWN0ZWRcbiAgICAgICAgICAgIFtjb2xvcl09XCJvcHRpb25zLmNoaXBDb2xvclwiXG4gICAgICAgICAgICBbc2VsZWN0YWJsZV09XCJmYWxzZVwiXG4gICAgICAgICAgICBbcmVtb3ZhYmxlXT1cInRydWVcIlxuICAgICAgICAgICAgKHJlbW92ZWQpPVwicmVtb3ZlQ2hpcChjaGlwKVwiPlxuICAgIHt7Y2hpcH19XG4gICAgPG1hdC1pY29uIG1hdENoaXBSZW1vdmU+Y2FuY2VsPC9tYXQtaWNvbj5cbiAgPC9tYXQtY2hpcD5cblxuICA8aW5wdXQgbWF0SW5wdXQgdHlwZT1cInRleHRcIlxuICAgICAgICAgKm5nSWY9XCJmb3JtQXJyYXkuY29udHJvbHMubGVuZ3RoIDwgb3B0aW9ucy5tYXhpbXVtXCJcbiAgICAgICAgIHBsYWNlaG9sZGVyPVwie3tvcHRpb25zLnBsYWNlaG9sZGVyfHRyYW5zbGF0ZX19XCJcbiAgICAgICAgIFttYXRDaGlwSW5wdXRGb3JdPVwiY2hpcExpc3RcIlxuICAgICAgICAgW21hdENoaXBJbnB1dFNlcGFyYXRvcktleUNvZGVzXT1cIm9wdGlvbnMuc2VwYXJhdG9yXCJcbiAgICAgICAgIChtYXRDaGlwSW5wdXRUb2tlbkVuZCk9XCJhZGRDaGlwKCRldmVudClcIj5cblxuPC9tYXQtY2hpcC1saXN0PlxuYCxcbiAgICAgICAgICAgICBzdHlsZXM6IFtgYF1cbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRDaGlwc1RleHRGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBvcHRpb25zOiBCb290c3RhcnRDaGlwc1RleHRGb3JtT3B0aW9ucztcbiAgQElucHV0KCkgZm9ybUFycmF5OiBGb3JtQXJyYXk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdtYXhpbXVtJywgSW5maW5pdHkpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3NlcGFyYXRvcicsIFtFTlRFUiwgQ09NTUFdKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdjaGlwQ29sb3InLCAncHJpbWFyeScpO1xuICB9XG5cblxuICBwdWJsaWMgYWRkQ2hpcChldmVudDogTWF0Q2hpcElucHV0RXZlbnQpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5mb3JtQXJyYXkuY29udHJvbHMubGVuZ3RoIDwgdGhpcy5vcHRpb25zLm1heGltdW0pIHtcbiAgICAgIGNvbnN0IGlucHV0ID0gZXZlbnQuaW5wdXQ7XG4gICAgICBjb25zdCB2YWx1ZSA9IGV2ZW50LnZhbHVlO1xuXG4gICAgICBpZiAoKHZhbHVlIHx8ICcnKS50cmltKCkpIHtcbiAgICAgICAgdGhpcy5mb3JtQXJyYXkucHVzaChuZXcgRm9ybUNvbnRyb2wodmFsdWUudHJpbSgpKSk7XG4gICAgICB9XG4gICAgICAvLyBSZXNldCB0aGUgaW5wdXQgdmFsdWVcbiAgICAgIGlmIChpbnB1dCkge1xuICAgICAgICBpbnB1dC52YWx1ZSA9ICcnO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyByZW1vdmVDaGlwKGNoaXA6IHN0cmluZyk6IHZvaWQge1xuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5mb3JtQXJyYXkudmFsdWUuaW5kZXhPZihjaGlwKTtcblxuICAgIGlmIChpbmRleCA+PSAwKSB7XG4gICAgICB0aGlzLmZvcm1BcnJheS5yZW1vdmVBdChpbmRleCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBfcmVwbGFjZVVuZGVmaW5lZE9wdGlvbihmaWVsZDogYW55LCBkZWZhdWx0VmFsdWU6IGFueSk6IHZvaWQge1xuICAgIGlmICh0aGlzLm9wdGlvbnNbZmllbGRdID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9uc1tmaWVsZF0gPSBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiIsImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Qm9vdHN0YXJ0Q3VycmVuY3lGb3JtT3B0aW9uc30gZnJvbSAnLi4vLi4vbW9kZWxzJztcbmltcG9ydCB7Rm9ybUdyb3VwfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICAgICAgICAgICAgIHNlbGVjdG9yICAgOiAnYm9vdHN0YXJ0LWN1cnJlbmN5LWZvcm0nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ1N0eWxlXT1cInsnd2lkdGgnIDogb3B0aW9ucy53aWR0aCsnJSd9XCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+e3tvcHRpb25zLmljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJvcHRpb25zLmxhYmVsXCI+e3tvcHRpb25zLmxhYmVsfHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8Zm9ybSBbZm9ybUdyb3VwXT1cImdyb3VwXCI+XG4gICAgPGlucHV0IGN1cnJlbmN5TWFzayBtYXRJbnB1dFxuICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJ7e2NvbnRyb2xOYW1lfX1cIlxuICAgICAgICAgICBbcmVxdWlyZWRdPVwib3B0aW9ucy5yZXF1aXJlZFwiXG4gICAgICAgICAgIFtvcHRpb25zXT1cIntcbiAgICAgICAgICAgYWxpZ246IG9wdGlvbnMuYWxpZ24sXG4gICAgICAgICAgIGFsbG93TmVnYXRpdmU6IG9wdGlvbnMuYWxsb3dOZWdhdGl2ZSxcbiAgICAgICAgICAgZGVjaW1hbDogb3B0aW9ucy5kZWNpbWFsLFxuICAgICAgICAgICBwcmVjaXNpb246IG9wdGlvbnMucHJlY2lzaW9uLFxuICAgICAgICAgICBwcmVmaXg6IG9wdGlvbnMucHJlZml4LFxuICAgICAgICAgICBzdWZmaXg6IG9wdGlvbnMuc3VmZml4LFxuICAgICAgICAgICB0aG91c2FuZHM6IG9wdGlvbnMudGhvdXNhbmRzLFxuICAgICAgICAgICBudWxsYWJsZTogb3B0aW9ucy5udWxsYWJsZVxuICAgICAgICAgICB9XCI+XG4gIDwvZm9ybT5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gLFxuICAgICAgICAgICAgIHN0eWxlczogW2BgXVxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgZ3JvdXA6IEZvcm1Hcm91cDtcbiAgQElucHV0KCkgY29udHJvbE5hbWU6IHN0cmluZztcbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0Q3VycmVuY3lGb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3dpZHRoJywgMTAwKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCdhbGlnbicsICdsZWZ0Jyk7XG4gICAgdGhpcy5fcmVwbGFjZVVuZGVmaW5lZE9wdGlvbignYWxsb3dOZWdhdGl2ZScsIHRydWUpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ2RlY2ltYWwnLCAnLicpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3ByZWNpc2lvbicsIDIpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3ByZWZpeCcsICckICcpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ3N1ZmZpeCcsICcnKTtcbiAgICB0aGlzLl9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKCd0aG91c2FuZHMnLCAnLCcpO1xuICAgIHRoaXMuX3JlcGxhY2VVbmRlZmluZWRPcHRpb24oJ251bGxhYmxlJywgdHJ1ZSk7XG4gIH1cblxuICBwcml2YXRlIF9yZXBsYWNlVW5kZWZpbmVkT3B0aW9uKGZpZWxkOiBhbnksIGRlZmF1bHRWYWx1ZTogYW55KTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9uc1tmaWVsZF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5vcHRpb25zW2ZpZWxkXSA9IGRlZmF1bHRWYWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIiwiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydFRleHRGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXRleHQtZm9ybS9ib290c3RhcnQtdGV4dC1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7XG4gIE1hdENoaXBzTW9kdWxlLFxuICBNYXRGb3JtRmllbGRNb2R1bGUsXG4gIE1hdEljb25Nb2R1bGUsXG4gIE1hdElucHV0TW9kdWxlLFxuICBNYXRPcHRpb25Nb2R1bGUsXG4gIE1hdFNlbGVjdE1vZHVsZSxcbiAgTWF0U2xpZGVUb2dnbGVNb2R1bGVcbn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHtUcmFuc2xhdGVNb2R1bGV9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRQYXJhZ3JhcGhGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXBhcmFncmFwaC1mb3JtL2Jvb3RzdGFydC1wYXJhZ3JhcGgtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRDaGlwc0Zvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtY2hpcHMtZm9ybS9ib290c3RhcnQtY2hpcHMtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRTZWxlY3RGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LXNlbGVjdC1mb3JtL2Jvb3RzdGFydC1zZWxlY3QtZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnROdW1iZXJGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LW51bWJlci1mb3JtL2Jvb3RzdGFydC1udW1iZXItZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRCb29sZWFuRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1ib29sZWFuLWZvcm0vYm9vdHN0YXJ0LWJvb2xlYW4tZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtCb290c3RhcnRDaGlwc1RleHRGb3JtQ29tcG9uZW50fSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWNoaXBzLXRleHQtZm9ybS9ib290c3RhcnQtY2hpcHMtdGV4dC1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge0Jvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1jdXJyZW5jeS1mb3JtL2Jvb3RzdGFydC1jdXJyZW5jeS1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge05neEN1cnJlbmN5TW9kdWxlfSBmcm9tICduZ3gtY3VycmVuY3knO1xuXG5ATmdNb2R1bGUoe1xuICAgICAgICAgICAgaW1wb3J0cyAgICAgOiBbXG4gICAgICAgICAgICAgIENvbW1vbk1vZHVsZSxcbiAgICAgICAgICAgICAgRm9ybXNNb2R1bGUsXG4gICAgICAgICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdElucHV0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRGb3JtRmllbGRNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdEljb25Nb2R1bGUsXG4gICAgICAgICAgICAgIE1hdENoaXBzTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRTZWxlY3RNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdE9wdGlvbk1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0U2xpZGVUb2dnbGVNb2R1bGUsXG4gICAgICAgICAgICAgIE5neEN1cnJlbmN5TW9kdWxlLFxuICAgICAgICAgICAgICBUcmFuc2xhdGVNb2R1bGVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0VGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydFBhcmFncmFwaEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0U2VsZWN0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TnVtYmVyRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Qm9vbGVhbkZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydENoaXBzVGV4dEZvcm1Db21wb25lbnQsXG4gICAgICAgICAgICAgIEJvb3RzdGFydEN1cnJlbmN5Rm9ybUNvbXBvbmVudFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGV4cG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBCb290c3RhcnRUZXh0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0UGFyYWdyYXBoRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q2hpcHNGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRTZWxlY3RGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnROdW1iZXJGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBCb290c3RhcnRCb29sZWFuRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q2hpcHNUZXh0Rm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0Q3VycmVuY3lGb3JtQ29tcG9uZW50XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRGb3JtTW9kdWxlIHtcbn1cbiIsImltcG9ydCB7Rm9ybUFycmF5LCBGb3JtQnVpbGRlcn0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5leHBvcnQgZnVuY3Rpb24gU2FmZVNldEZvcm1BcnJheVZhbHVlKHZhbHVlOiBhbnkpOiBGb3JtQXJyYXkge1xuICBjb25zdCBidWlsZGVyID0gbmV3IEZvcm1CdWlsZGVyKCk7XG4gIGlmICh0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSB7XG4gICAgcmV0dXJuIGJ1aWxkZXIuYXJyYXkodmFsdWUpO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBidWlsZGVyLmFycmF5KFtdKTtcbiAgfVxufVxuIl0sIm5hbWVzIjpbIkZvcm1Db250cm9sIiwiQ29tcG9uZW50IiwiSW5wdXQiLCJTZWxlY3Rpb25Nb2RlbCIsIkV2ZW50RW1pdHRlciIsIk91dHB1dCIsIkVOVEVSIiwiQ09NTUEiLCJOZ01vZHVsZSIsIkNvbW1vbk1vZHVsZSIsIkZvcm1zTW9kdWxlIiwiUmVhY3RpdmVGb3Jtc01vZHVsZSIsIk1hdElucHV0TW9kdWxlIiwiTWF0Rm9ybUZpZWxkTW9kdWxlIiwiTWF0SWNvbk1vZHVsZSIsIk1hdENoaXBzTW9kdWxlIiwiTWF0U2VsZWN0TW9kdWxlIiwiTWF0T3B0aW9uTW9kdWxlIiwiTWF0U2xpZGVUb2dnbGVNb2R1bGUiLCJOZ3hDdXJyZW5jeU1vZHVsZSIsIlRyYW5zbGF0ZU1vZHVsZSIsIkZvcm1CdWlsZGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7UUF1QkU7WUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUlBLGlCQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDcEM7Ozs7UUFFRCw2Q0FBUTs7O1lBQVI7Z0JBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQzthQUM1Qzs7Ozs7O1FBR08sNERBQXVCOzs7OztzQkFBQyxLQUFVLEVBQUUsWUFBaUI7Z0JBQzNELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLEVBQUU7b0JBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO2lCQUNwQzs7O29CQS9CSkMsY0FBUyxTQUFDO3dCQUNFLFFBQVEsRUFBSyxxQkFBcUI7d0JBQ2xDLFFBQVEsRUFBRSx3WUFTdEI7d0JBQ1ksTUFBTSxFQUFFLENBQUMseUJBQXlCLENBQUM7cUJBQ3BDOzs7Ozs4QkFHVEMsVUFBSzs4QkFDTEEsVUFBSzs7eUNBckJSOzs7Ozs7O0FDQUE7UUEwQkU7WUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUlGLGlCQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDcEM7Ozs7UUFFRCxrREFBUTs7O1lBQVI7Z0JBQ0UsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN6Qzs7Ozs7O1FBR08saUVBQXVCOzs7OztzQkFBQyxLQUFVLEVBQUUsWUFBaUI7Z0JBQzNELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLEVBQUU7b0JBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO2lCQUNwQzs7O29CQW5DSkMsY0FBUyxTQUFDO3dCQUNFLFFBQVEsRUFBSywwQkFBMEI7d0JBQ3ZDLFFBQVEsRUFBRSw0ZEFZdEI7d0JBQ1ksTUFBTSxFQUFFLENBQUMseUJBQXlCLENBQUM7cUJBQ3BDOzs7Ozs4QkFHVEMsVUFBSzs4QkFDTEEsVUFBSzs7OENBeEJSOzs7SUNBQTs7Ozs7Ozs7Ozs7Ozs7QUFjQSxvQkF1R3VCLENBQUMsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxHQUFHLE9BQU8sTUFBTSxLQUFLLFVBQVUsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDakMsSUFBSTtZQUNBLE9BQU8sQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLElBQUk7Z0JBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDOUU7UUFDRCxPQUFPLEtBQUssRUFBRTtZQUFFLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQztTQUFFO2dCQUMvQjtZQUNKLElBQUk7Z0JBQ0EsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwRDtvQkFDTztnQkFBRSxJQUFJLENBQUM7b0JBQUUsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQUU7U0FDcEM7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7QUFFRDtRQUNJLEtBQUssSUFBSSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO1lBQzlDLEVBQUUsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3pDLE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7OztRQzFHQzs2QkFIWSxJQUFJQywwQkFBYyxDQUFTLElBQUksQ0FBQztTQUkzQzs7OztRQUVELDhDQUFROzs7WUFBUjtnQkFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsd0JBQXdCLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzNELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxtQkFBbUIsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDNUQsQ0FBQSxLQUFBLElBQUksQ0FBQyxTQUFTLEVBQUMsTUFBTSxvQkFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRTs7YUFDaEQ7Ozs7OztRQU1NLHNEQUFnQjs7Ozs7c0JBQUMsS0FBYTtnQkFDbkMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUN6RyxPQUFPO2lCQUNSO2dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM3QixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJSCxpQkFBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7aUJBQzdDO3FCQUFNO29CQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSyxHQUFBLENBQUMsQ0FBQyxDQUFDO2lCQUNwRjs7Ozs7O1FBR0ksaURBQVc7Ozs7c0JBQUMsS0FBYTtnQkFDOUIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRTtvQkFDcEUsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDekM7Z0JBQ0QsT0FBTyxJQUFJLENBQUM7Ozs7Ozs7UUFJTiw2REFBdUI7Ozs7O3NCQUFDLEtBQVUsRUFBRSxZQUFpQjtnQkFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtvQkFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7aUJBQ3BDOzs7b0JBL0RKQyxjQUFTLFNBQUM7d0JBQ0UsUUFBUSxFQUFLLHNCQUFzQjt3QkFDbkMsUUFBUSxFQUFFLG1wQkFjdEI7d0JBQ1ksTUFBTSxFQUFFLENBQUMsNEZBQTRGLENBQUM7cUJBQ3ZHOzs7OztnQ0FHVEMsVUFBSzs4QkFDTEEsVUFBSzs7MENBM0JSOzs7Ozs7O0FDQUE7UUFrQ0U7WUFDRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUlGLGlCQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJSSxpQkFBWSxFQUEwQixDQUFDO1NBQ25FOzs7O1FBRUQsK0NBQVE7OztZQUFSO2dCQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDM0QsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQzthQUM1Qzs7Ozs7UUFFTSx1REFBZ0I7Ozs7c0JBQUMsS0FBNkI7Z0JBQ25ELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDOzs7Ozs7O1FBSTNCLDhEQUF1Qjs7Ozs7c0JBQUMsS0FBVSxFQUFFLFlBQWlCO2dCQUMzRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxFQUFFO29CQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztpQkFDcEM7OztvQkEvQ0pILGNBQVMsU0FBQzt3QkFDRSxRQUFRLEVBQUssdUJBQXVCO3dCQUNwQyxRQUFRLEVBQUUsc2xCQWF0Qjt3QkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7cUJBQ2I7Ozs7OzhCQUlUQyxVQUFLOzhCQUdMQSxVQUFLO3NDQUdMRyxXQUFNOzsyQ0FoQ1Q7Ozs7Ozs7QUNBQTtRQXVCRTtZQUNFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSUwsaUJBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNwQzs7OztRQUVELCtDQUFROzs7WUFBUjtnQkFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQzlDOzs7Ozs7UUFHTyw4REFBdUI7Ozs7O3NCQUFDLEtBQVUsRUFBRSxZQUFpQjtnQkFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtvQkFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7aUJBQ3BDOzs7b0JBL0JKQyxjQUFTLFNBQUM7d0JBQ0UsUUFBUSxFQUFLLHVCQUF1Qjt3QkFDcEMsUUFBUSxFQUFFLDBZQVN0Qjt3QkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7cUJBQ2I7Ozs7OzhCQUdUQyxVQUFLOzhCQUNMQSxVQUFLOzsyQ0FyQlI7Ozs7Ozs7QUNBQTtRQW1CRTtZQUNFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSUYsaUJBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN2Qzs7OztRQUVELGdEQUFROzs7WUFBUjtnQkFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDO2FBQ2xEOzs7Ozs7UUFFTywrREFBdUI7Ozs7O3NCQUFDLEtBQVUsRUFBRSxZQUFpQjtnQkFDM0QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLFNBQVMsRUFBRTtvQkFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUM7aUJBQ3BDOzs7b0JBMUJKQyxjQUFTLFNBQUM7d0JBQ0UsUUFBUSxFQUFLLHdCQUF3Qjt3QkFDckMsUUFBUSxFQUFFLGlSQUt0Qjt3QkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7cUJBQ2I7Ozs7OzhCQUdUQyxVQUFLOzhCQUNMQSxVQUFLOzs0Q0FqQlI7Ozs7Ozs7QUNBQTtRQXlDRTtTQUNDOzs7O1FBRUQsa0RBQVE7OztZQUFSO2dCQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQ2xELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQ0ksY0FBSyxFQUFFQyxjQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO2FBQ3REOzs7OztRQUdNLGlEQUFPOzs7O3NCQUFDLEtBQXdCO2dCQUNyQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTs7b0JBQ3pELElBQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7O29CQUMxQixJQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO29CQUUxQixJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRTt3QkFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSVAsaUJBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO3FCQUNwRDs7b0JBRUQsSUFBSSxLQUFLLEVBQUU7d0JBQ1QsS0FBSyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7cUJBQ2xCO2lCQUNGOzs7Ozs7UUFHSSxvREFBVTs7OztzQkFBQyxJQUFZOztnQkFDNUIsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVqRCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2hDOzs7Ozs7O1FBR0ssaUVBQXVCOzs7OztzQkFBQyxLQUFVLEVBQUUsWUFBaUI7Z0JBQzNELElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxTQUFTLEVBQUU7b0JBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDO2lCQUNwQzs7O29CQXZFSkMsY0FBUyxTQUFDO3dCQUNFLFFBQVEsRUFBSywyQkFBMkI7d0JBQ3hDLFFBQVEsRUFBRSxrM0JBeUJ0Qjt3QkFDWSxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7cUJBQ2I7Ozs7OzhCQUdUQyxVQUFLO2dDQUNMQSxVQUFLOzs4Q0F2Q1I7Ozs7Ozs7QUNBQTtRQXNDRTtTQUNDOzs7O1FBRUQsaURBQVE7OztZQUFSO2dCQUNFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQzNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3BELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQzdDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQzdDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQy9DLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDaEQ7Ozs7OztRQUVPLGdFQUF1Qjs7Ozs7c0JBQUMsS0FBVSxFQUFFLFlBQWlCO2dCQUMzRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssU0FBUyxFQUFFO29CQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQztpQkFDcEM7OztvQkFwREpELGNBQVMsU0FBQzt3QkFDRSxRQUFRLEVBQUsseUJBQXlCO3dCQUN0QyxRQUFRLEVBQUUsNnpCQXVCdEI7d0JBQ1ksTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO3FCQUNiOzs7Ozs0QkFHVEMsVUFBSztrQ0FDTEEsVUFBSzs4QkFDTEEsVUFBSzs7NkNBcENSOzs7Ozs7O0FDQUE7Ozs7b0JBdUJDTSxhQUFRLFNBQUM7d0JBQ0UsT0FBTyxFQUFPOzRCQUNaQyxtQkFBWTs0QkFDWkMsaUJBQVc7NEJBQ1hDLHlCQUFtQjs0QkFDbkJDLHVCQUFjOzRCQUNkQywyQkFBa0I7NEJBQ2xCQyxzQkFBYTs0QkFDYkMsdUJBQWM7NEJBQ2RDLHdCQUFlOzRCQUNmQyx3QkFBZTs0QkFDZkMsNkJBQW9COzRCQUNwQkMsNkJBQWlCOzRCQUNqQkMsc0JBQWU7eUJBQ2hCO3dCQUNELFlBQVksRUFBRTs0QkFDWiwwQkFBMEI7NEJBQzFCLCtCQUErQjs0QkFDL0IsMkJBQTJCOzRCQUMzQiw0QkFBNEI7NEJBQzVCLDRCQUE0Qjs0QkFDNUIsNkJBQTZCOzRCQUM3QiwrQkFBK0I7NEJBQy9CLDhCQUE4Qjt5QkFDL0I7d0JBQ0QsT0FBTyxFQUFPOzRCQUNaLDBCQUEwQjs0QkFDMUIsK0JBQStCOzRCQUMvQiwyQkFBMkI7NEJBQzNCLDRCQUE0Qjs0QkFDNUIsNEJBQTRCOzRCQUM1Qiw2QkFBNkI7NEJBQzdCLCtCQUErQjs0QkFDL0IsOEJBQThCO3lCQUMvQjtxQkFDRjs7a0NBMURYOzs7Ozs7O0FDQUE7Ozs7QUFFQSxtQ0FBc0MsS0FBVTs7UUFDOUMsSUFBTSxPQUFPLEdBQUcsSUFBSUMsaUJBQVcsRUFBRSxDQUFDO1FBQ2xDLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLE9BQU8sT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM3QjthQUFNO1lBQ0wsT0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzFCO0tBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=